Funcion rellenarArreglo(arreglo Por Referencia, n Por Valor, mensaje Por Valor)
	Para i = 1 Hasta n Hacer
		Escribir mensaje;
		Leer dato;
		arreglo[i] = dato;
	FinPara
FinFuncion

Funcion pos = busquedaSecuencial (arreglo Por Referencia, n Por Valor, elemento Por Valor)
	Definir i Como Entero;
	i = 1;
	pos = -1;
	Mientras i<= n y pos = -1 Hacer
		si arreglo[i] <> elemento Entonces
			i = i +1;
		SiNo
			pos = i;
		FinSi
	FinMientras
FinFuncion

Algoritmo SaldoCuentaBancaria
	///el lector toma el n�mero de cuenta y el sistema busca dicho
	///n�mero en la base de datos. Si lo encuentra reporta el saldo, si no muestra un mensaje
	///de error. Suponiendo que la base de datos consiste en tres vectores: n�mero de cuenta,
	///titular y saldo. Dise�ar un algoritmo para consultar el saldo de una cuenta
	
	Definir tama�o Como Entero;
	Escribir "Ingrese la cantidad de usuarios que hay en la base de datos";
	Leer tama�o;
	
	//Arreglos
	Dimension nroCuenta[tama�o];
	Dimension titular[tama�o];
	Dimension saldo[tama�o];
	
	rellenarArreglo(nroCuenta, tama�o, "Ingrese el numero de cuenta");
	rellenarArreglo(titular, tama�o, "Ingrese el nombre del titular de la cuenta");
	rellenarArreglo(saldo, tama�o, "Ingrese el saldo de la cuenta");
	
	Definir NumCuenta Como Entero;
	Escribir "Ingrese su numero de cuenta";
	Leer NumCuenta;
	
	Definir pos Como Entero;
	pos = busquedaSecuencial(nroCuenta, tama�o, NumCuenta);
	
	Si pos = -1 Entonces
		Escribir " El numero de cuenta ingresado no est� en la base de datos";
	SiNo
		Escribir "La cuenta pertenece a: ", titular[pos];
		Escribir "El saldo disponible en la cuenta es  ", saldo[pos];
	FinSi
	
	
FinAlgoritmo
