
Funcion rellenarArreglo(NumFac Por Referencia, NomCli Por Referencia, FechaFac Por Referencia, valorFac Por Referencia, n Por Valor)
	Definir dato, precio Como Entero;
	Definir nombre, fecha Como Cadena;
Para i = 1 Hasta n Hacer
	Escribir "Ingrese el numero de factura";
	Leer dato;
	NumFac[i] = dato;
	Escribir "Ingrese el nombre del cliente";
	Leer nombre;
	NomCli[i] = nombre;
	Escribir "Ingrese la fecha de la factura";
	Leer fecha;
	FechaFac[i] = fecha;
	Escribir "Ingrese el valor de la factura";
	Leer precio;
	valorFac[i]= precio;

FinPara

FinFuncion

Funcion pos = busquedaSecuencial (arreglo Por Referencia, n Por Valor, elemento Por Valor)
	Definir i Como Entero;
	i = 1;
	pos = -1;
	Mientras i<= n y pos = -1 Hacer
		si arreglo[i] <> elemento Entonces
			i = i +1;
		SiNo
			pos = i;
		FinSi
	FinMientras
FinFuncion


Algoritmo FacturaAlmacen
	//n�mero de factura, nombre
	//del cliente, fecha de facturaci�n y valor de la factura, almacenados en vectores. Se desea
	//un algoritmo que lea el n�mero de factura y muestre los dem�s datos
	
	Definir n Como Entero;
	Escribir "Ingrese la cantidad de facturas emitidas";
	Leer n;
	
	///Se crean los arreglos de los datos
	Dimension numFactura[n];
	Dimension nombreCliente[n];
	Dimension fechaFacturacion[n];
	Dimension valorFactura[n];
	
	///Se completan los arreglos con los datos 
	rellenarArreglo(numFactura, nombreCliente, fechaFacturacion,  valorFactura, n) 
	
	//Buscar numero factura
	Definir nroFactura Como Entero;
	Escribir "Ingrese el numero de factura";
	Leer nroFactura;
	
	///Invocar funcion
	Definir pos Como Entero;
	pos=busquedaSecuencial(numFactura,n,nroFactura);
	
	Si pos = -1 Entonces
		Escribir " El numero de factura es incorrecto";
	SiNo
		Escribir "Cliente: ", nombreCliente[pos];
		Escribir "Fecha facturacion: ", fechaFacturacion[pos];
		Escribir "Valor de la factura: ", valorFactura[pos], "$";
	FinSi
	
	
FinAlgoritmo
