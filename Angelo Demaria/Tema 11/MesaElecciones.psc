Funcion rellenarArreglo(arreglo Por Referencia, n Por Valor, mensaje Por Valor)
	Para i = 1 Hasta n Hacer
		Escribir mensaje;
		Leer dato;
		arreglo[i] = dato;
	FinPara
FinFuncion

Funcion pos = busquedaSecuencial (arreglo Por Referencia, n Por Valor, elemento Por Valor)
	Definir i Como Entero;
	i = 1;
	pos = -1;
	Mientras i<= n y pos = -1 Hacer
		si arreglo[i] <> elemento Entonces
			i = i +1;
		SiNo
			pos = i;
		FinSi
	FinMientras
FinFuncion



Algoritmo MesaElecciones
	//El d�a de elecciones, en cada mesa se cuenta con un listado de las personas
	//que votar�n en dicha mesa. Los datos est�n guardados en dos vectores, en el primero la
	//c�dula y en el segundo el nombre. Se requiere un algoritmo que ingrese la c�dula de una
	//persona e informe si puede sufragar en ese puesto de votaci�n
	
	Definir tamanio Como Entero;
	Escribir "Ingrese el la cantidad de personas en la lista";
	Leer tamanio;
	
	//Declarar arreglo
	Dimension cedula[tamanio];
	Dimension nombre[tamanio];
	
	//Llamamos a la Funcion 
	rellenarArreglo(cedula, tamanio, " Ingrese la cedula");
	rellenarArreglo(nombre, tamanio, " Ingrese el nombre");
	
	//Se pide el dato a buscar
	Definir NroCedula Como Entero;
 	Escribir "ingrese el numero de cedula a buscar";
	Leer NroCedula;
	
	Definir pos Como Entero;
	pos = busquedaSecuencial(cedula, tamanio, NroCedula);
	
	Si pos = -1 Entonces
		Escribir " El numero de cedula ingresado no est� en la base de datos";
	SiNo
		Escribir nombre[pos], " puede sufragar en este puesto";
	
	FinSi
FinAlgoritmo
