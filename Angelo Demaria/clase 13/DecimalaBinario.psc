Funcion ConvertirABinario(decimal Por Valor, arreglo Por Referencia, n Por Referencia)
	n = 1;
	Mientras decimal > 0 Hacer
		arreglo[n] <- "1";
		Si decimal % 2 = 0  Entonces
			arreglo[n] = "0";
		FinSi
		decimal = trunc(decimal / 2);
		n = n + 1;
	FinMientras
FinFuncion

Algoritmo DecimalaBinario
	//Ingresar una cantidad en sistema decimal y transformar a binario
	
	Definir num Como Entero;
	Escribir "Ingrese un numero decimal";
	Leer num;
	
	Definir binario Como Cadena;
	binario = " ";
	
	Definir resultado Como Logico;
	
	//Proceso 
	Si num > 0 Entonces
		Dimension arreglo[100];
		Definir n Como Entero;
		n = 1;
		ConvertirABinario(num, arreglo, n);
		
		Para i = 1 Hasta n Hacer
			binario = arreglo[i] + binario;
		FinPara
		Escribir binario;
	SiNo
		si num = 0 Entonces
			Escribir Num;
		SiNo
			Escribir "Ingreso un numero negativo";
		FinSi
	FinSi
	
FinAlgoritmo
