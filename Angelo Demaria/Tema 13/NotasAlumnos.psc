Funcion Aprobados = calcular_Aprobados (arreglo Por Referencia, n Por Valor)
	Definir Aprobados Como Entero;
	Aprobados = 0;
	Para i = 1 hasta n Hacer
		Si arreglo[i] <=6 Entonces
			Aprobados = Aprobados + 1; 
		FinSi

	FinPara
FinFuncion

Algoritmo NotasAlumnos 
	///: Construya un algoritmo que permita ingresar 90 notas entre uno y siete,
	///indicando finalmente cu�ntos alumnos aprobaron y cu�ntos reprobaron
	
	Definir cantAlumnos Como Entero;
	Escribir "Ingrese la cantidad de alumnos";
	Leer cantAlumnos;
	Dimension Alumnos[cantAlumnos];
	Definir  Aprobados, i, nota Como Entero;
	Aprobados = 0 ;
	
	i = 1;
	Mientras nota <= 7 Y nota >= 0 Y i <= cantAlumnos Hacer
		Escribir "Ingrese la nota, al ingresar una nota fuera de los parametros corta la secuencia";
		Leer nota;
		Alumnos[i] = nota; 
		i = i + 1 ; 
	FinMientras
	
	Aprobados = calcular_Aprobados(Alumnos, cantAlumnos);
	
	Definir Desaprobados Como Entero;
	Desaprobados = cantAlumnos - Aprobados;
	
	Escribir "Aprobaron ", Aprobados, " alumnos";
	Escribir "Desaprobaron ", Desaprobados, " alumnos"; 
FinAlgoritmo
