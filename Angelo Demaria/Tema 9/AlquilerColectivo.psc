Algoritmo 	AlquilerColectivo
	
	///y requiere determinar cu�nto debe cobrar a cada alumno y cu�nto se debe pagar a la agencia de
	///viajes por el servicio
	Definir cantAlumnos Como Entero;
	Escribir "Ingrese la cantidad de alumnos que participaran en el viaje";
	Leer cantAlumnos;
	Definir costoViaje Como Real;
	
	//Si anidado para separar que tipo de presupuesto le corresponde a cada alumno
	Si cantAlumnos >= 100 Entonces
		costoViaje = 65;
		Escribir "Cada alumno debe pagar ", costoViaje," $";
	SiNo
		SI cantAlumnos >=50 Y cantAlumnos <= 99 Entonces
			costoViaje = 70;
			Escribir "Cada alumno debe pagar ", costoViaje," $";
		SiNo
			si cantAlumnos <= 49 Y cantAlumnos >= 30 Entonces
				costoViaje = 95;
				Escribir "Cada alumno debe pagar ", costoViaje," $";
			SiNo
				costoViaje = 4000 / cantAlumnos;
				Escribir "Cada alumno debe pagar ", costoViaje," $";
			FinSi
		FinSi
		
	FinSi

	
FinAlgoritmo
