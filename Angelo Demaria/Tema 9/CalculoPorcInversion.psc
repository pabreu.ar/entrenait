Algoritmo CalculoPorcInversion
	///a. Obtener el porcentaje que cada quien invierte con respecto a la cantidad total invertida
	
	Definir nombre1, nombre2, nombre3 Como Cadena;
	Definir montoInversion1, montoInversion2, montoInversion3 Como Real;
	
	Escribir " Ingrese el nombre";
	Leer nombre1;
	Escribir "Ahora el monto a invertir:";
	Leer montoInversion1;
	Escribir " Ingrese el nombre";
	Leer nombre2;
	Escribir "Ahora el monto a invertir:";
	Leer montoInversion2;
	Escribir " Ingrese el nombre";
	Leer nombre3;
	Escribir "Ahora el monto a invertir:";
	Leer montoInversion3;
	
	Definir totalInversion Como Real;
	totalInversion = montoInversion1 + montoInversion2 + montoInversion3;
	Definir porcentaje Como Entero;
	Porcentaje1 = Redon(montoInversion1*100/totalInversion);
	Porcentaje2 = Redon(montoInversion2*100/totalInversion);
	Porcentaje3 = Redon(montoInversion3*100/totalInversion);
	
	
	Escribir nombre1, " Invirtio un ", Porcentaje1,"% del total del fondo.";
	Escribir nombre2, " Invirtio un ", Porcentaje2,"% del total del fondo.";
	Escribir nombre3, " Invirtio un ", Porcentaje3,"% del total del fondo.";
	
FinAlgoritmo
