Algoritmo ValorNum
	///Determinar el algoritmo para saber si un n�mero es positivo o negativo
	Definir num Como Entero;
	Leer num;
	
	//Parte Logica 
	Si num < 0 Entonces
		Escribir num," es numero negativo";
	SiNo
		SI num > 0 Entonces
			Escribir num , " es un numero positivo";
		Sino 
			Escribir "Su numero es 0 (CERO)";
		FinSi
	FinSi
	
FinAlgoritmo
