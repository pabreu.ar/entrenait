Algoritmo VentaLapices
	// cu�nto se debe pagar por una cantidad
	//de l�pices considerando que si son 1000 o m�s el costo es de 85 pesos; de lo contrario, el
		//precio es de 90 persos.
	Definir cantLapices Como Entero;
	Escribir "Ingrese la cantidad de lapices que desea vender";
	Leer cantLapices;
	
	Definir precioVenta Como Entero;
	Si cantLapices >= 1000 Entonces
		precioVenta = cantLapices * 85;
	SiNo
		precioVenta = cantLapices * 90;
	FinSi
	
	Escribir "El valor total de los lapices es ", precioVenta, " $";
	
FinAlgoritmo
