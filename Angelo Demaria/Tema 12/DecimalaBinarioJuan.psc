Funcion ConvertirABinario(decimal Por Valor, arreglo Por Referencia, n Por Referencia)
	n = 1;
	Mientras decimal > 0 Hacer
		arreglo[n] <- "1";
		Si decimal % 2 = 0  Entonces
			arreglo[n] = "0";
		FinSi
		decimal = trunc(decimal / 2);
		n = n + 1;
	FinMientras
FinFuncion

Funcion ConvertirABinarioJuan(decimal Por Valor, arreglo Por Referencia)
	Definir n Como Entero;
	n = 1;
	Mientras decimal > 0 Hacer
		arreglo[n] <- "1";
		Si decimal % 2 = 0  Entonces
			arreglo[n] = "0";
		FinSi
		decimal = trunc(decimal / 2);
		n = n + 1;
	FinMientras
FinFuncion

Funcion n = DeterminarNJuan(decimal Por Valor)
	Mientras decimal > 0 Hacer
		decimal = trunc(decimal / 2);
		n = n + 1;
	FinMientras
FinFuncion

Algoritmo DecimalaBinario
	//Ingresar una cantidad en sistema decimal y transformar a binario
	
	Definir num Como Entero;
	Escribir "Ingrese un numero decimal";
	Leer num;
	
	Definir binario Como Cadena;
	binario = " ";
	
	Definir resultado Como Logico;
	
	//Proceso 
	Si num > 0 Entonces
		Definir n Como Entero;
		n = DeterminarNJuan(num);
		Dimension arreglo[n];
		
		ConvertirABinarioJuan(num, arreglo);
		
		Para i = 1 Hasta n Hacer
			binario = arreglo[i] + binario;
		FinPara
		Escribir binario;
	SiNo
		si num = 0 Entonces
			Escribir Num;
		SiNo
			Escribir "Ingreso un numero negativo";
		FinSi
	FinSi
	
FinAlgoritmo
