Algoritmo SumaDeArreglos
	//Sumar los elementos de dos arreglos y guardar el resultado en otro
	//arreglo de 1 posici�n
	
	//Se crean los arreglos 
	Definir tamanio Como Entero;
	Escribir "Ingrese el tama�o del arreglo"
	Leer tamanio;
	Dimension ArregloA[tamanio];
	Dimension ArregloB[tamanio];
	Dimension ArregloSuma[tamanio];
	
	//Se llenan los arrelgos conn datos y se los suman a un tercer arreglo
	Definir i Como Entero;
	Para i = 1 hasta tamanio Hacer
		Escribir "Ingrese el ", i,"� numero del primer arreglo"
		Leer num1;
		ArregloA[i] = num1;
		Escribir "Ingrese el ", i,"� numero del segundo arreglo"
		Leer num2;
		ArregloB[i] = num2;
		ArregloSuma[i] = ArregloA [i] + ArregloB[i];
		Escribir "La suma de los dos arreglos en la posicion ", i, " es: ",ArregloSuma[i];
	FinPara
	
FinAlgoritmo
