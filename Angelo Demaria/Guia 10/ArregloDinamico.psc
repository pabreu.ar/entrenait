Algoritmo ArregloDinamico
	 /// Crear un arreglo con n n�meros, ingresados por teclado y mostrar sus
	///valores elevados al cuadrado.
	
	Definir tamanio Como Entero;
	Escribir "Ingrese la cantidad de numeros que desea ingresar";
	Leer tamanio;
	
	//arreglo
	Definir num Como Entero; 
	Dimension arreglo[tamanio];
	Escribir "Ingrese los numeros para calcular su potencia";
	Para i = 1 Hasta tamanio Hacer
		Leer num;
		arreglo[i] = num^2;
	FinPara
	
	///Mostrar 
	Para i = 1 Hasta tamanio Hacer
		Escribir " el cuadrado es ", arreglo[i];
	FinPara
	
FinAlgoritmo
