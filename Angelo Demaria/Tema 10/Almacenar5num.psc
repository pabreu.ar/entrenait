Algoritmo Almacenar5num
	//Ingresar 5 n�meros, almacenarlos en un arreglo y mostrarlos.
	Definir num,i Como Entero
	Escribir "Ingrese 5 numeros";
	
	///Definir arreglo
	Dimension arreglo[5];
	Para i=1 hasta 5 Hacer
		Leer num;
		arreglo[i] = num;
	FinPara
	
	//Mostrar resultado
	Escribir "Los numeros ingresados son:";
	Para i = 1 Hasta 5 hacer
		Escribir Sin Saltar arreglo[i]," -"; 
	FinPara
	
FinAlgoritmo
