Algoritmo ArregloRaizCuadrada
	//Crear un arreglo con n n�meros, ingresados por teclado y mostrar sus
	//valores elevados al cuadrado.
	
	Definir cantidad Como Entero
	Escribir "Ingrese la cantidad de datos que elevara al cuadrado"
	Leer cantidad 
	
	Dimension num[cantidad]
	
	Definir numRaiz Como Real
	Para i <- 1 Hasta cantidad Con Paso 1 Hacer
		Escribir "Ingrese el numero que sera elevado al cuadrado " i 
		Leer num[i]
		numRaiz <- num[i] * num[i]
		Escribir "La raiz del numero " num[i] " es " numRaiz
	Fin Para
FinAlgoritmo
