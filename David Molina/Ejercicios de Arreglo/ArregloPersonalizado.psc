Algoritmo ArregloPersonalizado
	//Crear un arreglo de 5 posiciones y ll�nelo con los n�meros que el usuario
    //desee.
	
	Definir cantidad Como Entero
	cantidad <- 5
	
	Definir i Como Entero
	Dimension arregloUsuario[cantidad]
	
	Para i <- 1 Hasta cantidad Con Paso 1 Hacer
		Escribir "Ingrese un numero ";
		Leer arregloUsuario[i];
	Fin Para
	
	Para i <- 1 Hasta cantidad Con Paso 1 Hacer
		Escribir "El numero que ingreso en la posicion " i " fue " arregloUsuario[i]
	FinPara
FinAlgoritmo
