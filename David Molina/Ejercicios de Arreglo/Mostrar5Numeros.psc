Algoritmo Mostrar5Numeros
	// Ingresar 5 n�meros, almacenarlos en un arreglo y mostrarlos
	
	Definir cantidad Como Entero
	cantidad <- 5
	Dimension num[cantidad]
	
	Para i <- 1 Hasta cantidad Con Paso 1 Hacer
		Escribir "Ingrese un numero para " i
		Leer num[i]
	Fin Para
	
	Para i <- 1 Hasta cantidad Con Paso 1 Hacer
		Escribir "El numero ingresado para " i " fue " num[i]
	Fin Para
FinAlgoritmo