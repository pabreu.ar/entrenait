Algoritmo ArregloPlatoPrecio
	//Dise�e un arreglo en el que se ingrese la cantidad de productos y sus
	//respectivos precios, para la preparaci�n de un plato, tambi�n se debe mostrar al final el
    //costo a gastar.
	
	Definir cantidadProductos, preciosPrecio Como Entero;
	Escribir "Ingrese la cantidad de los productos"
	Leer cantidadProductos;
	
	Dimension num[cantidadProductos]
	Dimension precio[cantidadProductos]
	
	
	Definir totalPagar Como Entero
	totalPagar <- 0
	Para i <- 1 Hasta cantidadProductos Con Paso 1 Hacer
		Escribir "Ingrese el precio de los productos"
		Leer precio[i]
		totalPagar <- totalPagar + precio[i]
	Fin Para
	Escribir " La cantidad de productos es " cantidadProductos " y el precio a pagar por el plato es " totalPagar
FinAlgoritmo