Algoritmo NumRomano
	
	Definir numeRomano Como Entero
	Escribir "Hola, ingrese el numero que desea convertir a Romano"
	Leer numeRomano 
	Segun numeRomano Hacer
		numeRomano = 1:
			Escribir " El numero " numeRomano " es I "
		numeRomano = 5:
			Escribir " El numero " numeRomano " es V "
		numeRomano = 9:
			Escribir " El numero " numeRomano " es IX  "
		numeRomano = 14:
		    Escribir " El numero " numeRomano " es XIV  "
		numeRomano = 20:
			Escribir  " El numero " numeRomano " es XX "
		De Otro Modo:
			Escribir " La traduccion de " numeRomano " no esta disponible "
	Fin Segun
FinAlgoritmo
