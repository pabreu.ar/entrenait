//La compa��a de aviaci�n Aerol�neas Argentinas mantiene la informaci�n de
//todas sus rutas de vuelo en una base de datos con informaci�n como: lugares de origen y
//destino, horario, valor; para ello utiliza una matriz en la que cada columna est� destinada a
//uno de los datos mencionados. Dise�ar un algoritmo para averiguar si existe un vuelo entre
//un origen y un destino introducidos por el usuario y en caso de existir obtener informaci�n
//sobre el mismo.
	
Funcion pos = BusquedaSecuencial(arreglo, arreglo2, n, key, key2)
	pos = -1;
	Definir i Como Entero;
	i = 1;
	
	Mientras pos = -1 Y i <= n Hacer
		Si arreglo[i] = key y arreglo2[i] = key2 Entonces
			pos = i;
		FinSi
		i = i + 1;
	Fin Mientras
FinFuncion

//Llenar la BD
Funcion InicializarBaseDeDatosAerolineasArgentinas(lugaresOrigen, lugaresDestino, horarios, valores)
	lugaresOrigen[1] = "Sunchales";
	lugaresDestino[1] = "Santa Fe";
	horarios[1] = "08:00";
	valores[1] = 1000;
	
	lugaresOrigen[2] = "Sunchales";
	lugaresDestino[2] = "Mar del Plata";
	horarios[2] = "17:00";
	valores[2] = 2000;
	
	lugaresOrigen[3] = "Mar del Plata";
	lugaresDestino[3] = "Bariloche";
	horarios[3] = "19:30";
	valores[3] = 3200;
	
	lugaresOrigen[4] = "Rosario";
	lugaresDestino[4] = "Buenos Aires";
	horarios[4] = "10:15";
	valores[4] = 2200;
FinFuncion

Algoritmo DeterminarExistenciaDeVueloEntreOrigenDestinoDado
	Definir origenBuscado, destinoBuscado Como Cadena;
	
	Escribir "Ingrese el origen que desea buscar: ";
	Leer origenBuscado;
	
	Escribir "Ingrese el destino que desea buscar: ";
	Leer destinoBuscado;
	
	Definir cantidad Como Entero;
	cantidad = 1000;
	Dimension lugaresOrigen[cantidad];
	Dimension lugaresDestino[cantidad];
	Dimension horarios[cantidad];
	Dimension valores[cantidad];
	
	InicializarBaseDeDatosAerolineasArgentinas(lugaresOrigen, lugaresDestino, horarios, valores);
	
	Definir indice, indice1 Como Entero;
	indice = BusquedaSecuencial(lugaresOrigen, lugaresDestino, cantidad, origenBuscado, destinoBuscado)
	
	Si indice <> -1 Entonces
		Escribir "Los datos de su viaje son: ", lugaresOrigen[indice], " - ", lugaresDestino[indice] " a la hora: ", horarios[indice], " con un costo de: $", valores[indice];
	FinSi
	Si indice = -1 Entonces
		Escribir "No se encuentra el viaje buscado, intente de nuevo ";
	FinSi
FinAlgoritmo