//En el almac�n TodoCaro se cuenta con los datos: n�mero de factura, nombre
//del cliente, fecha de facturaci�n y valor de la factura, almacenados en vectores. Se desea
//un algoritmo que lea el n�mero de factura y muestre los dem�s datos.

Funcion posicion <- BusquedaSecuencial(arreglo, n, key)
	posicion = -1;
	Definir i Como Entero;
	i <- 1;
	Mientras posicion = -1 y i <= n Hacer
		Si arreglo[i] = key Entonces
			posicion <- i;
		FinSi
		i <- i + 1;
	FinMientras
FinFuncion

Funcion InicializarBaseDeDatosAlmacenTodoCaro(nroFactura, nombreCliente, fechaFacturacion, importeFactura)
	nroFactura[1] <- "F00-025";
	nombreCliente[1] <-  "Mateo";
	fechaFacturacion[1] <- "10-09-2021";
	importeFactura[1] <- 9929;
	
	nroFactura[2] <- "F00-026";
	nombreCliente[2] <-  "Gabriel";
	fechaFacturacion[2] <- "11-11-2021";
	importeFactura[2] <- 9010;
	
	nroFactura[3] <- "F00-035";
	nombreCliente[3] <-  "Pamela";
	fechaFacturacion[3] <- "12-11-2021";
	importeFactura[3] <- 5005;
	
	nroFactura[4] <- "F00-038";
	nombreCliente[4] <-  "Maria";
	fechaFacturacion[4] <- "19-12-2021";
	importeFactura[4] <- 11000;
	
	nroFactura[5] <- "F00-100";
	nombreCliente[5] <-  "Marcelo";
	fechaFacturacion[5] <- "29-04-2021";
	importeFactura[5] <- 88000;
FinFuncion

Algoritmo ConsultarFactura
	
	Definir nroFacturaBuscado Como Cadena;
	Escribir "Ingrese el n�memro de factura a buscar: ";
	Leer nroFacturaBuscado;
	
	//Definir cantidad de clientes
	Definir cantidadClientes Como Entero;
	cantidadClientes <- 1000;
	
	//Declarar vectores
	Dimension nroFactura[cantidadClientes];
	Dimension nombreCliente[cantidadClientes];
	Dimension fechaFacturacion[cantidadClientes];
	Dimension importeFactura[cantidadClientes];
	
	InicializarBaseDeDatosAlmacenTodoCaro(nroFactura, nombreCliente, fechaFacturacion, importeFactura)
	
	//Buscar por n�mero de factura
	Definir indice Como Entero;
	indice <- BusquedaSecuencial(nroFactura, cantidadClientes, nroFacturaBuscado)
	Si indice <> -1 Entonces
		Escribir "La factura nro ", nroFacturaBuscado, " se encuentra a nombre de: ", nombreCliente[indice];
		Escribir "La fecha de emisi�n de la factura es: ", fechaFacturacion[indice];
		Escribir "El importe de la factura es: $", importeFactura[indice];
	SiNo
		Escribir "El n�mero de factura no se encuentra en la base de datos";
	FinSi
FinAlgoritmo

