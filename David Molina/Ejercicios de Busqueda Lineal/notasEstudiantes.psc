Funcion pos = busquedaSecuencial (arreglo Por Referencia, n Por Valor, elemento Por Valor)
	Definir i Como Entero;
	i = 1;
	pos = -1;
	Mientras i<= n y pos = -1 Hacer
		si arreglo[i] <> elemento Entonces
			i = i +1;
		SiNo
			pos = i;
		FinSi
	FinMientras
FinFuncion

Funcion rellenarArreglo(arreglo Por Referencia, n Por Valor, mensaje Por Valor)
	///Funcion para llenar el arreglo con datos
	Para i = 1 Hasta n Hacer
		Escribir mensaje;
		Leer inPut;
		arreglo[i] = inPut;
	FinPara
FinFuncion

Algoritmo DatosAlumnos
	//Un profesor guarda los datos de sus estudiantes en tres vectores: c�digo,
	//nombre y nota. Se requiere un algoritmo para consultar la nota de un estudiante a partir de
	//	su c�digo.
	Definir  cantEstudiantes Como Entero;
	Escribir"Ingrese la cantidad de estudientes" ;
	leer cantEstudiantes;
	
	//Arreglos
	Dimension codigo[cantEstudiantes];
	Dimension nombre[cantEstudiantes];
	Dimension nota[cantEstudiantes];
	
	rellenarArreglo(codigo,cantEstudiantes,"Ingrese el codigo del estudiante ");
	rellenarArreglo(nombre,cantEstudiantes,"Ingrese el nombre del estudiante ");
	rellenarArreglo(nota,cantEstudiantes,"Ingrese la nota del estudiante ");
	
	Definir cod Como Entero;
	Escribir "Ingrese el codigo del estudiante buscado ";
	Leer cod;
	
	Definir pos Como Entero;
	pos = busquedaSecuencial(codigo,cantEstudiantes, cod);
	
	Si pos = -1 Entonces
		Escribir " El codigo ingresa del alumno no es valido";
	SiNo
		Escribir "La nota del alumno: ", nombre[pos], " es: ", nota[pos];
	FinSi
FinAlgoritmo
