Algoritmo AlmacenTodoCaro
	//En el almac�n TodoCaro se cuenta con los datos: n�mero de factura, nombre
	//del cliente, fecha de facturaci�n y valor de la factura, almacenados en vectores. Se desea
	//un algoritmo que lea el n�mero de factura y muestre los dem�s datos
	//
	//Ingresar los datos de i y j 
	//Leer numero de factura
	//Mostrar los datos de nombre, fecha, valor 
	Funcion InicializarBaseDatos <- (codigos, nombres, valorFactura, fechaFactura);
	Definir n Como Entero;
	n <- 5
	
	Dimension codigos[n]
	codigos[1] <- 1
	codigos[2] <- 3
	codigos[3] <- 4
	codigos[4] <- 2
	codigos[5] <- 5
	
	Dimension nombres[n]
	nombres[1] <- "pedro"
	nombres[2] <- "juan"
	nombres[3] <- "daiana"
	nombres[4] <- "agus"
	nombres[5] <- "david"
	
	Dimension fechaFactura[n]
	fechaFactura[1] <- "11/3/22"
	fechaFactura[2] <- "10/5/22"
	fechaFactura[3] <- "08/12/22"
	fechaFactura[4] <- "05/3/22"
	fechaFactura[5] <- "20/2/22"
	
	Dimension valorFactura[n]
	valorFactura[1] <- "1000"
	valorFactura[2] <- "2000"
	valorFactura[3] <- "3000"
	valorFactura[4] <- "4000"
	valorFactura[5] <- "5000"
	
	Definir codigoBuscado Como Entero
	Escribir "Ingrese el codigo que desea buscar"
	Leer codigoBuscado
	Definir i Como Entero
	Para i <- 1 Hasta 5 Con Paso 1 Hacer
		Si codigoBuscado = codigos[i]
		FinSi
	Fin Para
	

FinFuncion
FinAlgoritmo