//La empresa de comunicaciones L�nea Segura registra el origen, el destino, la
//fecha, la hora y la duraci�n de todas las llamadas que hacen sus usuarios, para esto hace
//uso de cinco vectores. Se requiere un algoritmo para saber si desde un tel�fono
//determinado se ha efectuado alguna llamada a un destino y en caso afirmativo conocer la
//fecha, la hora y la duraci�n de la llamada.

Funcion pos = BusquedaSecuencial(arreglo, n, key)
	pos = -1;
	Definir i Como Entero;
	i = 1;
	
	Mientras pos = -1 Y i <= n Hacer
		Si arreglo[i] = key Entonces
			pos = i;
		FinSi
		i = i + 1;
	Fin Mientras
FinFuncion

Funcion InicializarBaseDatosLineaSegura (origen, destino, fecha, hora, duracion)
	origen[1] = "3493449120";
	destino[1] = "3492601230";
	fecha[1] = "05-11-21";
	hora[1] = "12:00";
	duracion[1] = "20min";
	
	origen[2] = "3546520526";
	destino[2] = "0116680752";
	fecha[2] = "08-11-21";
	hora[2] = "18:45";
	duracion[2] = "25min";
	
	origen[3] = "3492569874";
	destino[3] = "3515992385";
	fecha[3] = "30-11-21";
	hora[3] = "09:00";
	duracion[3] = "35min";
	
	origen[4] = "3493664589";
	destino[4] = "3547654578";
	fecha[4] = "19-09-21";
	hora[4] = "13:30";
	duracion[4] = "34min";
	
	origen[5] = "3492451200";
	destino[5] = "0116541233";
	fecha[5] = "22-04-22";
	hora[5] = "15:00";
	duracion[5] = "20min";
	
	origen[6] = "3493662323";
	destino[6] = "3436652100";
	fecha[6] = "12-03-21";
	hora[6] = "20:00";
	duracion[6] = "5min";
FinFuncion

Algoritmo InformacionLlamadas
	Definir telOrigenBuscado Como Cadena;
	Escribir "Ingrese el numero de tel�fono que desea consultar: ";
	Leer telOrigenBuscado;
	
	//Definir cantidad m�xima de usuarios
	Definir cantUsuarios Como Entero;
	cantUsuarios = 1000;
	
	//Declarar los cinco vectores
	Dimension origen[cantUsuarios];
	Dimension destino[cantUsuarios];
	Dimension fecha[cantUsuarios];
	Dimension hora[cantUsuarios];
	Dimension duracion[cantUsuarios];
	
	//Inicializar valores
	InicializarBaseDatosLineaSegura(origen, destino, fecha, hora, duracion);
	
	Definir indice Como Entero;
	indice = BusquedaSecuencial(origen, cantUsuarios, telOrigenBuscado)
	
	Si indice <> -1 Entonces
		Escribir "El tel�fono ", telOrigenBuscado, " ha realizado una llamada al n�mero ", destino[indice], ", el d�a ", fecha[indice], ", a la hora ", hora[indice], ", y cuya duraci�n fue de ", duracion[indice];
	FinSi
	
	Si indice = -1 Entonces
		Escribir "El t�lefono no ha realizado ninguna llamada";
	FinSi
	
FinAlgoritmo