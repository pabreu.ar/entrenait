Algoritmo CalculoDiaMa�ana
	//Programa que muestra el d�a que ser� ma�ana.
	
	Definir diaHoy, mesHoy, a�oHoy Como Entero;
	Escribir "Ingrese la fecha de hoy en numeros";
	Leer diaHoy, mesHoy, a�oHoy;
	
	Definir diaMa�ana, mesMa�ana, a�oMa�ana Como Entero;
	
	Si diaHoy <= 30 Entonces
		diaMa�ana <- diaHoy + 1;
	SiNo
		diaMa�ana <- 1
	Fin Si
	
	Si diaHoy = 31 Y mesHoy = 12 Entonces
		mesMa�ana <- 1;
	SiNo
		Si diaHoy >= 31 Y mesHoy <= 11 Entonces
			mesMa�ana <- mesHoy + 1
		SiNo
			mesMa�ana <- mesHoy;
		FinSi
	Fin Si

	Si diaHoy = 31 Y mesHoy = 12 Entonces
		a�oMa�ana <- a�oHoy + 1;
	SiNo
		a�oMa�ana <- a�oHoy;
	Fin Si
	
	Escribir "Hoy es: ", diaHoy, "/", mesHoy, "/", a�oHoy;
	Escribir "Ma�ana es: ", diaMa�ana, "/", mesMa�ana,"/", a�oMa�ana;
FinAlgoritmo
