Algoritmo NumeroRomano
	//Escriba un programa que convierta los siguientes n�meros a su
    //correspondiente n�mero romano: 1, 5, 9, 14, 20, 26, 40, 50, 90, 99, 100, 500 y 1000. Para
	//cualquier otro n�mero se deber� reportar: "No existe traducci�n disponible"
	
	Definir numeRomano Como Entero
	Escribir "Hola, ingrese el numero que desea convertir a Romano"
	Leer numeRomano 
	
	Si numeRomano = 1 Entonces
		Escribir " El numero " numeRomano " es I "
	FinSi
	
	Si numeRomano = 5 Entonces
		Escribir " El numero " numeRomano " es V "
	FinSi
	
	Si numeRomano =  9 Entonces
		Escribir " El numero " numeRomano " es IX  "
	FinSi
	
	Si numeRomano =  14 Entonces
		Escribir " El numero " numeRomano " es XIV  "
	FinSi
	
	Si numeRomano =  20 Entonces
		Escribir " El numero " numeRomano " es XX "
	FinSi
	
	Si numeRomano =  26 Entonces
		Escribir " El numero " numeRomano " es XXVI "
	FinSi
	
	Si numeRomano =  40 Entonces
		Escribir " El numero " numeRomano " es XL "
	FinSi
	
	Si numeRomano =  50 Entonces
		Escribir " El numero " numeRomano " es L "
	FinSi
	
	Si numeRomano =  90 Entonces
		Escribir " El numero " numeRomano " es XC "
	FinSi
	
	Si numeRomano =  99 Entonces
		Escribir " El numero " numeRomano " es XCIX "
	FinSi
	
	Si numeRomano =  100 Entonces
		Escribir " El numero " numeRomano " es C "
	FinSi
	
	Si numeRomano =  500 Entonces
		Escribir " El numero " numeRomano " es D "
	FinSi
	
	Si numeRomano =  1000 Entonces
		Escribir " El numero " numeRomano " es M "
	FinSi
FinAlgoritmo
