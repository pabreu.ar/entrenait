Algoritmo ConvBinarioDecimal
	//Ingresar una cantidad en binario y transformar al sistema decimal.
	Definir x, num, decimal Como Entero;
	Definir binario Como Caracter;
	
	Escribir "Ingresa un numero en binario";
	Leer num
	binario = ConvertirATexto(num)
	num = Longitud(binario)
	x = 0
	decimal = 0
	
	Mientras num > 0 Hacer
		Si Subcadena(binario, num, num) == "1" Entonces
			decimal = decimal + 2 ^ x;
		FinSi
		num = num - 1;
		x = x + 1;
	FinMientras
	Escribir "El numero en decimal es: ", decimal;
FinAlgoritmo