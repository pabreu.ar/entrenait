Algoritmo CalcularPrecioViaje
	//En un Instituto una escuela est� organizando un viaje de estudios, y requiere
	//determinar cu�nto debe cobrar a cada alumno y cu�nto se debe pagar a la agencia de
	//viajes por el servicio. La forma de cobrar es la siguiente: si son 100 alumnos o m�s, el
	//importe por cada alumno es de 65 pesos; de 50 a 99 alumnos, e es de 70 pesos, de 30 a 49 de 95 pesos, y si son menos de 30, 
	//el importe del alquiler del autob�s es de 4000pesos, 
	//sin importar el n�mero de alumnos. Realiza un algoritmo que permita determinar el
	//pago a la agencia de alquiler de autobuses y lo que debe pagar cada alumno por el viaje.
	
	Definir alumnos como Entero;
	Definir precioPasaje, totalViaje Como Real
	Escribir "Ingrese la cantidad de alumnos:"
	Leer alumnos;
	
	Si alumnos >= 100 Entonces
		precioPasaje <- 65;
		totalViaje <- alumnos * precioPasaje;
		Escribir "El precio del pasaje por alumno ser�: $", precioPasaje;
		Escribir "El costo total del viaje ser�: $", totalViaje;		
	Fin Si
	
	Si alumnos >= 50 Y alumnos <= 99 Entonces
		precioPasaje <- 70;
		totalViaje <- alumnos * precioPasaje;
		Escribir "El precio del pasaje por alumno ser�: $", precioPasaje;
		Escribir "El costo total del viaje ser�: $", totalViaje;		
	Fin Si
	
	Si alumnos >= 30 Y alumnos <= 49 Entonces
		precioPasaje <- 95;
		totalViaje <- alumnos * precioPasaje;
		Escribir "El precio del pasaje por alumno ser�: $", precioPasaje;
		Escribir "El costo total del viaje ser�: $", totalViaje;		
	Fin Si
	
	Si alumnos < 30 Entonces
		precioPasaje <- 4000 / alumnos;
		totalViaje <- 4000;
		Escribir "El precio del pasaje por alumno ser�: $", precioPasaje;
		Escribir "El costo total del viaje ser�: $", totalViaje;		
	Fin Si
	
	
FinAlgoritmo
