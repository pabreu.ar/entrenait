Algoritmo CalcularPromedioNotas
	//Un estudiante realiza cuatro ex�menes, los cuales tienen la misma
	//ponderaci�n. Realizar el pseudoc�digo y el diagrama de flujo que representen el algoritmo
	//correspondiente para obtener el promedio de las calificaciones obtenidas.

	Definir nota, acumula como Real;
	Escribir "Ingrese las notas:"
	acumula <- 0;
	Para i <- 0 Hasta 3 Con Paso 1 Hacer
		Leer Nota;
		acumula <- acumula + nota;
	Fin Para
	Definir promedio Como Real;
	promedio = acumula / 4;
	Escribir "El promedio de las notas es: ", promedio;
	
FinAlgoritmo
