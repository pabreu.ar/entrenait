Algoritmo CalcularAreaTerreno
	//Una empresa constructora vende terrenos con la forma A de la figura. Realizar
	//un algoritmo y repres�ntelo mediante un diagrama de flujo y el pseudoc�digo para obtener
	//el �rea respectiva de un terreno de medidas de cualquier valor.

	Definir A, B, C Como Real;
	Escribir "Ingrese la longitud del lado A: ";
	Leer A;
	Escribir "Ingrese la longitud del lado B: ";
	Leer B;
	Escribir "Ingrese la longitud del lado C: ";
	Leer C;
	
	Definir D, areaRectangulo, areaTriangulo Como Real;
	//El lado D se calcula A-C y corresponde a la altura del tri�ngulo rect�ngulo.
	D <- A - C;
	areaRectangulo <- B * C;
	areaTriangulo <- (B * D) / 2
	//Escribir "El �rea del rect�ngulo es: ", areaRectangulo;
	//Escribir "El �rea del tri�ngulo es: ", areaTriangulo;
	
	Definir areaTotal Como Real;
	areaTotal <- areaRectangulo + areaTriangulo;
	
	Escribir "El �rea del terreno es: ", areaTotal;
FinAlgoritmo
