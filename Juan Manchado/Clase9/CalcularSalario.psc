Algoritmo CalcularSalario
	// Calcular el nuevo salario de un empleado si obtuvo un incremento del 8%
	//sobre su salario actual y un descuento de 2,5% por servicios.

	Definir salarioActual, porcentajeIncremento, porcentajeDescuento Como Real;
	porcentajeIncremento <- 0.08;
	porcentajeDescuento <- 0.025;
	
	Escribir "Ingrese su salario actual: ";
	Leer salarioActual;
	
	Definir incremento, descuento, salarioNuevo Como Real;
	incremento <- salarioActual * porcentajeIncremento;
	descuento <- salarioActual * porcentajeDescuento;
	salarioNuevo <- salarioActual + incremento - descuento;
	
	Escribir "El nuevo salario del empleado es: ", salarioNuevo;
FinAlgoritmo
