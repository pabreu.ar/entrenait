Funcion inicializarMatriz(matriz Por Referencia, n Por Valor, m Por Valor)
	matriz[1,1] = "Argentina"; // aca cargamos del primer registro o fila, su origen.
	matriz[1,2] = "Brazil"; // aca cargamos del primer registro o fila, su destino.
	matriz[1,3] = "2022/04/16 09:15"; // aca cargamos del primer registro o fila, su fecha y horario.
	matriz[1,4] = "$3500"; // aca cargamos del primer registro o fila, su precio.
	
	matriz[2,1] = "Argentina"; 
	matriz[2,2] = "Paraguay"; 
	matriz[2,3] = "2022/04/16 19:15"; 
	matriz[2,4] = "$800";
	
	matriz[3,1] = "Chile"; 
	matriz[3,2] = "Uruguay"; 
	matriz[3,3] = "2022/06/18 11:15"; 
	matriz[3,4] = "$2800"; 
	
	matriz[4,1] = "Uruguay"; 
	matriz[4,2] = "Argentina"; 
	matriz[4,3] = "2022/05/18 08:15"; 
	matriz[4,4] = "$500"; 
	
	matriz[5,1] = "Argentina"; 
	matriz[5,2] = "Colombia"; 
	matriz[5,3] = "2022/04/18 07:15"; 
	matriz[5,4] = "$4500"; 
	
FinFuncion

Funcion position <- busquedaSecuencial(matriz Por Referencia, llave1 Por Valor, llave2 Por Valor, n Por Valor)
	Definir position Como Entero;
	position = -1;
	Definir i Como Entero;
	i=1;
	Mientras (position = -1 Y i <= n) Hacer
		Si (matriz[i, 1] = llave1 Y matriz[i, 2] = llave2) Entonces
			position = i;
		FinSi
		i = i + 1;
	FinMientras
FinFuncion

Algoritmo buscarVuelos
	//Ejercicio 6: La compa��a de aviaci�n Aerol�neas Argentinas mantiene la informaci�n de
	//todas sus rutas de vuelo en una base de datos con informaci�n como: lugares de origen y
	//destino, horario, valor; para ello utiliza una matriz en la que cada columna est� destinada a
	//uno de los datos mencionados. Dise�ar un algoritmo para averiguar si existe un vuelo entre
	//un origen y un destino introducidos por el usuario y en caso de existir obtener informaci�n
	//sobre el mismo.
	
	Definir n, m Como Entero;
	n = 5	// cantidad de filas
	m = 4;	// cantidad de columnas	// Columna 1: origen; columna 2: destino; columna 3: fecha y horario; columna 4: precio
	Dimension matrizVuelos[n,m];
	
	inicializarMatriz(matrizVuelos, n, m);
	
	Definir origen, destino Como Cadena;
	Escribir "Ingrese el origen del vuelo buscado";
	Leer origen;
	Escribir "Ingrese el destino del vuelo buscado";
	Leer destino;
	
	Definir posicion Como Entero;
	posicion = busquedaSecuencial(matrizVuelos, origen, destino, n);
	
	Si posicion <> -1 Entonces
		Escribir "El vuelo de ", matrizVuelos[posicion,1], " con destino a ", matrizVuelos[posicion,2], " parte ", matrizVuelos[posicion,3], " y cuesta ", matrizVuelos[posicion,4];
	SiNo
		Escribir "No se encontr� un vuelo de ", origen, " con destino a ", destino;
	FinSi
	
FinAlgoritmo
