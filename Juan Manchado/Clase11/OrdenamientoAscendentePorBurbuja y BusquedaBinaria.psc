Funcion OrdenamientoBurbujaAsc(arreglo Por Referencia, n Por valor)
		Definir i, j Como Entero;
		Para i <- 1 Hasta n - 1 Con Paso 1 Hacer
			Para j <- i + 1 Hasta n Con Paso 1 Hacer
				Si arreglo[i] > arreglo[j] Entonces
					auxChange <- arreglo[i]; 
					arreglo[i] <- arreglo[j]; 
					arreglo[j] <- auxChange;
				FinSi
			FinPara
		FinPara
FinFuncion

Funcion indice <- BusquedaBinaria(arreglo Por Referencia, n Por Valor, elemento Por Valor)
	indice <- -1;
	inferior <- 1;
	superior <- n;
	encontro <- Falso;
	
	Mientras encontro = Falso y inferior <= superior Hacer
		mitad <- Trunc((inferior + superior) / 2)
		Si elemento = arreglo[mitad] Entonces
			encontro = Verdadero
			indice <- mitad;
		Sino 
			Si elemento < arreglo[mitad] Entonces
				superior <- mitad - 1;
			SiNo
				inferior <- mitad + 1;
			FinSi
		FinSi
	FinMientras
FinFuncion

Algoritmo OrdenamientoAscendentePorBurbuja
	Definir n como Entero;
	n <- 5;
	Dimension vector[n];
	vector[1] = 3;
	vector[2] = 8;
	vector[3] = 11;
	vector[4] = 22;
	vector[5] = 13;
	
	
	OrdenamientoBurbujaAsc(vector, n)
	Para i <- 1 Hasta n Con Paso 1 Hacer
		Escribir vector[i];
	FinPara
	
	Escribir "Ingrese el elemento que desea buscar: ";
	Leer num;
	
	//OrdenamientoBurbujaAsc(vector, n)
	indice <- BusquedaBinaria(vector, n, num) 
	
	Si indice <> -1 Entonces
		Escribir "El elemento se encuentra en la posici�n: ", indice;
	SiNo
		Escribir "El elemento no se encuentra";
	FinSi
FinAlgoritmo
