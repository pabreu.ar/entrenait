Algoritmo MostrarQuinceLineas
	// Programa que muestra 15 l�neas como estas: 1 12 123 1234
	Definir n Como Entero;
	n = 15;
	
	Definir resultado como Cadena;
	resultado <- "1";
	
	Para i<-2 Hasta n Con Paso 1 Hacer
		resultado <- resultado + ConvertirATexto(i)
		Escribir resultado;
	Fin Para
	
	
FinAlgoritmo







