Funcion escribirArreglo (productos por Referencia, precios por Referencia, sizeArray por Valor, pepito por Valor)
	
	Definir j Como Entero
		
	Para j<-1 Hasta sizeArray Hacer
		Escribir "El precio de ",productos[j]," es:", precios[j];
	FinPara
	Escribir "El precio final del plato es: ",pepito
	
FinFuncion

Algoritmo PreparacionPlatoYCosto
	//Dise�e un arreglo en el que se ingrese la cantidad de productos y sus
	//respectivos precios, para la preparaci�n de un plato, tambi�n se debe mostrar al final el
	//costo a gastar.
	Definir n como Entero;
	Escribir "Ingrese la cantidad de productos:"
	Leer n
	Dimension arrProductos[n]
	Dimension arrPrecioProd[n]
	
	//Definir producto como Entero;
	//Definir precioProd Como Caracter;
	Definir suma Como Real
	suma <-0
	
	Para i<-1 Hasta n Hacer
		Escribir "Ingrese producto: ", i;
		Leer arrProductos[i];
		Escribir "Ingrese precio producto: ", i;
		Leer arrPrecioProd[i];
		suma = suma + arrPrecioProd[i]
	FinPara
	
	
	escribirArreglo(arrProductos, arrPrecioProd, n, suma)	
	
FinAlgoritmo
