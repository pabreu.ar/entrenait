Funcion muestraArreglo( verArreglo Por Referencia, cantidad Por Valor )
		Escribir "El arreglo completo es :"
		Definir j Como Entero;
		
		Para j<-1 Hasta cantidad Con Paso 1 Hacer
			Escribir verArreglo[j];
		FinPara
		
Fin Funcion

Algoritmo CrearArregloYMostrarlo
	// Ingresar 5 n�meros, almacenarlos en un arreglo y mostrarlos.
	Definir num como Entero;
	Definir cant Como Entero;
	
	Escribir "Ingrese la cantidad de elementos del arreglo:"
	Leer cant;
		
	Dimension arreglo[cant];
	
	Para i<-1 Hasta cant Con Paso 1 Hacer
		Escribir "Ingrese n�mero ",i," :"
		Leer num
		arreglo[i] <- num;
	Fin Para
	
	muestraArreglo(arreglo, cant)
	
	
FinAlgoritmo


