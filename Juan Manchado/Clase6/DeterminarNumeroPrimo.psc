Algoritmo DeterminarNumeroPrimo
	Definir N como entero;
	Leer N;
	// Un n�mero primo es aquel > 1 que es divisible s�lo por s� mimso y por la unidad.
	Definir esPrimo como Logico;
	esPrimo = verdadero;
	Definir divisor como Numero;
	divisor = 2;
	Mientras divisor < N Y esPrimo Hacer
		esPrimo = (N MOD divisor) <> 0;
		divisor = divisor + 1;
	FinMientras
	Si esPrimo = verdadero Entonces
		Escribir "El n�mero es primo"
	SiNo
		Escribir "El n�mero no es primo"
	FinSi
FinAlgoritmo
