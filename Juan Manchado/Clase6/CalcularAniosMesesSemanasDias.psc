Algoritmo CalculoA�osMesesSemanasDias
	Escribir 'Ingrese un n�mero'
	Leer cantDias
	Definir diasRestantes como Entero
	Definir anios Como Entero
	anios = Trunc(cantDias / 365) 
	diasRestantes = cantDias - anios * 365
	Definir meses Como Entero
	// Se considera un mes de 30 d�as
	meses <- Trunc(diasRestantes / 30)
	diasRestantes = diasRestantes - meses * 30
	Definir semanas Como Entero
	semanas = Trunc(diasRestantes / 7)
	diasRestantes = diasRestantes - semanas * 7
	Escribir "Es equivalente a: ",anios," a�os, ",meses," meses, ",semanas," semanas y ",diasRestantes," d�as."
FinAlgoritmo
