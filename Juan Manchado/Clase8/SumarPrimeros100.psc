Algoritmo SumarPrimeros100
	// Hacer un algoritmo en Pseint para calcular la suma de los primeros cien n�meros con un ciclo Do-While.
	Definir suma, contador Como Entero
	contador <- 0;
	suma <- 0;
	
	Mientras contador < 100 Hacer
		contador <- contador + 1;
		suma <- suma + contador;
				
	Fin Mientras
	
	Escribir suma;
	
FinAlgoritmo
