Funcion InicializarBaseDeDatos(cedula Por Referencia, nombre)
	cedula[1] <- 101010;
	nombre[1] <- "Pedro";

	cedula[2] <- 202020;
	nombre[2] <- "Juan";
	
	cedula[3] <- 303030;
	nombre[3] <- "Carlos";
FinFuncion

Funcion pos <- BusquedaSecuencial(arreglo Por Referencia numMax Por Valor key Por Referencia)
	//Devuelve -1 en caso de que no lo encuentre
	pos <- -1;
	Definir i Como Entero;
	i <- 1;
	Mientras pos = -1 Y i <= numMax Hacer
		Si arreglo[i] = key Entonces
			pos = i
		Fin Si
		i = i + 1
	Fin Mientras
Fin Funcion

Algoritmo SufragioVotaciones
	//El d�a de elecciones, en cada mesa se cuenta con un listado de las personas
	//que votar�n en dicha mesa. Los datos est�n guardados en dos vectores, en el primero la c�dula y en el segundo el nombre. 
	
	Definir cedula1 Como Entero;
	Escribir "Ingrese el numero de cedula";
	Leer cedula1;
	
	Definir cantPersonas Como Entero;
	cantPersonas = 3;
	
	Dimension cedula[cantPersonas];
	Dimension nombre[cantPersonas];
	
	InicializarBaseDeDatos(cedula, nombre);
	
	Definir indice Como Entero;
	indice = BusquedaSecuencial(cedula, cantPersonas, cedula1);
	
	Si indice <> -1 Entonces
		Escribir "El cliente con cedula:",cedula[indice], " sufragia en este puesto de votacion";
	SiNo
		Escribir "No se encontr� a la persona";
	Fin Si
FinAlgoritmo
