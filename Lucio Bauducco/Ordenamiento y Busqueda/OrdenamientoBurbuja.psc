Funcion ordenarBurbujaAsc(arreglo Por Referencia, n Por Valor)
	Definir i,j Como Entero;
	Para i<- 1 Hasta n-1 Con Paso 1 Hacer
		Para j<- i + 1 Hasta n Con Paso 1 Hacer
			Si arreglo[i] > arreglo[j] Entonces
				auxChange <- arreglo[i];
				arreglo[i]<- arreglo[j];
				arreglo[j]<- auxChange;
			FinSi
		FinPara
	FinPara
FinFuncion

Algoritmo OrdenamientoBurbuja
	Definir n Como Entero;
	n<- 10;
	Dimension arreglo[n];
	arreglo[1]<-2;
	arreglo[2]<-6;
	arreglo[3]<-3;
	arreglo[4]<-1;
	arreglo[5]<-4;
	arreglo[6]<-7;
	arreglo[7]<-10;
	arreglo[8]<-8;
	arreglo[9]<-5;
	arreglo[10]<-9;
	
	ordenarBurbujaAsc(arreglo,n);
	
	Escribir "El arreglo ordenado";
	Para s<- 1 Hasta n Hacer
		Escribir arreglo[s];
	FinPara
FinAlgoritmo
