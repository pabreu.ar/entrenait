Funcion InicioBaseDeDatos( nroCuenta, nombre, saldo Por Referencia )
	nroCuenta[1] = 123456789;
	nombre[1] = "Bruno";
	saldo[1] = 15000;
	
	nroCuenta[2] = 987654321;
	nombre[2] = "Lucio";
	saldo[2] = 23000;
	
	nroCuenta[3] = 42530283;
	nombre[3] = "Angela";
	saldo[3] = 42325;
Fin Funcion

Funcion pos <- BusquedaSecuencial(arreglo Por Referencia numMax Por Valor key Por Referencia)
	//Devuelve -1 en caso de que no lo encuentre
	pos <- -1;
	Definir i Como Entero;
	i <- 1;
	Mientras pos = -1 Y i <= numMax Hacer
		Si arreglo[i] = key Entonces
			pos = i
		Fin Si
		i = i + 1
	Fin Mientras
Fin Funcion

Algoritmo ConsultarSaldo
	// En un hipot�tico cajero electr�nico, cuando un cliente solicita el saldo de su
	//cuenta desliza la tarjeta, el lector toma el n�mero de cuenta y el sistema busca dicho
	//n�mero en la base de datos. Si lo encuentra reporta el saldo, si no muestra un mensaje de error
	
	Definir numeroCuenta Como Entero;
	Escribir "Ingrese el numero de cuenta";
	Leer numeroCuenta;
	
	Definir cantClientes Como Entero;
	cantClientes <- 3;
	
	Dimension nroCuenta[cantClientes];
	Dimension nombre[cantClientes];
	Dimension saldo[cantClientes];
	
	InicioBaseDeDatos(nroCuenta, nombre, saldo);
	
	Definir indice Como Entero;
	indice = BusquedaSecuencial(nroCuenta, cantClientes, numeroCuenta);
	
	Si indice <> -1 Entonces
		Escribir "El saldo del cliente es de $", saldo[indice];
	SiNo
		Escribir "No se encontr� al cliente";
	Fin Si
FinAlgoritmo
