Funcion pos <- BusquedaSecuencial(arreglo Por Referencia numMax Por Valor key Por Referencia)
	//Devuelve -1 en caso de que no lo encuentre
	pos <- -1;
	Definir i Como Entero;
	i <- 1;
	Mientras pos = -1 Y i <= numMax Hacer
		Si arreglo[i] = key Entonces
			pos = i
		Fin Si
		i = i + 1
	Fin Mientras
Fin Funcion

Funcion InicioBaseDeDatos( codigo, nombre, nota Por Referencia )
	//Funci�n con los datos de los estudiantes"
	codigo[1] = 001;
	nombre[1] = "Bruno";
	nota[1] = 6;
	
	codigo[2] = 002;
	nombre[2] = "Walter";
	nota[2] = 9;
	
	codigo[3] = 003;
	nombre[3] = "Juan";
	nota[3] = 4;
Fin Funcion

Algoritmo DatosDeEstudiantes
	//Un profesor guarda los datos de sus estudiantes en tres vectores: c�digo,
	//nombre y nota. Se requiere un algoritmo para consultar la nota de un estudiante a partir de su c�digo.
	
	Definir codigo1 Como Entero;
	Escribir "Ingrese el codigo del estudiante";
	Leer codigo1;
	
	//Cantidad maxima de estudiantes
	Definir matricula Como Entero;
	matricula <- 3
	
	Dimension codigo[matricula];
	Dimension nombre[matricula];
	Dimension nota[matricula];
	
	InicioBaseDeDatos(codigo, nombre, nota)
	
	Definir indice Como Entero;
	indice = BusquedaSecuencial(codigo, matricula, codigo1);
	
	Si indice <> -1 Entonces
		Escribir "La nota del alumno es ", nota[indice];
	SiNo
		Escribir "No se encontr� al alumno";
	Fin Si
FinAlgoritmo
