Funcion InicializarBaseDeDatos(numeroFactura Por Referencia, nombreCliente, fechaFacturacion, valorFactura)
	numeroFactura[1] <- 1111;
	nombreCliente[1] <- "Juana";
	fechaFacturacion[1] <- "15/04/22";
	valorFactura[1] <- 5000;
	
	numeroFactura[2] <- 2222;
	nombreCliente[2] <- "Lucio";
	fechaFacturacion[2] <- "01/01/22";
	valorFactura[2] <- 8900;
	
	numeroFactura[3] <- 3333;
	nombreCliente[3] <- "Santino";
	fechaFacturacion[3] <- "21/12/21";
	valorFactura[3] <- 15000;
FinFuncion

Funcion pos <- BusquedaSecuencial(arreglo Por Referencia numMax Por Valor key Por Referencia)
	//Devuelve -1 en caso de que no lo encuentre
	pos <- -1;
	Definir i Como Entero;
	i <- 1;
	Mientras pos = -1 Y i <= numMax Hacer
		Si arreglo[i] = key Entonces
			pos = i
		Fin Si
		i = i + 1
	Fin Mientras
Fin Funcion

Algoritmo MostrarFacturasAlmacen
	//En el almac�n TodoCaro se cuenta con los datos: n�mero de factura, nombre
	//del cliente, fecha de facturaci�n y valor de la factura, almacenados en vectores. Se desea
	//un algoritmo que lea el n�mero de factura y muestre los dem�s datos.
	Definir nroFactura Como Entero;
	Escribir "Ingresar el numero de factura";
	Leer nroFactura;
	
	Definir cantFacturas Como Entero;
	cantFacturas <- 3;
	
	Dimension numeroFactura[cantFacturas];
	Dimension nombreCliente[cantFacturas];
	Dimension fechaFacturacion[cantFacturas];
	Dimension valorFactura[cantFacturas];
	
	InicializarBaseDeDatos(numeroFactura, nombreCliente, fechaFacturacion, valorFactura);
	
	Definir indice Como Entero;
	indice = BusquedaSecuencial(numeroFactura, cantFacturas, nroFactura);
	
	Si indice <> -1 Entonces
		Escribir "Los datos de la factura ", nroFactura, " son: Nombre: ",nombreCliente[indice], " Fecha: ", fechaFacturacion[indice], " Valor: ", valorFactura[indice];
	SiNo
		Escribir "No se encontr� la factura";
	Fin Si
FinAlgoritmo
