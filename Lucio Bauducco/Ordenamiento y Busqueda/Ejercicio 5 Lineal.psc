Funcion pos <- BusquedaSecuencial(arreglo, numMax, key)
	pos <- -1;
	Definir i Como Entero;
	i <- 1;
	Mientras pos = -1 Y i <= numMax Hacer
		Si arreglo[i] = key Entonces
			pos = i;
		Fin Si
		i <- i + 1;
	Fin Mientras
Fin Funcion

Funcion  InicioBaseDeDatos(origen, destino, fechaLlamada, horaLlamada, duracionLlamada)
	origen[1] = 3493441812;
	destino[1] = 34935523;
	fechaLlamada[1] = "15/09/2022";
	horaLlamada[1] = "09:00";
	duracionLlamada[1] = 12;
	
	origen[2] = 349355555;
	destino[2] = 349366666;
	fechaLlamada[2] = "11/04/2019";
	horaLlamada[2] = "15:00";
	duracionLlamada[2] = 23;
Fin Funcion

Algoritmo RegistroLlamadas
	//La empresa de comunicaciones L�nea Segura registra el origen, el destino, la
	//fecha, la hora y la duraci�n de todas las llamadas que hacen sus usuarios, para esto hace
	//uso de cinco vectores. Se requiere un algoritmo para saber si desde un tel�fono
	//determinado se ha efectuado alguna llamada a un destino y en caso afirmativo conocer la
	//fecha, la hora y la duraci�n de la llamada.
	
	Definir numeroTelefono Como Entero;
	Escribir "Ingrese el numero del telefono";
	Leer numeroTelefono;
	
	Definir cantLlamadas Como Entero;
	cantLlamadas <- 5;
	
	Dimension telefonoOrigen[cantLlamadas];
	Dimension telefonoDestino[cantLlamadas];
	Dimension fechaLlamada[cantLlamadas];
	Dimension horaLlamada[cantLlamadas];
	Dimension duracionLlamada[cantLlamadas];
	
	InicioBaseDeDatos( telefonoOrigen, telefonoDestino, fechaLlamada, horaLlamada, duracionLlamada);
	
	Definir resultado Como Entero;
	resultado <- BusquedaSecuencial(telefonoOrigen, cantLlamadas, numeroTelefono);
	
	Si resultado <> -1 Entonces
		Escribir "El n�mero: ", telefonoOrigen[resultado], " llam� al n�mero ", telefonoDestino[resultado], " en la fecha: ", fechaLlamada[resultado], " Y en la hora: ", horaLlamada[resultado], " con una duraci�n de ", duracionLlamada[resultado], " minutos";
	SiNo
		Escribir "No se encontraron llamadas registradas";
	Fin Si

FinAlgoritmo
