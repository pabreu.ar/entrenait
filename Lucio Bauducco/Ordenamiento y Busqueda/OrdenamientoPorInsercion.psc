Funcion OrdenarPorInsercionFormaLarga(vector Por Referencia, n Por Valor)
	Definir j, i, valorAnterior, valorActual Como Entero;
	Para i <- 2 Hasta n Con Paso 1 Hacer
		valorActual <- vector[i];
		j <- i-1;
		valorAnterior = vector[j];
		Mientras valorActual < vector[j] Hacer
			//intercambio posicion si es menor
			vector[j] = valorActual;
			vector[j+1] = valorAnterior;
			//Tomar proximo valor a comparar
			j = j - 1;
			Si j <> 0 Entonces
				valorAnterior = vector[j];
			FinSi
		Fin Mientras
	FinPara
FinFuncion
Funcion OrdenarPorInsercionFormaOptima(vector Por Referencia, n Por Valor)
	Definir j, i, valorAnterior, valorActual Como Entero;
	Para i <- 2 Hasta n Con Paso 1 Hacer
		valorActual <- vector[i];
		j <- i;
		Mientras j > 0 Y valorActual < vector[j - 1] Hacer
			//intercambio posicion si es menor
			vector[j] = vector[j - 1];
			j = j - 1;
		Fin Mientras
		vector[j] = valorActual;
	FinPara
FinFuncion

Algoritmo OrdenamientoPorInsercion
	Definir n como Entero;
	n <- 10;
	Dimension vector[n];
	vector[1] = 1;
	vector[2] = 3;
	vector[3] = 6;
	vector[4] = 8;
	vector[5] = 2;
	vector[6] = 5;
	vector[7] = 9;
	vector[8] = 10;
	vector[9] = 4;
	vector[10] = 7;
	
	OrdenarPorInsercionFormaLarga(vector,n);
	
	Escribir "Vector Original";
	Para i<- 1 Hasta  n Hacer
		Escribir vector[i];
	FinPara
FinAlgoritmo