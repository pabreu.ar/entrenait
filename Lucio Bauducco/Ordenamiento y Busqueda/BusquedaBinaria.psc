Algoritmo BusquedaBinaria
	Definir puntero Como Entero;
	puntero<- 1;
	Definir final Como Entero;
	final<- 5;
	
	Dimension vec[5];
	vec[1]<- 3;
	vec[2]<- 8;
	vec[3]<- 11;
	vec[4]<- 22;
	vec[5]<- 13;
	
	Definir encontro Como Logico;
	encontro<- Falso;
	Definir numBusqueda,mitad Como Entero;
	Escribir "Ingresar numero a buscar";
	Leer numBusqueda;
	
	Mientras encontro = Falso Y puntero <= final Hacer
		mitad<- trunc((puntero + final) / 2);
		Si numBusqueda = vec[mitad] Entonces
			encontro<- Verdadero;
		SiNo
			Si numBusqueda < vec[mitad] Entonces
				final<- mitad - 1;
			SiNo
				puntero<- mitad + 1;
			Fin Si
		Fin Si
	FinMientras 
	
	Si encontro = Verdadero Entonces
		Escribir "El dato se encuentra en la posicion ", mitad;
	SiNo
		Escribir "El dato no se encuentra";
	Fin Si
FinAlgoritmo
