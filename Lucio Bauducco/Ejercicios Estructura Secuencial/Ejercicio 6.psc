Algoritmo InversionBancoInteres02
	//Suponga que un individuo desea invertir su capital en un banco y desea
	//saber cu�nto dinero ganar� despu�s de un mes si el banco paga a raz�n de 2% mensual.
	Definir montoInversion Como Real;
	Escribir "Ingrese el monto de inversion";
	Leer montoInversion;
	
	Definir mesInversion Como Entero;
	mesInversion<-1;
	
	Definir porcentajeInteres, retornoInversion, gananciaInversion Como Real;
	porcentajeInteres <- 0.02 * mesInversion;
	retornoInversion <- montoInversion * porcentajeInteres;
	gananciaInversion <- montoInversion + retornoInversion;
	
	Escribir  "El dinero del interes generado en 1 mes es de $ ", retornoInversion, " dando un total de $", gananciaInversion;
FinAlgoritmo
