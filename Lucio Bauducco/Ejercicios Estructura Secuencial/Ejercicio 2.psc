Algoritmo SumadeCifras
	//Escribir un programa que determine la suma de las cifras de un entero positivo de 4 cifras..
	Definir numero1 Como Entero;
	Escribir "Ingrese el numero de 4 cifras";
	Leer numero1;
	Definir millares, centena,decena,unidad,resto Como Entero;
	millares <- Trunc(numero1 / 1000);
	resto <- numero1 MOD 1000;
	centena <- Trunc(resto / 100);
	resto <- resto MOD 100;	
	decena <- Trunc(resto / 10);
	unidad <- resto MOD 10;
	Definir suma Como Entero;
	suma <- millares + centena + decena + unidad;
	Escribir "LA SUMA DE SUS CIFRAS ES: ", suma;
FinAlgoritmo
