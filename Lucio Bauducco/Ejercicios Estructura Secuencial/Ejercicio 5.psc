Algoritmo ConversionGrados
	//Escribir un programa para la conversión de grados sexagesimales a radianes y Centesimales..
	// Convertir sexagesimal a radianes: grados sexagesimales * pi / 180
	// Convertir sexagesimales a Centesimales: grados sexagesimales * 200 / 180
	// Mostrar resultados
	
	Definir sexagesimales Como Real;
	Escribir "Ingrese grados en sexagecimal";
	Leer sexagesimales;
	
	Definir radianes Como Real;
	radianes <- sexagesimales * pi / 180;
	
	Definir centesimales Como Real;
	centesimales <- sexagesimales * 200 / 180;
	
	Escribir "Los grados sexagesimales convertidos a radianes son: ", radiantes;
	Escribir "Los grados sexagesimales convertidos a centesimales son: ", centesimales;
FinAlgoritmo