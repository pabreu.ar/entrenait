Algoritmo InteresPrestamos
	//Calcula el Monto a devolver si nos prestan un capital c, a una tasa de inter�s t% durante n periodos.
	Definir prestamo, intereses Como Real;
	Escribir  "Ingrese el monto del prestamo";
	Leer prestamo;
	Escribir "Ingrese el % de inter�s";
	Leer interes;
	
	Definir tiempo Como Entero;
	Escribir "Ingrese el tiempo en meses";
	Leer tiempo;
	
	Definir montoInteres, montoTotal Como Real;
	montoInteres <- (prestamo * (interes / 100) * tiempo);
	montoTotal <- montoInteres + prestamo;
	
	Escribir "El monto a devolver es igual a ", montoTotal
FinAlgoritmo