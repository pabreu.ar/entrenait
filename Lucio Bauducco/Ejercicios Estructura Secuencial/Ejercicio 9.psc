Algoritmo PulsacionesEn10SegundosDeEjercicio
	// Calcular el n�mero de pulsaciones que una persona debe tener por cada 10
	//segundos de ejercicio, si la f�rmula es: num. pulsaciones = (220 - edad)/10
	Definir edad Como Entero;
	Escribir  "Ingrese la edad de la persona";
	Leer edad;
	
	Definir pulsaciones Como Real;
	pulsaciones <- (220 - edad) /10;
	
	Escribir "El numero de pulsaciones cada 10 segundos de ejercicio debe ser de: ", pulsaciones, " pulsasiones";
FinAlgoritmo