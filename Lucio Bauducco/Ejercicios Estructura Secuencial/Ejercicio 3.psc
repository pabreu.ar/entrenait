Algoritmo ConvertirTemperatura
	//Una temperatura Celsius (Centígrados) puede ser convertida a una temperatura equivalente F. Escriba un programa para calcular ambos valores
	Definir temperaturaInicial Como Real;
	Escribir "Ingrese los grados";
	Leer temperaturaInicial;	
	Definir unidad Como Caracter;
	Escribir "Ingrese si la unidad es Celsius (c) o Farenheit (f)";
	Leer unidad;
	Definir resultado Como Real;
	Si Mayusculas(unidad) = "C" Entonces
		resultado <- (temperaturaInicial * 9/5) + 32;
		Escribir "La temperatura ingresada equivale a: ", resultado, "F";
	FinSi
	Si Mayusculas(unidad) = "F" Entonces
		resultado <- (temperaturaInicial - 32) * 5/9;
		Escribir "La temperatura ingresada equivale a: ", resultado, "C";
	FinSi
	Si Mayusculas(unidad) <> "C" Y Mayusculas(unidad) <> "F" Entonces
		Escribir "El tipo de temperatura ingresada no es valida";
	FinSi
FinAlgoritmo
