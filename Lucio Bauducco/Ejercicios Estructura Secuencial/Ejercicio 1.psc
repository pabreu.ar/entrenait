Algoritmo ComisionPorVentas
	//Un vendedor recibe un sueldo base m�s un 10% extra por comisi�n de sus ventas. 
	//El vendedor desea saber cu�nto dinero obtendr� por concepto de comisiones por las tres ventas que realiza en el mes 
	//y el total que recibir� en el mes tomando en cuenta su sueldo base y sus comisiones.
	
	Definir sueldoBase Como Real;
	Escribir "Ingrese sueldo base";
	Leer sueldoBase;
	
	Definir valorVenta, comision, sueldoTotal Como Real;
	Definir i Como Entero;
	i <- 0;
	Mientras i < 3 Hacer
		Escribir "Ingrese el monto de la venta";
		Leer valorVenta;
		comision <- (valorVenta *10) /100;
		Escribir "La comisi�n correspondiente por la venta ", i " es de: " comision;
		sueldoTotal <- sueldoBase + comision;
		i <- i + 1;
	FinMientras
	
	Escribir "El sueldo total es de: ", (sueldoTotal + comision);
FinAlgoritmo