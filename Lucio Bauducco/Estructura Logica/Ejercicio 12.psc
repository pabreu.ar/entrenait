Algoritmo CalculoPorcentajeInversion
	// Tres personas deciden invertir su dinero para fundar una empresa. Cada
	//una de ellas invierte una cantidad distinta. Obtener el porcentaje que cada quien invierte
	//con respecto a la cantidad total invertida.
	Definir persona1,persona2,persona3 Como Cadena;
	Definir montoInversion1, montoInversion2, montoInversion3 Como Real;
	
	Escribir "Ingrese el nombre de una persona";
	Leer persona1;
	Escribir "Ahora el monto que invirti�";
	Leer montoInversion1;
	Escribir "Ingrese el nombre de una persona";
	Leer persona2;
	Escribir "Ahora el monto que invirti�";
	Leer montoInversion2;
	Escribir "Ingrese el nombre de una persona";
	Leer persona3;
	Escribir "Ahora el monto que invirti�";
	Leer montoInversion3;
	
	Definir totalInversion Como Real;
	totalInversion<- montoInversion1 + montoInversion2 + montoInversion3;
	
	Escribir persona1, " Invirti� un ", (montoInversion1*100/totalInversion), "% del total";
	Escribir persona2, " Invirti� un ", (montoInversion2*100/totalInversion),"% del total";
	Escribir persona3, " Invirti� un ", (montoInversion3*100/totalInversion),"% del total";
FinAlgoritmo
