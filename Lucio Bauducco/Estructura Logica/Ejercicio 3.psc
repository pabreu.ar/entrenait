Algoritmo NumPositivoNegativo
	//Determinar el algoritmo para saber si un n�mero es positivo o negativo
	Definir num Como Real;
	Escribir "Ingrese un n�mero";
	Leer num;
	
	Si num >= 0 Entonces
		Escribir "El numero ingresado es positivo";
	SiNo
		Escribir "El numero ingresado es negativo";
	FinSi
FinAlgoritmo