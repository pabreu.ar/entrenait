Algoritmo CalcularEdad
	//Mostrar la edad de una persona, ingresando el a�o de nacimiento y el a�o actual.
	Definir a�oActual,a�oNacimiento Como Entero;
	Escribir "Ingrese el a�o de nacimiento de la persona";
	Leer a�oNacimiento;
	Escribir "Ingrese el a�o actual";
	Leer a�oActual;
	
	Definir edad Como Entero;
	edad<- a�oActual - a�oNacimiento;
	
	Escribir "La edad de la persona es ", edad;
FinAlgoritmo
