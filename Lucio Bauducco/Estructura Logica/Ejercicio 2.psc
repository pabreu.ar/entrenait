Algoritmo calclculoAreaTerreno
	//Una empresa constructora vende terrenos con la forma A de la figura.obtener
	//el �rea respectiva de un terreno de medidas de cualquier valor
	Definir ladoA,ladoB,ladoC Como Real;
	Escribir "Ingrese la longitud del lado A";
	Leer ladoA;
	Escribir "Ingrese la longitud del lado B";
	Leer ladoB;
	Escribir "Ingrese la longitud del lado C";
	Leer ladoC;
	
	Definir D,areaRectangulo,areaTriangulo Como Real;
	//El lado D se calcula A-C y corresponde a la altura del triangulo
	D<- A - C;
	areaRectangulo<- B * C;
	areaTriangulo<- (B * D) / 2;
	Escribir "El area del rectangulo es ", areaRectangulo;
	Escribir "El area del triangulo es ", areaTriangulo;
	
	Definir areaTotal como Real;
	areaTotal<- areaRectangulo + areaTriangulo;
	
	Escribir "El area del terreno es ", areaTotal;
FinAlgoritmo
