Algoritmo CabinaInternet
	//Calcular el monto a pagar en una cabina de Internet si el costo por hora es de
	//S/.1,5 y por cada 5 horas te dan una hora de promoción gratis, sabiendo que la
	//permanencia en la cabina fue de 12 horas.
	Definir cantidadHoras Como Entero;
	Escribir "Ingrese la cantidad de horas de permanencia en la cabina";
	Leer cantidadHoras;
	
	Definir montoAPagar, horaGratis Como Real;
	horaGratis<- trunc(cantidadHoras / 6);
	montoAPagar<- (cantidadHoras - horaGratis) * 1.5;
	Escribir "El monto a pagar es de $", montoAPagar;
FinAlgoritmo
