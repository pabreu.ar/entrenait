Algoritmo CalculoNuevoSalario
	//Calcular el nuevo salario de un empleado si obtuvo un incremento del 8% sobre su salario actual y un descuento de 2,5% por servicios
	Definir salarioEmpleado Como Real;
	Escribir "Ingrese el salario del empleado";
	Leer salarioEmpleado;
	
	Definir salarioNuevo Como Real;
	//8% de incremento - 2.5% de descuento = 5.5%
	salarioNuevo<- salarioEmpleado + ((salarioEmpleado/100)* 5.5);
	
	Escribir "El nuevo salario corresponde a $", salarioNuevo;
FinAlgoritmo
