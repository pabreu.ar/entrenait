Algoritmo CalcularPromedio
	//Se deben ingresar las 4 calificaciones de los examenes
	//Calcular su promedio
	//Mostrar en pantalla el resultado obtenido
	Definir nota1,nota2,nota3,nota4 Como Real;
	Escribir "Ingrese las 4 calificaciones";
	Leer nota1,nota2,nota3,nota4;
	
	Definir promedio Como Real;
	promedio<- (nota1+nota2+nota3+nota4) / 4;
	Escribir "El promedio obtenido es ", promedio;
FinAlgoritmo