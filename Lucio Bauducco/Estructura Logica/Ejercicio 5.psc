Algoritmo CobrodeViaje
	//si son 100 alumnos o m�s, el importe por cada alumno es de 65 pesos; de 50 a 99 alumnos, e es de 70 pesos, de 30 a 49, de 95 pesos, y si son menos de 30
	Definir cantAlumnos Como Entero;
	Escribir "Ingrese la cantidad de alumnos";
	Leer cantAlumnos;
	
	Definir importe Como Entero;
	Si cantAlumnos >= 100 Entonces
		importe<- 65;
	FinSi
	Si cantAlumnos >= 50 Y cantAlumnos <= 99 Entonces
		importe<- 70;
	FinSi
	Si cantAlumnos >= 30 Y cantAlumnos <= 49 Entonces
		importe<- 95;
	FinSi
	Si cantAlumnos < 30 Entonces
		Escribir "El valor del servicio es de $4000";
	FinSi
	Definir valorViaje Como Real
	valorViaje<- importe * cantAlumnos;
	Escribir "El valor del servicio es de $", valorViaje;
FinAlgoritmo