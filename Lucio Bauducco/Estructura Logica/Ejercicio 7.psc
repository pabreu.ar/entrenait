Algoritmo AreaTrapecio
	//Calcular el �rea de un trapecio
	Definir base1,base2,altura Como Real;
	Escribir "Ingrese una base del trapecio";
	Leer base1;
	Escribir "Ingrese otra de las bases del trapecio";
	Leer base2;
	Escribir "Ingrese la altura del trapecio";
	Leer altura;
	
	Definir area Como Real;
	area<- (base1+base2)*altura;
	
	Escribir "El area del trapecio es ", area;
FinAlgoritmo
