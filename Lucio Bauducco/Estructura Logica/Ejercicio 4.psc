Algoritmo CalcularPrecioLapices
	//Si son 1000 o m�s el costo es de 85 pesos; de lo contrario, el precio es de 90 persos
	Definir cantLapices Como Entero;
	Escribir "Ingrese la cantidad de lapices que desea comprar";
	Leer cantLapices;
	
	Definir precioLapiz,precioTotal Como Entero;
	Si cantLapices >= 1000 Entonces
		precioLapiz <- 85;
		precioTotal <- precioLapiz*cantLapices;
	SiNo
		precioLapiz <- 90;
		precioTotal <- precioLapiz*cantLapices;
	FinSi

	Escribir "El precio total de los lapices es de $", precioTotal;
FinAlgoritmo
