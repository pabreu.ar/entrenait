Algoritmo DeterminarMayoryMenor
	Definir a,b,c Como Real
	Escribir "Ingrese el primer numero"
	Leer a
	Escribir "Ingrese el segundo numero"
	Leer b
	Escribir "Ingrese el tercer numero"
	Leer c
	Si a=b o a=c o b=c  Entonces
		Escribir "Los numeros ingresados son iguales"
	SiNo
		Definir maxim, minim Como Real
		Si a>b Entonces
			maxim <- a
			minim <- b
		SiNo
			maxim <- b
			minim <- a
		FinSi
		Si c>maxim Entonces
			maxim <- c
		SiNo
			Si c<minim Entonces
				minim <- c
			FinSi
		FinSi
		Escribir "El mayor es: " maxim
		Escribir "El menor es: " minim
	FinSi
FinAlgoritmo
