Algoritmo OrdenarDeMenorAMayor
	Definir numero1, numero2 Como Real
	Escribir "Ingrese un n�mero"
	Leer numero1
	Escribir "Ingrese otro n�mero"
	Leer numero2
	Si numero1 = numero2 Entonces
		Escribir "Los n�meros ingresados son iguales"
	SiNo
		Definir resultado Como Cadena
		Si numero1 > numero2 Entonces
			resultado <- Concatenar(ConvertirATexto(numero1),Concatenar(",",ConvertirATexto(numero2)))
		SiNo
			resultado <- Concatenar(ConvertirATexto(numero2),Concatenar(",",ConvertirATexto(numero1)))
		FinSi
		Escribir "Los numeros ordenados de mayor a menor: " , resultado
	FinSi
FinAlgoritmo
