Algoritmo DeterminarNumeroPrimo
	Definir num Como Entero
	Repetir
		Escribir "Ingrese un n�mero entero"
		Leer num
	Hasta Que num>0
	Definir contador Como Entero
	contador <- 0
	Para i<-1 Hasta num Hacer
		Si num MOD i = 0 Entonces
			contador <- contador+1
		FinSi
	FinPara
	Si contador=2 Entonces
		Escribir "El n�mero es primo"
	SiNo
		Escribir "El n�mero no es primo"
	FinSi
FinAlgoritmo
