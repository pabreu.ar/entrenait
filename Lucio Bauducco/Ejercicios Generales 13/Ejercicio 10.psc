Algoritmo CalcularCantidadDeCajas
	//Juan Carlos es jefe de bodega en una f�brica de pa�ales desechables y
	//sabe que la producci�n diaria es de 744 pa�ales y que en cada caja donde se empacan
	//para la venta caben 12 pa�ales. �Cu�ntas cajas debe conseguir Juan Carlos para empacar
	//los pa�ales fabricados en una semana (5 d�as)?
	Definir cantDias Como Entero;
	Escribir "Ingrese la cantidad de dias de fabricacion";
	Leer cantDias;
	
	Definir produccionDiaria, capacidadCaja, cajasPorDia, cajasEnXDias Como Entero;
	produccionDiaria <- 744;
	capacidadCaja <- 12;
	cajasPorDia <- produccionDiaria / capacidadCaja;
	cajasEnXDias <- cajasPorDia * cantDias;
	
	Escribir "Juan Carlos necesitar� ", cajasEnXDias, " cajas para empacar los pa�ales fabricados en ", cantDias, " dias";
FinAlgoritmo
