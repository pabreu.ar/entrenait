Algoritmo Escribir15Lineas
	//Programa que muestra 15 l�neas como estas: 1 12 123 1234
	Definir n Como Entero;
	n <- 15;
	
	Definir resultado Como Cadena;
	resultado <- "1";
	Escribir resultado;

	Para i <- 2 Hasta n Con Paso 1 Hacer
		resultado <- resultado + ConvertirATexto(i);
		Escribir resultado;
	FinPara
FinAlgoritmo
