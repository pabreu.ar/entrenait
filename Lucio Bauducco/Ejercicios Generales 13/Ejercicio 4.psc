Algoritmo CalculoResultadoAlumno
	//Escribir un algoritmo para calcular la nota final de un estudiante, considerando
	//que: por cada respuesta correcta 5 puntos, por una incorrecta -1 y por respuestas en blanco
	//0. Imprime el resultado obtenido por el estudiante
	Definir rtasCorrectas, rtasIncorrectas Como Entero;
	
	Escribir "Ingrese la cantidad de respuestas correctas";
	Leer rtasCorrectas;
	Escribir "Ingrese la cantidad de respuestas incorrectas";
	Leer rtasIncorrectas;
	//las rtas en blanco no suman ni restan puntos, por lo que no se tienen en cuenta
	
	Definir puntosRtasCorrectas, puntosRtasIncorrectas, puntosRtasEnBlanco Como Entero;
	puntosRtasCorrectas <- 5;
	puntosRtasIncorrectas <- -1;
	
	
	Definir resultado Como Entero;
	resultado <- (rtasCorrectas * puntosRtasCorrectas) + (rtasIncorrectas * puntosRtasIncorrectas);
	
	Escribir "El estudiante alcanz� el resultado :", resultado;
FinAlgoritmo
