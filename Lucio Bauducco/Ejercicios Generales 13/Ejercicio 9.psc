Funcion CalcularMonedas(monedas Por Referencia, cantMonedas Por Valor, cantDinero Por Valor)
	Definir valorMoneda Como Entero;
	Para i = 1 Hasta cantMonedas Con Paso 1 Hacer
		valorMoneda = 0;
		valorMoneda = Trunc(cantDinero / monedas[i,1]);
		monedas[i,2] = valorMoneda;
		cantDinero = cantDinero - valorMoneda * monedas[i,1];
	Fin Para
FinFuncion

Algoritmo CalcularCantidadBilletes
	//Escriba un programa que lea un n�mero que representar� una cantidad de
	//dinero. El n�mero debe estar entre 1 y 4000. Su programa deber� determinar la menor
	//cantidad de billetes de $500, $200, $100, $50 y $20 as� como de monedas de $10, $5 y $1
	//que debe entregar un empleado a una persona que solicita dicha cantidad.
	
	Definir cantDinero Como Entero;
	Escribir Sin Saltar "Ingrese la cantidad de dinero";
	Leer cantDinero;
	
	Si cantDinero >= 1 Y cantDinero <= 4000 Entonces
		Definir cantMonedas Como Entero;
		cantMonedas = 8;
		Dimension monedas[cantMonedas,2];
		monedas[1,1] = 500;
		monedas[2,1] = 200;
		monedas[3,1] = 100;
		monedas[4,1] = 50;
		monedas[5,1] = 20;
		monedas[6,1] = 10;
		monedas[7,1] = 5;
		monedas[8,1] = 1;
		
		CalcularMonedas(monedas, cantMonedas, cantDinero);
		
		Para i = 1 Hasta cantMonedas Con Paso 1 Hacer
			Si monedas[i,2] > 0 Entonces
				Escribir "Se entregan ", monedas[i,2], " de ", monedas[i,1];
			FinSi
		FinPara
	Sino
		Escribir "Debe ingresar un numero mayor 1 y menor a 4000";
	Fin Si
FinAlgoritmo
