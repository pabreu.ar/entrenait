Funcion resultado <- traducir_a_Romano(num) 
	Segun num Hacer
		1:
			Escribir "El numero ", num, " convertido a romano es igual a I";
		5:
			Escribir "El numero ", num, " convertido a romano es igual a V";
		9:
			Escribir "El numero ", num, " convertido a romano es igual a IX";
		14:
			Escribir "El numero ", num, " convertido a romano es igual a XIV";
		20:
			Escribir "El numero ", num, " convertido a romano es igual a XX";
		26:
			Escribir "El numero ", num, " convertido a romano es igual a XXVI";
		40:
			Escribir "El numero ", num, " convertido a romano es igual a XL";
		50:
			Escribir "El numero ", num, " convertido a romano es igual a L";
		90:
			Escribir "El numero ", num, " convertido a romano es igual a XC";
		99:
			Escribir "El numero ", num, " convertido a romano es igual a XCIX";
		100:
			Escribir "El numero ", num, " convertido a romano es igual a C";
		500:
			Escribir "El numero ", num, " convertido a romano es igual a D";
		1000:
			Escribir "El numero ", num, " convertido a romano es igual a M";
		De Otro Modo:
			Escribir "No existe traduccion posible";
	Fin Segun
FinFuncion

Algoritmo TaducirDeDecimalARomano
	//Escriba un programa que convierta los siguientes n�meros a su
	//correspondiente n�mero romano: 1, 5, 9, 14, 20, 26, 40, 50, 90, 99, 100, 500 y 1000. Para
	//cualquier otro n�mero se deber� reportar: "No existe traducci�n disponible".
	Definir num Como Entero;
	Escribir "Escribir un numero para convertir a romano";
	Leer num;

	Definir resultado Como Cadena;
	resultado <- traducir_a_Romano(num);
	
	Escribir resultado;
FinAlgoritmo
