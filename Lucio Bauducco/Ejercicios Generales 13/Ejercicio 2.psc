Algoritmo Convertir_a_Binario
	//Ingresar una cantidad en binario y transformar al sistema decimal.
	
	Definir binario Como Cadena;
	Definir i, decimal Como Entero;
	i <- 1;
	decimal = 0;
	
	Escribir "Ingrese un numero binario para su conversión";
	Leer binario;
	
	Definir n Como Entero;
	n = Longitud(binario);
	Definir digito Como Entero;
	
	Para i = 0 Hasta n-1 Con Paso 1 Hacer
		digito = ConvertirANumero(Subcadena(binario, n-i, n-i));
		decimal = decimal + digito * (2^(i));
	FinPara
	
	Escribir "El numero ingresado en decimal es ", decimal;
FinAlgoritmo
