Algoritmo Convertir_a_Binario
	//Ingresar una cantidad en sistema decimal y transformar a binario.
	
	Definir cantDecimal Como Real;
	Escribir "Ingrese un numero para convertir a binario";
	Leer cantDecimal;
	
	Definir binario1, binario Como Entero;
	binario1 = 1;
	binario = 0;
	
	Mientras cantDecimal >= 1 Hacer
		Si cantDecimal mod 2 = 1 Entonces
			binario = binario + binario1;
		FinSi
		cantDecimal = trunc(cantDecimal/2);
		binario1 = binario1 * 10;
	FinMientras
	
	Escribir "El numero convertido a binario es igual a ", binario;
 FinAlgoritmo
