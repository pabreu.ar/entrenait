Algoritmo HoraDeLlegadaViaje
	//Un ciclista parte de una ciudad A a las HH horas, MM minutos y SS
	//segundos. El tiempo de viaje hasta llegar a otra ciudad B es de T segundos. Escribir un
	//algoritmo que determine la hora de llegada a la ciudad B
	Definir horas, minutos, seg, tiempo Como Entero;
	Escribir "Ingresar hora de partida";
	Leer horas;
	Escribir "Ingresar minutos";
	Leer minutos;
	Escribir "Ingresar segundos";
	Leer seg;
	Escribir "Ingrese el tiempo de viaje en segundos";
	Leer tiempo;
	
	Definir totalSegundos, segRestantes, nuevaHora, nuevoMin, nuevoSeg Como Entero;
	totalSegundos = (horas * 3600 + minutos * 60 + seg) + tiempo;
	nuevaHora = trunc( totalSegundos / 3600);
	segRestantes = trunc( totalSegundos % 3600);
	nuevoMin = trunc( segRestantes / 60);
	nuevoSeg = trunc( segRestantes % 60);
	
	Escribir "El horario de llegada a destino es: ", nuevaHora, ":", nuevoMin, ":", nuevoSeg;
FinAlgoritmo
