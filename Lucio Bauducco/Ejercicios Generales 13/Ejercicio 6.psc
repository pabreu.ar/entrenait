Algoritmo QueDiaEsMa�ana
	// Programa que muestra el d�a que ser� ma�ana.
	Definir hoy Como Cadena;
	Escribir "Ingrese que dia es hoy";
	Leer hoy;
	
	hoy <- Minusculas(hoy);
	
	Si hoy = "lunes" Entonces
		Escribir "Ma�ana ser� martes";
	FinSi
	Si hoy = "martes" Entonces
		Escribir "Ma�ana ser� miercoles";
	FinSi
	Si hoy = "miercoles" Entonces
		Escribir "Ma�ana ser� jueves";
	FinSi
	Si hoy = "jueves" Entonces
		Escribir "Ma�ana ser� viernes";
	FinSi
	Si hoy = "viernes" Entonces
		Escribir "Ma�ana ser� sabado";
	FinSi
	Si hoy = "sabado" Entonces
		Escribir "Ma�ana ser� domingo";
	FinSi
	Si hoy = "domingo" Entonces
		Escribir "Ma�ana ser� lunes";
	FinSi
FinAlgoritmo
