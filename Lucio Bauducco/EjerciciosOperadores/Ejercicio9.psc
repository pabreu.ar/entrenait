Algoritmo CalcularMayorNumero
	//Escribir un programa que indique cu�l es el mayor de cuatro n�meros enteros
	//Entrada de datos
	Definir num1,num2,num3,num4 Como Real;
	Escribir "Ingrese los 4 n�meros";
	Leer num1,num2,num3,num4;
	//Procesamiento de datos y muestra de resultado
	Definir resultado Como Entero;
	resultado=num1;
	Si num2>resultado Entonces
		resultado=num2;
	FinSi
	Si num3>resultado Entonces
		resultado=num3;
	FinSi
	Si num4>resultado Entonces
		resultado=num4;
	FinSi
	Escribir "El mayor numero es " resultado;
FinAlgoritmo
