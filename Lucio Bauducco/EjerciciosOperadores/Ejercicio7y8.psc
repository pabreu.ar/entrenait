Algoritmo MarcayModeloInvertido
	//Escribir un programa que lea de teclado la marca y modelo de un auto e imprima en pantalla el modelo y la marca (orden invertido a lo que se lee)
	//Declarar variables
	Definir marca,modelo Como Cadena;
	//Ingreso de datos
	Escribir "Ingresar marca";
	Leer marca;
	Escribir "Ingresar modelo";
	Leer modelo;
	//Muestra de resultado
	Escribir "El auto es un " modelo," ",marca;
FinAlgoritmo
