Algoritmo CalcularCuadrado
	//Escribir un programa que calcule el cuadrado de 243
	//Definicion de variable
	Definir calc_cuadrado Como Entero;
	//Procesamiento de datos
	calc_cuadrado<-243^2;
	//Muestra de resultado
	Escribir "El cuadrado de 243 es " calc_cuadrado;
FinAlgoritmo