Algoritmo Ejercicio3operando
	//Escribir un programa que calcule el �rea y per�metro de un pent�gono.
	Definir long_lado Como Real;
	Escribir "Ingrese la longitud de un lado del pentagono";
	Leer long_lado;
	Definir perimetro Como Real;
	perimetro<-long_lado*5;
	Definir apotema Como Real;
	Escribir "Ingrese la apotema del pent�gono";
	Leer apotema;
	Definir area Como Real;
	area<-(perimetro/2)*apotema;
	Escribir "El �rea del pentagono es de " area;
	Escribir "El per�metro del pent�gono es de " perimetro;
FinAlgoritmo
