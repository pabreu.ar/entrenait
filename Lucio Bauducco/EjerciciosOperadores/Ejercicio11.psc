Algoritmo EsoNoDivisiblepor14
	// Escribir un programa que indique si un n�mero es divisible entre 14
	Definir numero1 Como Real;
	Escribir "Ingrese el numero";
	Leer numero1;
	Definir cociente Como Real;
	cociente<-(numero1/14);
	Definir residuo Como Real;
	residuo<-numero1 mod 14;
	Si residuo=0 Entonces
		Escribir "El numero es divisible por 14";
	SiNo
		Escribir "El numero no es divisible por 14";
	FinSi
FinAlgoritmo
