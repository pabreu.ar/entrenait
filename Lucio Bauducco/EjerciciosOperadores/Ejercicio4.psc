Algoritmo SumarEnteros
	//Escribir un programa que realice la suma de dos n�meros enteros.
	Definir num1,num2 Como Entero;
	//Entrada de datos
	Escribir "Ingresar el primer numero entero";
	Leer num1;
	Escribir "Ingresar el segundo numero entero";
	Leer num2;
	//Procesamiento de datos
	Definir suma Como Entero;
	suma<-num1+num2;
	//Salida de datos
	Escribir "La suma de ambos numeros es " suma;
FinAlgoritmo
