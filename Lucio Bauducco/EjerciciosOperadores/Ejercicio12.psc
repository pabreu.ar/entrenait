Algoritmo SumaPositivaNegativaoCero
	//Escribir un programa que indique si la suma de dos valores es positiva, negativa o cero
	Definir numero1,numero2 Como Real;
	Escribir "Ingrese el primer valor";
	Leer numero1;
	Escribir "Ingrese el segundo valor";
	Leer numero2;
	Definir suma Como Real;
	suma<-numero1+numero2;
	Si suma>0 Entonces
		Escribir "La suma es positiva";
	FinSi
	Si suma<0 Entonces
		Escribir "La suma es negativa";
	FinSi
	Si suma=0 Entonces
		Escribir "La suma es igual a 0";
	FinSi
FinAlgoritmo
