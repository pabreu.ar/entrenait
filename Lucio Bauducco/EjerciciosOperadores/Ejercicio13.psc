Algoritmo Divisiblepor2y5
//Escribir un programa que indique si un n�mero es divisible entre dos y cinco (a la vez).
	Definir numero1 Como Real;
	Escribir "Ingrese el numero";
	Leer numero1;
	Definir cociente1 Como Real;
	cociente1<-(numero1/2);
	Definir residuo1 Como Real;
	residuo1<-numero1 mod 2;
	Definir cociente2 Como Real;
	cociente2<-(numero1/5);
	Definir residuo2 Como Real;
	residuo2<-numero1 mod 5;
	Si residuo1+residuo2 = 0 Entonces
		Escribir "El numero ingresado es divisible por 2 y 5 a la vez";
	SiNo
		Escribir "El numero ingresado no es divisible por 2 y 5 a la vez";
	FinSi
FinAlgoritmo
