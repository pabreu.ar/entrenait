Algoritmo SumaDeImpares
	// Hacer un algoritmo en Pseint para calcular la suma de los n�meros impares menores o iguales a n
	Definir n, suma Como Entero;
	Escribir "Ingrese el valor de n";
	Leer n;
	
	Definir i Como Entero;
	i <- 1;
	suma <- 0;
	Mientras i <= n Hacer
		Si i MOD 2 <> 0 Entonces
			suma <- i + suma;
		FinSi
		i <- i + 1;
	FinMientras
	Escribir "El valor de la suma es: ", suma;
FinAlgoritmo
