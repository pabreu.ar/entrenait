Algoritmo NumerosPrimosdelUnoalCien
	//Hacer un algoritmo que escriba todos los n�meros primos del 1 al 100
	Definir i Como Entero;
	Definir esPrimo Como Logico;
	Definir divisor Como Numero;
	Para i=2 Hasta 100 Hacer
		esPrimo = Verdadero;
		divisor = 2;
		Mientras esPrimo Y divisor<i Hacer
			esPrimo = i MOD divisor <> 0;
			divisor = divisor + 1;
		FinMientras
		Si esPrimo Entonces
			Escribir i, " - " ;
		FinSi
	FinPara
FinAlgoritmo
