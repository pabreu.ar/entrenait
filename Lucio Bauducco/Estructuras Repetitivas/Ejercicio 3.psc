Algoritmo SumaNumeros 
	//Escribir un programa que determine la suma de las cifras de un entero positivo de 4 cifras..
	Definir suma,contador Como Entero;
	contador<- 0 
	suma<- 0;
	Hacer
		contador<- contador+1;
		suma<- suma + contador;
	Hasta Que contador< 100
	Escribir  "La suma es ",suma;
FinAlgoritmo
