Algoritmo CiudadMasPoblada
	//Se quiere saber cu�l es la ciudad con la poblaci�n de m�s personas, son tres
	//provincias y once ciudades cada una, hacer un algoritmo en Pseint que nos permita saber eso
	Definir provincia, ciudad, ciudadMasGrande Como Cadena
	Definir poblacionCiudad, mayorPoblacion Como Entero
	mayorPoblacion <- 0
	Para i <- 1 Hasta 3 Con Paso 1 Hacer
		Escribir "Ingrese el nombre de la provincia n�mero ", i;
		Leer provincia;
		Para j <- 1 Hasta 11 Con Paso 1 Hacer
			Escribir "Ingrese el nombre de la ciudad n�mero ", j;
			Leer ciudad
			Escribir "Ingrese la cantidad de habitantes en la ciudad n�mero ", j;
			Leer poblacionCiudad;
			Si poblacionCiudad > mayorPoblacion Entonces
				mayorPoblacion <- poblacionCiudad
				ciudadMasGrande <- ciudad
			FinSi
		FinPara
		Escribir "Para la provincia n�mero ",i " ,", provincia, " la ciudad con m�s habitantes es: " ciudadMasGrande, " con un total de ", mayorPoblacion, " habitantes";
	FinPara
FinAlgoritmo