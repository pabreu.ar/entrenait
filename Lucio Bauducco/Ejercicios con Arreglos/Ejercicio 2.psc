Algoritmo ArregloAlCuadrado
	//Crear un arreglo con n n�meros, ingresados por teclado y mostrar sus valores elevados al cuadrado.
	Definir cantidad Como Entero;
	Escribir "Ingrese la cantidad de numeros que desea almacenar";
	Leer cantidad;
	
	Dimension arreglo[cantidad];
	Para i<-1 Hasta cantidad Con Paso 1 Hacer
		Escribir "Ingrese un numero para almacenar ",i; 
		Leer arreglo[i];
	FinPara
	
	Definir alCuadrado Como Entero;
	Para i<-1 Hasta cantidad Con Paso 1 Hacer
		alCuadrado<-arreglo[i]^2;
		Escribir "El numero ingresado al cuadrado es ", alCuadrado;
	FinPara
FinAlgoritmo
