Algoritmo CalcularCostoPlato
	//Dise�e un arreglo en el que se ingrese la cantidad de productos y sus
	//respectivos precios, para la preparaci�n de un plato, tambi�n se debe mostrar al final el
	//costo a gastar
	Definir cantProductos Como Entero;
	Escribir "Ingrese la cantidad de productos del plato";
	Leer cantProductos;
	Dimension arreglo[cantProductos];
	Definir suma Como Real;
	Para i<-1 Hasta cantProductos Con Paso 1 Hacer
		Escribir "Ingrese el precio para del producto nro ",i;
		Leer arreglo[i];
		suma<-suma+arreglo[i];
	FinPara
	Escribir "El costo total es de ", suma;
FinAlgoritmo
