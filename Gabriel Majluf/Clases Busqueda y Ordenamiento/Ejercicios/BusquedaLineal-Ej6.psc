//Metodo de busqueda generico con dos arreglos y dos keys
Funcion pos = BusquedaSecuencial(arreglo, arreglo2, n, key, key2)
	pos = -1;
	Definir i Como Entero;
	i = 1;
	
	Mientras pos = -1 Y i <= n Hacer
		Si arreglo[i] = key y arreglo2[i] = key2 Entonces
			pos = i;
		FinSi
		i = i + 1;
	Fin Mientras
	
FinFuncion


//Llenar la BD
Funcion InicializarBaseDeDatos (origenes, destinos, horarios, valores)
	
	origenes[1] = "Sunchales";
	destinos[1] = "Rafaela";
	horarios[1] = "08:00";
	valores[1] = "500";
	
	origenes[2] = "Sunchales";
	destinos[2] = "Santa Fe";
	horarios[2] = "17:00";
	valores[2] = "800";
	
	origenes[3] = "Santa Fe";
	destinos[3] = "Cordoba";
	horarios[3] = "19:30";
	valores[3] = "1200";
	
	origenes[4] = "Cordoba";
	destinos[4] = "Buenos Aires";
	horarios[4] = "10:15";
	valores[4] = "1500";
	
FinFuncion

Algoritmo Compa�iaAviaci�n
	//La compa��a de aviaci�n Aerol�neas Argentinas mantiene la informaci�n de
	//todas sus rutas de vuelo en una base de datos con informaci�n como: lugares de origen y
	//destino, horario, valor; para ello utiliza una matriz en la que cada columna est� destinada a
	//uno de los datos mencionados. Dise�ar un algoritmo para averiguar si existe un vuelo entre
	//un origen y un destino introducidos por el usuario y en caso de existir obtener informaci�n
	//sobre el mismo.
	
	Definir origenBuscado, destinoBuscado Como Cadena;
	
	Escribir "Ingrese el origen que desea buscar"
	Leer origenBuscado;
	
	Escribir "Ingrese el destino que desea buscar"
	Leer destinoBuscado;
	
	Definir cantV Como Entero;
	cantV = 1000;
	Dimension origenes[cantV];
	Dimension destinos[cantV];
	Dimension horarios[cantV];
	Dimension valores[cantV];
	
	InicializarBaseDeDatos(origenes, destinos, horarios, valores);
	
	Definir indice, indice1 Como Entero;
	indice = BusquedaSecuencial(origenes, destinos, cantV, origenBuscado, destinoBuscado)
	
	Si indice <> -1 Entonces
			Escribir "Los datos de su viaje son: ", origenes[indice], " - ", destinos[indice] " a la hora: ", horarios[indice], " con un costo de: ", valores[indice];
	FinSi
	Si indice = -1 Entonces
		Escribir "No se encuentra el viaje buscado de: ", origenBuscado, " a: ", destinoBuscado;
	FinSi
	
FinAlgoritmo
