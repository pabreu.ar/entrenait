//Metodo de busqueda generico
Funcion pos = BusquedaSecuencial(arreglo, n, key)
	pos = -1;
	Definir i Como Entero;
	i = 1;
	
	Mientras pos = -1 Y i <= n Hacer
		Si arreglo[i] = key Entonces
			pos = i;
		FinSi
		i = i + 1;
	Fin Mientras
	
FinFuncion


//Llenar la BD
Funcion InicializarBaseDeDatos (codEstudiante, nombreEstudiante, notaEstudiante)
	
	codEstudiante[1] = "001";
	nombreEstudiante[1] = "Juan";
	notaEstudiante[1] = "8";
	
	codEstudiante[2] = "002";
	nombreEstudiante[2] = "Maria";
	notaEstudiante[2] = "7";
	
	codEstudiante[3] = "003";
	nombreEstudiante[3] = "Alejandro";
	notaEstudiante[3] = "9";
	
	codEstudiante[4] = "004";
	nombreEstudiante[4] = "Pablo";
	notaEstudiante[4] = "10";
	
FinFuncion


Algoritmo CodigoNombreNotaEstudiante
	//Un profesor guarda los datos de sus estudiantes en tres vectores: c�digo,
	//nombre y nota. Se requiere un algoritmo para consultar la nota de un estudiante a partir de
	//su c�digo.
	
	Definir codEstudianteBuscado Como Cadena;
	Escribir "Ingrese el codigo del alumno que desea buscar"
	Leer codEstudianteBuscado;
	
	Definir cantE Como Entero;
	cantE = 1000;
	Dimension codEstudiante[cantE];
	Dimension nombreEstudiante[cantE];
	Dimension notaEstudiante[cantE];
	
	InicializarBaseDeDatos(codEstudiante, nombreEstudiante, notaEstudiante);
	
	Definir indice Como Entero;
	indice = BusquedaSecuencial(codEstudiante, cantE, codEstudianteBuscado)
	
	Si indice <> -1 Entonces
		Escribir "Su nota es: ", notaEstudiante[indice];
	FinSi
	Si indice = -1 Entonces
		Escribir "No se encuentra en la Base de Datos del Profesor";
	FinSi
	
FinAlgoritmo
