//Metodo de busqueda generico
Funcion pos = BusquedaSecuencial(arreglo, n, key)
	pos = -1;
	Definir i Como Entero;
	i = 1;
	
	Mientras pos = -1 Y i <= n Hacer
		Si arreglo[i] = key Entonces
			pos = i;
		FinSi
		i = i + 1;
	Fin Mientras
	
FinFuncion

Funcion BaseDatosLlamadas (nroLinea, origen, destino, fecha, hora, duracion)
	
	nroLinea[1] = "3492111111";
	origen[1] = "Rafaela";
	destino[1] = "Buenos Aires";
	fecha[1] = "05-11-21";
	hora[1] = "12:00";
	duracion[1] = "10";
	
	nroLinea[2] = "3492222222";
	origen[2] = "Rafaela";
	destino[2] = "Buenos Aires";
	fecha[2] = "05-11-22";
	hora[2] = "12:00";
	duracion[2] = "10";
	
	nroLinea[3] = "3492333333";
	origen[3] = "Rafaela";
	destino[3] = "Buenos Aires";
	fecha[3] = "05-11-23";
	hora[3] = "12:00";
	duracion[3] = "10";
	
	nroLinea[4] = "3492444444";
	origen[4] = "Rafaela";
	destino[4] = "Santa ";
	fecha[4] = "05-11-24";
	hora[4] = "12:00";
	duracion[4] = "10";
	
	nroLinea[5] = "3492555555";
	origen[5] = "Rafaela";
	destino[5] = "Buenos Aires";
	fecha[5] = "05-11-25";
	hora[5] = "12:00";
	duracion[5] = "10";
	
	
FinFuncion


Algoritmo CajeroAutomatico
	//En un hipot�tico cajero electr�nico, cuando un cliente solicita el saldo de su
	//cuenta desliza la tarjeta, el lector toma el n�mero de cuenta y el sistema busca dicho
	//n�mero en la base de datos. Si lo encuentra reporta el saldo, si no muestra un mensaje
	//de error. Suponiendo que la base de datos consiste en tres vectores: n�mero de cuenta,
	//titular y saldo. Dise�ar un algoritmo para consultar el saldo de una cuenta.
	
	Definir nroCuentaBuscado Como Cadena;
	Escribir "Ingrese el numero de l�nea que desea consultar"
	Leer nroLineaBuscado;
	
	Definir cantC Como Entero;
	cantC = 1000;
	Dimension nroLinea[cantC];
	Dimension origLla[cantC];
	Dimension destLla[cantC];
	Dimension fechaLla[cantC]
	Dimension horaLla[cantC]
	Dimension duracLla[cantC]
	
	
	BaseDatosLlamadas(nroLinea, origLla, destLla, fechaLla, horaLla, duracLla);
	
	Definir indice Como Entero;
	indice = BusquedaSecuencial(nroLinea, cantC, nroLineaBuscado)
	
	Si indice <> -1 Entonces
		Escribir "Informaci�n de llamada: L�nea: ", nroLinea[indice],", Origen: ", origLla[indice],", Destino: ", destLla[indice],", Fecha: ", fechaLla[indice],", Hora: ", horaLla[indice],", Duraci�n: ", duracLla[indice];
	FinSi
	Si indice = -1 Entonces
		Escribir "Error no han realizado llamadas desde esta l�nea";
	FinSi
	
FinAlgoritmo