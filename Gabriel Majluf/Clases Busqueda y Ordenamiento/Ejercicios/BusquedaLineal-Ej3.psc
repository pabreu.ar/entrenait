//Metodo de busqueda generico
Funcion pos = BusquedaSecuencial(arreglo, n, key)
	pos = -1;
	Definir i Como Entero;
	i = 1;
	
	Mientras pos = -1 Y i <= n Hacer
		Si arreglo[i] = key Entonces
			pos = i;
		FinSi
		i = i + 1;
	Fin Mientras
	
FinFuncion

//Llenar la BD
Funcion InicializarBaseDeDatos (cedulas, nombres)
	
	cedulas[1] = "001";
	nombres[1] = "Juan";
	
	cedulas[2] = "002";
	nombres[2] = "Maria";
	
	cedulas[3] = "003";
	nombres[3] = "Alejandro";

	cedulas[4] = "004";
	nombres[4] = "Pablo";
	
FinFuncion

Algoritmo sin_titulo
	//El d�a de elecciones, en cada mesa se cuenta con un listado de las personas
	//que votar�n en dicha mesa. Los datos est�n guardados en dos vectores, en el primero la
	//c�dula y en el segundo el nombre. Se requiere un algoritmo que ingrese la c�dula de una
	//persona e informe si puede sufragar en ese puesto de votaci�n.
	
	Definir nroCedulaBuscado Como Cadena;
	Escribir "Ingrese el numero de cedula que desea buscar"
	Leer nroCedulaBuscado;
	
	Definir cantC Como Entero;
	cantC = 1000;
	Dimension cedulas[cantC];
	Dimension nombres[cantC];
	
	InicializarBaseDeDatos(cedulas, nombres);
	
	Definir indice Como Entero;
	indice = BusquedaSecuencial(cedulas, cantC, nroCedulaBuscado)
	
	Si indice <> -1 Entonces
		Escribir "Correcto, vota en �sta mesa. Su nombre es: ", nombres[indice];
	FinSi
	Si indice = -1 Entonces
		Escribir "Error no se encuentra el numero de cedula, vota en otra mesa";
	FinSi
FinAlgoritmo
