Funcion OrdenamientoBurbujaAsc(arreglo Por Referencia, n Por valor)
	Definir i, j Como Entero;
	Para i <- 1 Hasta n - 1 Con Paso 1 Hacer
		Para j <- i + 1 Hasta n Con Paso 1 Hacer
			Si arreglo[i] > arreglo[j] Entonces
				auxChange <- arreglo[i];
				arreglo[i] <- arreglo[j];
				arreglo[j] <- auxChange;
			FinSi
		FinPara
	FinPara
FinFuncion

Algoritmo OrdenamientoAscendentePorBurbuja
	Definir n como Entero;
	n <- 6;
	Dimension vector[n];
	vector[1] = 1;
	vector[2] = 3;
	vector[3] = 6;
	vector[4] = 8;
	vector[5] = 2;
	vector[6] = 5;
	
	OrdenamientoBurbujaAsc(vector, n)
	
	Para i <- 1 Hasta n Con Paso 1 Hacer
		Escribir vector[i];
	FinPara
FinAlgoritmo