Funcion OrdenarAscendentemente(arreglo Por Referencia, n Por Valor, arregloDeSalida Por Referencia) 
	//Realizando copia del arreglo de entrada/input
	Definir i Como Entero;
	Para i <- 1 Hasta n Con Paso 1 Hacer
		arregloDeSalida[i] = arreglo[i];
	FinPara
	
	Definir min, minPos, j Como Entero;
	Para i <- 1 Hasta n - 1 Con Paso 1 Hacer
		//Inicio obtener el minimo del resto del vector
		min = arregloDeSalida[i + 1];
		minPos = i + 1;
		Para j <- i + 2 Hasta n Con Paso 1 Hacer
			Si min > arregloDeSalida[j] Entonces
				min = arregloDeSalida[j];
				minPos = j;
			Fin Si
		Fin Para
		//Fin obtener el minimo del resto del vector
		//Intercambio en caso de ser mayor el elemento que estoy analizando
		Si arregloDeSalida[i] > min Entonces
			min = arregloDeSalida[i];
			arregloDeSalida[i] = arregloDeSalida[minPos];
			arregloDeSalida[minPos] = min;
		Fin Si
	Fin Para
FinFuncion

Algoritmo Ordenamiento_LucioCompany
	Definir n como Entero;
	n <- 10;
	Dimension vector[n];
	vector[1] = 1;
	vector[2] = 3;
	vector[3] = 6;
	vector[4] = 8;
	vector[5] = 2;
	vector[6] = 5;
	vector[7] = 9;
	vector[8] = 10;
	vector[9] = 4;
	vector[10] = 7;
	
    Dimension ordenadoAscendetemente[n];
	OrdenarAscendentemente(vector, n, ordenadoAscendetemente);
	
	Escribir "Vector Original"
	Para i<- 1 Hasta  n Hacer
		Escribir vector[i];
	FinPara
	
	Escribir "Vector Ordenado Ascendentemente"
	Para i<- 1 Hasta n Hacer
		Escribir ordenadoAscendetemente[i];
	FinPara
	
	Escribir "Vector Ordenado Descendentemente"
	Para i<- n Hasta 1 Con Paso -1 Hacer
		Escribir ordenadoAscendetemente[i];
	FinPara
FinAlgoritmo
