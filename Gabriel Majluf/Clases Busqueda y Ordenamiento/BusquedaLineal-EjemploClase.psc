//Metodo de busqueda generico
Funcion pos = BusquedaSecuencial(arreglo, n, key)
	//Devuelve -1 en caso que no lo encuentre y si lo encuentra, devuelve el indice.
	pos = -1;
	Definir i Como Entero;
	i = 1;
	
	Mientras pos = -1 Y i <= n Hacer
		Si arreglo[i] = key Entonces
			pos = i;
		FinSi
		i = i + 1;
	Fin Mientras
	
FinFuncion


//Llenar la BD
Funcion InicializarBaseDeDatos (dniEstudiante, nombreEstudiante, carreraEstudiante)
	
	dniEstudiante[1] = "11111111111";
	nombreEstudiante[1] = "Juan";
	carreraEstudiante[1] = "Programacion";
	
	dniEstudiante[2] = "22222222222";
	nombreEstudiante[2] = "Maria";
	carreraEstudiante[2] = "Contabilidad";
	
	dniEstudiante[3] = "33333333333";
	nombreEstudiante[3] = "Alejandro";
	carreraEstudiante[3] = "RRHH";
	
	dniEstudiante[4] = "44444444444";
	nombreEstudiante[4] = "Pablo";
	carreraEstudiante[4] = "Dise�o";
	
FinFuncion



Algoritmo CodNombreNotaEstudiante
	//Un profesor guarda los datos de sus estudiantes en tres vectores: c�digo, nombre y nota. Se requiere un algoritmo para consultar la nota de un estudiante a partir de	su c�digo.
	
	Definir nombreBuscado Como Cadena;
	Escribir "Ingrese el nombre del alumno que desea buscar"
	Leer nombreBuscado;
	
	Definir cantE Como Entero;
	cantE = 1000;
	Dimension dniEstudiante[cantE];
	Dimension nombreEstudiante[cantE];
	Dimension carreraEstudiante[cantE];
	
	InicializarBaseDeDatos(dniEstudiante, nombreEstudiante, carreraEstudiante);
	
	Definir indice Como Entero;
	indice = BusquedaSecuencial(nombreEstudiante, cantE, nombreBuscado)
	
	Si indice <> -1 Entonces
		Escribir "Su documento es: ", dniEstudiante[indice];
	FinSi
	Si indice = -1 Entonces
		Escribir "No se encuentra en la Base de Datos de la Universidad";
	FinSi
	
FinAlgoritmo
