Algoritmo AreaTrapecio
	//Calcular el �rea de un trapecio
	
	Definir baseMayor, baseMenor, altura Como Real;
	
	Escribir "Ingrese la longitud de la base mayor del trapecio: ";
	Leer baseMayor;
	Escribir "Ingrese la longitud de la base menor del trapecio: ";
	Leer baseMenor;
	Escribir "Ingrese la altura del trapecio: ";
	Leer altura;
	
	Definir area Como Real;
	area <- [(baseMayor + baseMenor) * altura] / 2;
	
	Escribir "El �rea del trapecio es: ", area;
	
FinAlgoritmo
