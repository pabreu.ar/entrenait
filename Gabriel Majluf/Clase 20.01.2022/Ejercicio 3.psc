Algoritmo PositivoNegativo
	//Determinar el algoritmo para saber si un n�mero es positivo o negativo.

	Definir num Como Real;
	Escribir "Ingrese un n�mero: ";
	Leer num;
	
	Definir resultado Como Cadena;
	
	Si num > 0 Entonces
		resultado <- "El n�mero es positivo";
	FinSi
	Si num < 0 Entonces
		resultado <- "El n�mero es negativo";
	FinSi
	Si num = 0 Entonces
		resultado <- "El n�mero es cero y se considera neutro";
	FinSi
	
	Escribir "N�mero ingresado: ", num;
	Escribir resultado;
	
FinAlgoritmo
