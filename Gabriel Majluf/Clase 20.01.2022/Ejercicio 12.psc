Algoritmo InversionEmpresa
	//Tres personas deciden invertir su dinero para fundar una empresa. Cada una de ellas invierte una cantidad distinta. Obtener el porcentaje que cada quien invierte con respecto a la cantidad total invertida.
	
	Definir inversion1, inversion2, inversion3 Como Real
	Escribir "Ingrese el total que va a invertir la persona 1";
	Leer inversion1;
	Escribir "Ingrese el total que va a invertir la persona 2";
	Leer inversion2;
	Escribir "Ingrese el total que va a invertir la persona 3";
	Leer inversion3;
	
	Definir totalInversion, porcentaje1, porcentaje2, porcentaje3 Como Real
	totalInversion = inversion1 + inversion2 + inversion3;
	porcentaje1 = (inversion1 / totalInversion) * 100
	porcentaje2 = (inversion2 / totalInversion) * 100
	porcentaje3 = (inversion3 / totalInversion) * 100
	
	Escribir "El porcentaje del total que va a invertir la persona 1 es: ", porcentaje1, "%";
	Escribir "El porcentaje del total que va a invertir la persona 2 es: ", porcentaje2, "%";
	Escribir "El porcentaje del total que va a invertir la persona 3 es: ", porcentaje3, "%";
	
FinAlgoritmo
