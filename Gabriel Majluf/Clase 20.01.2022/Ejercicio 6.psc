Algoritmo CalcularCostoCabinaInternet
	//Calcular el monto a pagar en una cabina de Internet si el costo por hora es de S/.1,5 y por cada 5 horas te dan una hora de promoción gratis, sabiendo que la permanencia en la cabina fue de 12 horas.
	
	Definir cantHoras Como Entero
	Escribir  "Ingrese la cantidad de horas en cabina"
	Leer cantHoras;
	Definir costoFinal, promocion Como Real
	promocion = Trunc(cantHoras/5)
	costoFinal = (cantHoras - promocion) * 1.5
	
	Escribir "Se debe abonar ", costoFinal, "$ por ", cantHoras, " horas de internet."
	
FinAlgoritmo
