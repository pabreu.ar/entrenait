Algoritmo ConvertirBinarioADecimalComoCADENA
	//Ingresar una cantidad en binario y transformar al sistema decimal.
	
	Definir nroBin Como Cadena;
	Definir i, decimal Como Entero;
	i = 0;
	decimal = 0;
	
	Escribir "Ingrese numero en binario: "
	Leer nroBin;
	
	Definir n Como Entero;
	n = Longitud(nroBin);
	Definir digito Como Entero;
	
	Para i = 0 Hasta n - 1 Con Paso 1 Hacer
		digito = ConvertirANumero(Subcadena(nroBin, n-i, n-i)); 
		decimal = decimal + digito * (2^(i));
	Fin Para
	
	Escribir "El numero ingresado en Decimal es: ", decimal;
	
FinAlgoritmo