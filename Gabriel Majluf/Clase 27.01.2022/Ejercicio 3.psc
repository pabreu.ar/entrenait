Algoritmo DistanciaCiclista
	//Un ciclista parte de una ciudad A a las HH horas, MM minutos y SS
	//segundos. El tiempo de viaje hasta llegar a otra ciudad B es de T segundos. Escribir un
	//algoritmo que determine la hora de llegada a la ciudad B.
	
	Definir horas, minutos, seg, tiempo Como Entero
	Escribir "Ingrese la hora de partida"
	Leer horas;
	Escribir "Ingrese los minutos de partida"
	Leer minutos;
	Escribir "Ingrese los segundos de partida"
	Leer seg;
	Escribir "Ingrese el tiempo de viaje en segundos hasta la ciudad de destino"
	Leer tiempo;
	
	Definir totalSeg, segRestantes,nuevaHH, nuevoMM, nuevoSS Como Entero
	totalSeg = (horas * 3600 + minutos * 60 + seg) + tiempo;
	nuevaHH =  Trunc(totalSeg / 3600);
	segRestantes = Trunc(totalSeg % 3600);
	nuevoMM = Trunc(segRestantes / 60);
	nuevoSS = Trunc(segRestantes % 60);
	
	Escribir "El horario de llegada a destino es: ", nuevaHH, " hs ", nuevoMM, " minutos ", nuevoSS, " segundos "; 
	
FinAlgoritmo
