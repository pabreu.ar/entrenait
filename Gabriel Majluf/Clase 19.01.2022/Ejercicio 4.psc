Algoritmo Suma_Numeros_For
	//Hacer un algoritmo en Pseint para calcular la suma de los primeros cien n�meros con un ciclo For.
	
	Definir suma, i Como Entero;
	suma <- 0;
	
	Para i <- 1 Hasta 100 Con Paso 1 Hacer
		suma <- suma + i;
	FinPara
	
	Escribir "La suma es: ", suma;
FinAlgoritmo
