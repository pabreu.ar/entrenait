Algoritmo SerieFibonacci
	// Hacer un algoritmo en Pseint para calcular la serie de Fibonacci.
	
	Escribir "Por favor ingrese cuantos n�meros de la sucesi�n de Fibonacci desea ver: "
	leer cantidadF
	
	Definir primerNro, segundoNro, sumaAmbosNros Como Entero;
	primerNro<-0;	segundoNro<-1;
	
	Para i<-1 Hasta cantidadF Hacer
		Escribir primerNro;
		sumaAmbosNros <- primerNro + segundoNro;
		primerNro <- segundoNro;
		segundoNro <- sumaAmbosNros;
	FinPara

FinAlgoritmo
