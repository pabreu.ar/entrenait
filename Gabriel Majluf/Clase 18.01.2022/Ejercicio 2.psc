Algoritmo Suma4Cifras
	//Escribir un programa que determine la suma de las cifras de un entero positivo de 4 cifras..

	
	Definir num Como Entero
	Escribir "Ingrese un numero"
	Leer num
	
	Definir  millar, centena, decena, unidad, resto Como Entero
		
	millar = Trunc(num / 1000);
	resto = num % 1000;
	centena = Trunc(resto / 100);
	resto = resto % 100;
	decena = Trunc(resto / 10);
	unidad = resto % 10;

	Definir suma Como Entero;
	suma = millar + centena + decena + unidad;
	Escribir  "La suma total es: ", suma;
		
FinAlgoritmo
