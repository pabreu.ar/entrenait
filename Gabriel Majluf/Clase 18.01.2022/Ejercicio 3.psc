Algoritmo ConvertidorTemperaturaCaF
	//Una temperatura Celsius (Centígrados) puede ser convertida a una temperatura equivalente F. Escriba un programa para calcular ambos valores
	
	Definir temperatura Como Real
	Escribir "Ingrese una temperatura"
	Leer temperatura
	Definir tipoTemperatura Como Caracter
	Escribir "Ingrese C o F dependiendo de tipo de temperatura que quiere saber"
	Leer tipoTemperatura
	Definir temperaturaFinal Como Real
	
	Si Mayusculas(tipoTemperatura) = "C" Entonces
		temperaturaFinal = (temperatura * 9/5) + 32;
		Escribir "La temperatura en grados Farenheit es: ", temperaturaFinal;
		SiNo Si Mayusculas(tipoTemperatura) = "F" Entonces
				temperaturaFinal = (temperatura - 32) * 5/9;
			Escribir "La temperatura en grados Celsius es: ", temperaturaFinal;
		SiNo
			Escribir "Ingrese el tipo de temperatura correspondiente";
		FinSi
	FinSi
	
FinAlgoritmo
