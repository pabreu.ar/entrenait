Algoritmo CalculoDescuentoOctubre
	
	// Una tienda ofrece un descuento del 15 MOD  sobre el total de la compra durante el mes de octubre. Dado un mes y un importe, calcular cu�l es la cantidad que se debe cobrar al cliente.
	Definir importe Como Real
	Escribir 'Ingrese el importe de la compra'
	Leer importe
	Definir nroMes Como Entero
	Escribir 'Ingrese el n�mero del mes de la compra'
	Leer nroMes
	Definir totalaPagar Como Real
	totalaPagar <- importe
	Si nroMes=10 Entonces
		totalaPagar <- importe-importe*0.15
	FinSi
	Escribir 'El total a pagar en la compra es: ',totalaPagar
	
FinAlgoritmo
