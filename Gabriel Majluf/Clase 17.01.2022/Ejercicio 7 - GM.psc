Algoritmo CalculoA�oMesSemanaDia
	Escribir "Ingrese una cantidad de d�as para convertir en a�os, meses y semanas"
	Definir numeroDias Como Entero
	Leer numeroDias
	Definir diasRestantes Como Entero
	Definir anios Como Entero
	anios = Trunc(numeroDias / 365)
	diasRestantes = numeroDias - anios*365
	Definir meses Como Entero
	// Los meses los consideramos 30 d�as por promedio
	meses = Trunc(diasRestantes / 30)
	diasRestantes = diasRestantes - meses*30
	Definir semanas Como Entero
	semanas = Trunc(diasRestantes / 7)
	Escribir "Es equivalente a ", anios, " a�os, ", meses, " meses, ", semanas, " semanas y ", diasRestantes, " d�as";
FinAlgoritmo
