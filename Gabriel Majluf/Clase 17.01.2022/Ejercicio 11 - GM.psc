Algoritmo AccesoaCurso
	// Algoritmo que nos diga si una persona puede acceder a cursar un ciclo formativo de grado superior o no. Para acceder a un grado superior, si se tiene un titulo de bachiller, en caso de no tenerlo, se puede acceder si hemos superado una prueba de acceso.
	Definir tituloBachiller,pruebaAcceso Como Caracter
	Escribir 'Responda con SI POSEE o NO POSEE'
	Escribir '�Posee titulo de Bachiller?'
	Leer tituloBachiller
	Escribir '�Posee prueba de acceso superada?'
	Leer pruebaAcceso
	Definir accesoCursado Como Logico
	accesoCursado <- Falso
	Si tituloBachiller='SI POSEE' O tituloBachiller='NO POSEE' O pruebaAcceso='SI POSEE' O pruebaAcceso='NO POSEE' Entonces
		Si tituloBachiller='SI POSEE' Entonces
			accesoCursado <- Verdadero
		SiNo
			Si pruebaAcceso='SI POSEE' Entonces
				accesoCursado <- Verdadero
			FinSi
		FinSi
	SiNo
		Escribir 'Escriba su respuesta correctamente'
	FinSi
	Escribir '�Puede acceder al curso? ',accesoCursado
FinAlgoritmo
