Algoritmo Ejercicio2
	
	Definir edad Como Entero;
	edad=23;
	Escribir edad;
	
	Definir altura Como Num�rico;
	altura = 1.78;
	Escribir altura, "mts";
	
	Definir nombre Como Cadena;
	nombre= "Gabriel";
	Escribir nombre;
	
	Definir iva, valor1, resultado Como Num�rico;
	iva = 1.21;
	valor1 = 50;
	resultado = valor1 * iva;
	Escribir resultado;
	
	Definir peso Como Num�rico;
	peso = 70.25;
	Escribir peso, "kg";
	
	Definir alumnorepetidor Como Cadena;
	Escribir "Ingrese SI para el alumno repetidor o NO si no lo es"
	Leer alumnorepetidor
	si alumnorepetidor  = "SI" o alumnorepetidor = "NO" Entonces
		Escribir "El alumno es repetidor? :", alumnorepetidor;
	SiNo
		Escribir "Escriba una respuesta correcta"
	FinSi
	
	Definir letra Como Caracter;
	letra= "a";
	Escribir letra;
	
	Definir minutos Como Num�rico;
	minutos = 30.25;
	Escribir minutos;
	
	Definir matriculacoche Como Cadena;
	matriculacoche= "abc705";
	Escribir matriculacoche;
	
	Definir mayordeedad Como Logico;
	Definir edad1 Como Entero;
	Escribir "Inserte su edad";
	Leer edad1;
	si edad1 >= 18 Entonces
		mayordeedad = Verdadero;
	SiNo
		mayordeedad = Falso;
	FinSi
	Escribir "Es mayor de edad? :", mayordeedad;
	
	Definir codpostal Como Entero;
	codpostal=2322;
	Escribir codpostal;
	
	Definir generoH, generoM Como Caracter; //H:Hombre, M:Mujer
	generoH= "H";
	generoM= "M";
	Escribir "Hombre: ", generoH, " - ", "Mujer: ", generoM;
	
	Definir numerodehijos Como Entero;
	numerodehijos=3;
	Escribir numerodehijos;
	
	Definir tallacamisa Como Entero;
	tallacamisa=44;
	Escribir tallacamisa;
	
	Definir precio Como Num�rico;
	precio=100.5;
	Escribir precio;
	
	Definir mensaje Como Cadena;
	Escribir "Ingrese el mensaje deseado"
	Leer mensaje
	Escribir mensaje;
	
	Definir lun,mar,mier,juev,vier Como Cadena;
	lun= "Lunes";
	mar= "Martes";
	mier= "Mi�rcoles";
	juev= "Jueves";
	vier= "Viernes";
	Escribir lun, "- ", mar, "- ", mier, "- ", juev, "- ", vier;
	
	//Deber�a estar en una sentencia repetitiva
	Definir contador Como Entero;
	contador = contador + 1;
	Escribir contador;
	
	Definir tallacamisetas, tallacamisetam, tallacamisetal, tallacamisetaxl Como Caracter;
	tallacamisetas= "S";
	tallacamisetam= "M";
	tallacamisetal= "L";
	tallacamisetaxl= "XL";
	Escribir tallacamisetas, "- ", tallacamisetam, "- ", tallacamisetal, "- ", tallacamisetaxl;

FinAlgoritmo
