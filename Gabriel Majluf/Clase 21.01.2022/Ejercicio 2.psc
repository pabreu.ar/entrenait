Algoritmo ArrayCuadradosNros
	//Crear un arreglo con n n�meros, ingresados por teclado y mostrar sus valores elevados al cuadrado.
	
	Definir n Como Entero;
	Escribir "Ingresar la cantidad de n�meros que desee";
	Leer n;
	Dimension valores[n];
	Dimension cuadrado[n];
	Para i<-1 Hasta n Hacer
		Escribir "Ingrese el " i " numero"
		Leer valores[i];
		cuadrado[i]<-valores[i]*valores[i];
		Escribir "Elevado al cuadrado es : ", cuadrado[i];
	FinPara
	
FinAlgoritmo
