Algoritmo PlatoProductoPrecio
	//Dise�e un arreglo en el que se ingrese la cantidad de productos y sus respectivos precios, para la preparaci�n de un plato, tambi�n se debe mostrar al final el costo a gastar.
	
	Escribir "Ingrese el nombre del plato"
	Leer plato
	Escribir "Ingrese la cantidad de ingredientes"
	Leer cantidadI
	Dimension productos[cantidadI];
	Dimension precios[cantidadI];
	gasto <- 0;
	
	Para i <- 1 Hasta cantidadI Hacer
		Escribir "Ingrese el nombre del " i " ingrediente " 
		Leer productos[i]
		Escribir "Ingrese el precio del " i " ingrediente " 
		Leer precios[i]
		gasto <- gasto + precios[i];
	FinPara
	
	Escribir "El costo del plato es : " gasto;

FinAlgoritmo
