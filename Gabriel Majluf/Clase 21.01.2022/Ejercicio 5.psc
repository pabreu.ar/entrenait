Algoritmo PartesPCAscendente
	//Ingresar por teclado el nombre de las partes de una computadora hasta	ingresar un dato vaci� y mostrarlos en forma ordenada de manera ascendente.
	
	Dimension listaPC[50]
	//Tome como limite 50 componentes ya que no sabia como hacerlo de otra manera
    Escribir "Ingrese las partes de la PC (ingrese valor en blanco para terminar):"
    // Leer componentes
    Leer componente
	cant<-0
    
	Mientras componente <> "" Hacer
        cant<-cant+1
        listaPC[cant]<-componente
        Repetir //Leer un componente y ver que no este ya en la lista
            Leer componente
            se_repite<-Falso
            Para i<-1 Hasta cant Hacer
                Si componente=listaPC[i] Entonces
                    se_repite<-Verdadero
                FinSi
            FinPara
        Hasta Que NO se_repite
    FinMientras
    
    //Ordenar
    Para i<-1 Hasta cant-1 Hacer
        //Busca el menor entre i y cant
        pos_menor<-i;
        Para j<-i+1 Hasta cant Hacer
            Si listaPC[j]<listaPC[pos_menor] Entonces
                pos_menor<-j;
            FinSi
        FinPara
        //Intercambia el que estaba en i con el menor que encontro
        aux<-listaPC[i];
        listaPC[i]<-listaPC[pos_menor];
        listaPC[pos_menor]<-aux;
    FinPara    
    
    //Mostrar como queda la lista
    Escribir "La lista ordenada es:"
    Para i<-1 Hasta cant Hacer
        Escribir "   ", listaPC[i];
    FinPara
	
FinAlgoritmo
