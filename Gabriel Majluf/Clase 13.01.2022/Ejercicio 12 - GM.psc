Algoritmo SumamPositivaNegativaCero
	//Escribir un programa que indique si la suma de dos valores es positiva, negativa o cero.
	
	Definir numero1, numero2 Como Entero
	Escribir "Ingrese un numero"
	Leer numero1
	Escribir "Ingrese otro numero"
	Leer numero2
	Definir suma Como Entero;
	suma = numero1 + numero2;
	Si suma < 0 Entonces
		Escribir "La suma es negativa"
	SiNo
		Si suma > 0 Entonces
			Escribir "La suma es positiva"
		SiNo
			Escribir "La suma es cero"
		FinSi
	FinSi	
	
FinAlgoritmo
