Algoritmo MarcayModeloInvertidos
//	Escribir un programa que lea de teclado la marca y modelo de un auto e imprima en pantalla el modelo y la marca (orden invertido a lo que se lee)

	Definir marca, modelo Como Cadena;
	Escribir "Ingrese la marca del auto";
	Leer  marca;
	Escribir  "Ingrese el modelo del auto";
	Leer modelo;	
	Definir resultado Como Cadena;
	resultado = Concatenar(Concatenar("el modelo es: ", modelo), Concatenar("y la marca es: ", marca)) 
	//con ,
	//resultado1 = Concatenar(Concatenar("", modelo), Concatenar(",", marca))
	Escribir resultado;
	
FinAlgoritmo
