Algoritmo VueltoDiferenciaCliente
	//Ejercicio 15: Escribir un programa que calcule el cambio que debe darse a un cliente
	
	Definir precio, pago Como Numerico
	Escribir "Ingrese el precio"
	Leer precio
	Escribir "Ingrese el pago del cliente"
	Leer pago
	Definir diferencia Como Numerico
	Si precio > 0 y pago > 0 Entonces
		Si pago > precio Entonces
			diferencia = pago - precio;
			Escribir "El vuelto que le debe dar al cliente es: ", diferencia;
		SiNo
			diferencia = precio - pago;
			Escribir "El cliente debe abonar todav�a: ", diferencia;
		FinSi
	SiNo
		Escribir "Ingrese valores positivos"
	FinSi
	
FinAlgoritmo
