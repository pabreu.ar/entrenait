//Una Funcion de suma de valores
Funcion suma <- SumarValoresDeArreglo (arreglo Por Referencia, n Por Valor)
	suma = 0;
	Para i <- 1 Hasta n Con Paso 1 Hacer
		suma = suma + arreglo[i];
	Fin Para
FinFuncion

//Un Procedimiento
Funcion IncrementeArregloEnUno (arreglo Por Referencia)
	Para i<-1 Hasta 10 Con Paso 1 Hacer
		arreglo[i] = arreglo[i] + 1;
	Fin Para
FinFuncion

//Un Procedimiento
Funcion InicializarArregloConsecutivoEn1 (arreglo Por Referencia)
	Definir i Como Entero
	Para i<-1 Hasta 10 Con Paso 1 Hacer
		arreglo[i] = i;
	Fin Para
FinFuncion

Algoritmo EjemploSumaArreglo
	Definir n Como Entero;
	n = 10;
	Dimension arreglo[n]
	
	//Inicializar el arreglo con valores del 1 al 10
	InicializarArregloConsecutivoEn1(arreglo);
	
	//Incrementar en uno el valor inicial del arreglo 
	IncrementeArregloEnUno(arreglo);
	
	Definir resultado Como Entero
	resultado = SumarValoresDeArreglo(arreglo, n);
	
	Escribir  "El resultado de la suma es: ", resultado;
FinAlgoritmo
