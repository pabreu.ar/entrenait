Funcion diaSiguiente(dia,mes,anio,ultimoDiaDelMes)
	// Muestra el d�a siguiente
	Definir diaSig, mesSig, anioSig Como Entero
	
	Si dia==ultimoDiaDelMes Entonces
		diaS <- 1
		Si mes<12 Entonces
			mesSig <- m+1
			anioSig <- anio
		SiNo
			mesSig <- 1
			anioSig <- anio + 1
		FinSi
	SiNo
		diaSig <- dia+1
		mesSig <- mes
		anioSig <- anio
	FinSi
	Escribir "D�a siguiente = ", diaSig, "/", mesSig, "/", anioSig
FinFuncion

// Recibe el a�o (year) y devuelve (lY) si es bisiesto o no
Funcion lY <- AnioBis(year)
	Definir lY Como Logico;
	// Si el a�o es m�ltiplo de 4
	Si (year MOD 4==0) Entonces
		lY <- Verdadero;
		// Si adem�s es m�ltiplo de 100 y de 400
		Si ((year MOD 100<>0) O (year MOD 400==0)) Entonces
			lY <- Verdadero;
		SiNo
			lY <- Falso;
		FinSi
	SiNo
		lY <- Falso;
	FinSi
FinFuncion

Algoritmo calcularDiaSiguiente
	Definir dia, mes, anio Como Entero
	
	Escribir "D�a: "
	Leer dia
	Escribir "Mes: "
	Leer mes
	Escribir "A�o: "
	Leer anio
	
	// Comprobar que los valores introducidos son correctos
	Mientras (anio < 2021 O ani >= 3000) Hacer
		Escribir "Los valores del a�o tienen que estar entre 2022 y 3000. Ingrese nuevamente:";
		Leer anio;
	FinMientras
	Mientras (mes < 1 O mes > 12) Hacer
		Escribir "Los valores del mes tienen que estar entre 1 y 12";
		Leer mes;
	FinMientras
	Mientras (dia < 1 O dia > 31)  Hacer
		Escribir "Los valores del d�a tienen que estar entre 1 y 31";
		Leer dia;
	FinMientras
	
	Segun mes Hacer
		1, 3, 5, 7, 8, 10, 12:
			// Si el mes tienes 31 d�as
			diaSiguiente(dia, mes, anio, 31)
			
		4, 6, 9, 11:
			// Si el mes tiene 30 d�as
			Si dia==31 Entonces
				Escribir "Este mes no tiene 31 d�as. Fecha incorrecta. Ingrese dia nuevamente".
				Leer dia;
			SiNo
				diaSiguiente(dia, mes, anio, 30)
			FinSi
			
		2:
			// Si el mes tiene 28 o 29 d�as
			// Analiza si es bisiesto o no
			Si AnioBis(anio) Entonces
				Mientras dia > 29 Hacer
					Escribir "En el a�o elegido Febrero tiene 29. Ingrese dia nuevamente";
					Leer dia;
				FinMientras
				diaSiguiente(dia,mes,anio,29)
			SiNo
				Mientras dia>28 Hacer
					Escribir "En el a�o elegido Febrero tiene 28. Fecha incorrecta. Ingrese nuevamente";
					Leer dia;
				FinMientras
				diaSiguiente(dia,mes,anio,28)
			FinSi
	FinSegun

FinAlgoritmo