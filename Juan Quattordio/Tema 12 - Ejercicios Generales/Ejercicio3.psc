Algoritmo CalcularHoraLlegadaViaje
	// Ejercicio 3: Un ciclista parte de una ciudad A a las HH horas, MM minutos y SS
	// segundos. El tiempo de viaje hasta llegar a otra ciudad B es de T segundos. Escribir un
	// algoritmo que determine la hora de llegada a la ciudad B.
	
	Definir horaSalida, minutosSalida, segSalida, tiempoViajeSeg Como Entero;
	
	Escribir "Ingrese la hora de salida de la ciudad A. Ingrese primero hora, minutos y segundos";
	Leer horaSalida, minutosSalida, segSalida;
	
	Escribir "Ingrese el tiempo de viaje en segundos";
	Leer tiempoViajeSeg;
	
	// Llevo todo a segundos y sumo los segundos del viaje
	Definir tiempoTotalSeg Como Entero;
	tiempoTotalSeg = horaSalida * 3600 + minutosSalida * 60 + segSalida + tiempoViajeSeg;
	
	Definir segRestantes, horaLlegada, minutosLlegada, segLlegada Como Entero;
	
	horaLlegada = TRUNC(tiempoTotalSeg / 3600);
	segRestantes = tiempoTotalSeg - horaLlegada*3600;
	minutosLlegada = TRUNC(segRestantes / 60);
	segRestantes = segRestantes - minutosLlegada * 60;
	segLlegada = segRestantes
	
	// Esto se realiza por si sale un d�a y llega otro.
	Definir diasViaje Como Entero;
	diasViaje = TRUNC(horaLlegada / 24);
	horaLlegada = horaLlegada % 24;
	
	Si diasViaje>0 Entonces
		Escribir "Llegar� a la ciudad B a las ", horaLlegada, ":", minutosLlegada, ":", segLlegada, " pasados ", diasViaje, " d�as";
	SiNo
		Escribir "Llegar� a la ciudad B a las ", horaLlegada, ":", minutosLlegada, ":", segLlegada, " del mismo d�a";
	FinSi
	
	
FinAlgoritmo
