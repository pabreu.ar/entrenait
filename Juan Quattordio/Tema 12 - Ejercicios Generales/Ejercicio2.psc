Algoritmo ConvertirBinarioADecimal
	// Ejercicio 2: Ingresar una cantidad en binario y transformar al sistema decimal.
	
	Escribir "Ingrese un numero binario para transformarlo a sistema decimal";
	Definir numBinario Como Cadena;
	Leer numBinario;
	// [ 1, 0, 0, 0 ] = 8
	Definir n Como Numero;
	n = LONGITUD(numBinario);
	
	Definir totalDecimal Como Entero;
	
	Definir i, num Como Entero;
	Definir valorDigitoBinario Como Entero;
	Para i = n Hasta 1 Con Paso -1 Hacer
		valorDigitoBinario = 2 ^ (n-i);
		totalDecimal = totalDecimal + valorDigitoBinario * ConvertirANumero( Subcadena(numBinario, i, i) );
	FinPara
	Escribir totalDecimal;
	
FinAlgoritmo
