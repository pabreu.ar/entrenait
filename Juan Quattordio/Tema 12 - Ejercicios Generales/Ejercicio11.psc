Funcion agruparNumeros (arreglo Por Referencia, sizeArray Por Valor, agrupacionNumeros Por Referencia)
	Para i = 1 Hasta sizeArray Hacer
		Si arreglo[i] > 80 Entonces
			agrupacionNumeros[1] = agrupacionNumeros[1] + 1;
		SiNo
			Si arreglo[i] >= 50 Y arreglo[i] <= 75 Entonces
				agrupacionNumeros[2] = agrupacionNumeros[2] + 1;
			SiNo
				Si arreglo[i] < 30 Entonces
					agrupacionNumeros[3] = agrupacionNumeros[3] + 1;
				FinSi	
			FinSi	
		FinSi
	FinPara
FinFuncion

Algoritmo agrupadorNumeros
	//Ejercicio 11: Realice un programa que le permita determinar de una lista de n�meros
	//ingresados: �Cu�ntos est�n entre el 50 y 75, ambos inclusive? �Cu�ntos mayores de
	//80?�Cu�ntos menores de 30?
	
	Definir sizeArray Como Entero;
	sizeArray = 10;
	Dimension arreglo[sizeArray];
	Definir numeroIngresado, i Como Entero;
	Para i = 1 Hasta sizeArray Hacer
		Escribir "Ingrese un valor";
		Leer numeroIngresado;
		arreglo[i] = numeroIngresado;
	FinPara
	
	Definir cantidadGrupos Como Entero;
	cantidadGrupos = 3;
	Dimension gruposNumerosValores[cantidadGrupos];
	Dimension gruposNumerosDescripcion[cantidadGrupos];
	gruposNumerosDescripcion[1] = "Mayores que 80";
	gruposNumerosDescripcion[2] = "Entre 50 y 75, inclusive";
	gruposNumerosDescripcion[3] = "Menores que 30";
	
	agruparNumeros(arreglo, sizeArray, gruposNumerosValores)
	Escribir "De los numeros ingresados hay: ";
	Para i = 1 Hasta cantidadGrupos Hacer
		Escribir gruposNumerosDescripcion[i], ": ", gruposNumerosValores[i];
	Fin Para
	
FinAlgoritmo
