Funcion resultadoSuma <- sumarElementosArray (arreglo, cantElementos)
	resultadoSuma = 0;
	Para i = 1 Hasta cantElementos Hacer
		resultadoSuma = resultadoSuma + arreglo[i];
	FinPara
FinFuncion
Algoritmo calcularNotaExamen
	//Ejercicio 4: Escribir un algoritmo para calcular la nota final de un estudiante, considerando
	//que: por cada respuesta correcta 5 puntos, por una incorrecta -1 y por respuestas en blanco
	//0. Imprime el resultado obtenido por el estudiante.
	
	Definir cantidadEjercicios Como Entero;
	cantidadEjercicios = 5;
	Dimension resultadosEjercicios[cantidadEjercicios];
	Definir resutladoEjercicio Como Entero;
	Para i = 1 Hasta cantidadEjercicios Hacer
		Escribir "Ingrese 5 si la respuesta es correcta. Ingrese -1 si la respuesta es incorrecta. Ingrese 0 si no hay respuesta";
		Leer nota;
		Mientras nota <> 5 Y nota <> -1 Y nota <> 0 Hacer
			Escribir "Ingreso no v�lido";
			Escribir "Ingrese 5 si la respuesta es correcta. Ingrese -1 si la respuesta es incorrecta. Ingrese 0 si no hay respuesta";
			Leer nota;
		FinMientras
		Segun nota Hacer
			5:
				resultadosEjercicios[i] = 5;
			-1:
				resultadosEjercicios[i] = -1;
			0:
				resultadosEjercicios[i] = 0;				
		Fin Segun
	FinPara
	
	Definir resultadoExamen Como Real;
	resultadoExamen = sumarElementosArray(resultadosEjercicios, cantidadEjercicios) / (5*cantidadEjercicios) *100;
	Si resultadoExamen <= 0 Entonces
		Escribir "La nota del examen es 0%";
	SiNo
		Escribir "La nota del examen es ", resultadoExamen,"%";
	FinSi
	
FinAlgoritmo
