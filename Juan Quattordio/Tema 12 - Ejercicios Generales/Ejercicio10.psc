Funcion cantidadCajas <- calculoCantidadCajas (produccionDiaria Por Valor, cantidadDias Por Valor, unidadesPorCaja Por Valor)
	Definir cantidadTotal Como Entero;
	cantidadTotal = produccionDiaria * cantidadDias;
	cantidadCajas = TRUNC(cantidadTotal / unidadesPorCaja)
FinFuncion

Algoritmo calculoPackaging
	//Ejercicio 10: Juan Carlos es jefe de bodega en una f�brica de pa�ales desechables y
	//sabe que la producci�n diaria es de 744 pa�ales y que en cada caja donde se empacan
	//para la venta caben 12 pa�ales. �Cu�ntas cajas debe conseguir Juan Carlos para empacar
	//los pa�ales fabricados en una semana (5 d�as)?
	
	Definir produccionDiaria, diasProduccion, unidadesPorCaja, cantidadCajas Como Entero;
	produccionDiaria = 744;
	diasProduccion = 5;
	unidadesPorCaja = 12;
	cantidadCajas = calculoCantidadCajas(produccionDiaria, diasProduccion, unidadesPorCaja);
	Escribir "Para ", diasProduccion, " dias de producci�n se necesitar�n ", cantidadCajas, " cajas";
	
FinAlgoritmo
