Algoritmo PonderacionExamenes
	// Un estudiante realiza cuatro ex�menes, los cuales tienen la misma ponderaci�n. Realizar el pseudoc�digo y el diagrama de flujo que representen el algoritmo
	// correspondiente para obtener el promedio de las calificaciones obtenidas.
	Definir nota1,nota2,nota3,nota4 Como Real
	Escribir "ingrese la nota del primer examen";
	Leer nota1;
	Escribir "ingrese la nota del segundo examen";
	Leer nota2;
	Escribir "ingrese la nota del tercer examen";
	Leer nota3;
	Escribir "ingrese la nota del cuarto examen";
	Leer nota4;
	
	Definir ponderacion1, ponderacion2, ponderacion3, ponderacion4 Como Real;
	ponderacion1 <- nota1 * 0.25;
	ponderacion2 <- nota2 * 0.25;
	ponderacion3 <- nota3 * 0.25;
	ponderacion4 <- nota4 * 0.25;
	
	Definir promedio Como Real;
	promedio <- (ponderacion1 + ponderacion2 + ponderacion3 + ponderacion4) / 4;
	
	Escribir "El promedio de las 4 notas es de: ", promedio;
FinAlgoritmo
