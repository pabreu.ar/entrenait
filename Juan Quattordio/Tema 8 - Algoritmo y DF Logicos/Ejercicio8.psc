Algoritmo SalarioEmpleado
	//Calcular el nuevo salario de un empleado si obtuvo un incremento del 8% sobre su salario actual y un descuento de 2,5% por servicios.
	
	//debemos ingresar el salario actual del empleado 
	Definir salarioActual Como Real;
	Escribir  "Ingrese su salario actual"
	Leer salarioActual
	
	Definir nuevoSalario, descuento, incremento Como Real;
	descuento <- salarioActual - (salarioActual * 2.5 / 100) 
	incremento<- salarioActual + (salarioActual * 8 / 100) 
	nuevoSalario <- salarioActual + incremento - descuento
	Escribir "Su nuevo salario es:" nuevoSalario
	
	
FinAlgoritmo

