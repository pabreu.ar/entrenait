Algoritmo sin_titulo
	//[7, 3, 1, 2, 4, 6]
	// Comienzo con el elemento 1 que ser� el elemento 0 de mi bucle y lo comparo con los n�meros anteriores. Ejemplo, 3 es el array[1] y lo comparo con 7.
	// como es menor, intercambio sus posiciones.
	// Ahroa tomo el array[2]. Como es menor que 7, intercambio 1 por 7, luego analizo contra el 3, y vuelvo a intercambiar
	// Lo mismo sucede cuando analizo el array[3]
	
	Definir n como	 Entero;
	n=6;
	Dimension array[n];
	array[1] = 7;
	array[2] = 3;
	array[3] = 1;
	array[4] = 1;
	array[5] = 4;
	array[6] = 6;
	
	Definir i, j, aux Como Entero;
	
	// Hay que mejorarlo, en el ciclo de J est� comparando contra n�meros que ya sabemos que son menores.
	Para i=2 Hasta n Hacer
		Escribir "Etapa ",i-1;
		Escribir array[1]," - ",array[2]," - ",array[3]," - ",array[4]," - ",array[5]," - ",array[6]
		Para j=i Hasta 2 Con Paso -1 Hacer
			Escribir "Analizo valor de array[",j,"] - ", array[j];
			Si array[j]<array[j-1] Entonces
				Escribir array[j], " es menor que ", array[j-1], " entonces:";
				aux = array[j-1];
				array[j-1]=array[j];
				array[j]=aux;
				Escribir array[1]," - ",array[2]," - ",array[3]," - ",array[4]," - ",array[5]," - ",array[6]
			FinSi
		FinPara
	FinPara
	
	Para i=1 Hasta n Hacer
		Escribir array[i]
	FinPara
	
FinAlgoritmo
