Funcion OrdenBurbujaAsc (arregloDesordenado Por Referencia, n Por Valor)
	Definir i Como Entero;
	Definir j Como Entero;
	Definir aux Como Entero;
	Para i = 1 Hasta n Con Paso 1 Hacer
		Para j=1 Hasta n-i Con Paso 1 Hacer	// no recorre hasta el final porque total al final ya tir� el mayor (o menor si es descendente)
			Si(arregloDesordenado[j]>arregloDesordenado[j+1]) Entonces	// Si se quiere hacer descendente, cambiar el signo ">" por "<"
				aux = arregloDesordenado[j];
				arregloDesordenado[j] = arregloDesordenado[j+1];
				arregloDesordenado[j+1] = aux;
			FinSi
		Fin Para
	Fin Para
FinFuncion

Funcion imprimirArray (arregloImprimir Por Referencia, n Por Valor)
	Definir i Como Entero;
	Para i=1 Hasta n Hacer
		Escribir arregloImprimir[i];
	FinPara
FinFuncion

Funcion indice <- busquedaBinaria(vec Por Referencia, sizeArray Por Valor, numeroBuscado Por Valor) 
	Definir indice Como Entero;
	indice = -1;
	Definir puntero Como Entero;
	puntero = 1;
	
	Definir mitad Como Entero;
	Mientras (indice = -1 Y puntero<=sizeArray) Hacer
		mitad = TRUNC ( (puntero+sizeArray)/2 )
		SI (numeroBuscado = vec[mitad]) Entonces
			indice = mitad;
		SiNo
			Si (numeroBuscado<vec[mitad]) Entonces
				sizeArray = mitad - 1;
			Sino 
				puntero = mitad + 1;
			FinSi
		FinSi
	FinMientras
FinFuncion


Algoritmo buscarNumero
	Definir final Como Entero;
	final = 5;
	Dimension vec[5];
	vec[1] = 3;
	vec[2] = 8;
	vec[3] = 11;
	vec[4] = 22;
	vec[5] = 13;
	
	Definir numeroBuscado Como Entero;
	Escribir "Favor ingresar el numero a buscar";
	Leer numeroBuscado;
	
	// Primero se debe ordenar el array
	OrdenBurbujaAsc(vec, final);
	
	// Aca busca y trae el indice si se encuentra o -1 si no.
	Definir indice Como Entero;
	indice = busquedaBinaria(vec, final, numeroBuscado);
	
	Si indice>=0 Entonces
		Escribir "El dato se encuentra en la posicion ", indice;
	SiNo
		Escribir "El dato no se encontr�";
	FinSi
	
FinAlgoritmo
