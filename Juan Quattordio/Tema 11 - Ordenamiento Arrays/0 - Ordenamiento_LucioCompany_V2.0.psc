Funcion OrdenarGenericamente(arreglo Por Referencia, n Por Valor, arregloDeSalida Por Referencia, asc Por Valor) 
	//Realizando copia del arreglo de entrada/input
	Definir i Como Entero;
	Para i <- 1 Hasta n Con Paso 1 Hacer
		arregloDeSalida[i] = arreglo[i];
	FinPara
	
	Definir dato, datoPos, j Como Entero;
	Definir intercambio Como Logico ///Solo solucion 2
	Para i <- 1 Hasta n - 1 Con Paso 1 Hacer
		//Inicio obtener el minimo/maximo del resto del vector
		dato = arregloDeSalida[i + 1];
		datoPos = i + 1;
		Para j <- i + 2 Hasta n Con Paso 1 Hacer
			////Solucion 1
			//Si asc Entonces
			//	Si dato > arregloDeSalida[j] Entonces
			//		dato = arregloDeSalida[j];
			//		datoPos = j;
			//	Fin Si
			//SiNo
			//	Si dato < arregloDeSalida[j] Entonces
			//		dato = arregloDeSalida[j];
			//		datoPos = j;
			//	Fin Si
			//Fin Si
			
			////Solucion 2
			intercambio = Falso
			
			Si asc y  dato > arregloDeSalida[j] Entonces
				intercambio = Verdadero
			FinSi
			
			Si NO asc y  dato < arregloDeSalida[j] Entonces
				intercambio = Verdadero
			FinSi
			
			Si intercambio Entonces
				dato = arregloDeSalida[j];
				datoPos = j;
			Fin Si			
   	Fin Para
	//Fin obtener el minimo del resto del vector
	//Intercambio en caso de ser mayor el elemento que estoy analizando
	///Solucion 1
//	Si asc Entonces
//		Si arregloDeSalida[i] > dato Entonces
//			dato = arregloDeSalida[i];
//			arregloDeSalida[i] = arregloDeSalida[datoPos];
//			arregloDeSalida[datoPos] = dato;
//		Fin Si
//	SiNo
//		Si arregloDeSalida[i] < dato Entonces
//			dato = arregloDeSalida[i];
//			arregloDeSalida[i] = arregloDeSalida[datoPos];
//			arregloDeSalida[datoPos] = dato;
//		Fin Si
//	Fin Si	
	///Solucion 
	intercambio = Falso
	
	Si asc y arregloDeSalida[i] > dato
		intercambio = Verdadero
	FinSi
	
	Si No asc y arregloDeSalida[i] < dato
		intercambio = Verdadero
	FinSi
	
	Si intercambio Entonces
		dato = arregloDeSalida[i];
		arregloDeSalida[i] = arregloDeSalida[datoPos];
		arregloDeSalida[datoPos] = dato;		
	FinSi
Fin Para

FinFuncion

Funcion OrdenarAscendentemente(arreglo Por Referencia, n Por Valor, arregloDeSalida Por Referencia) 
	OrdenarGenericamente(arreglo, n, arregloDeSalida, Verdadero);
FinFuncion

Funcion OrdenarDescendentemente(arreglo Por Referencia, n Por Valor, arregloDeSalida Por Referencia) 
	OrdenarGenericamente(arreglo, n, arregloDeSalida, Falso);
FinFuncion

Algoritmo Ordenamiento_LucioCompany
	Definir n como Entero;
	n <- 10;
	Dimension vector[n];
	vector[1] = 1;
	vector[2] = 3;
	vector[3] = 6;
	vector[4] = 8;
	vector[5] = 2;
	vector[6] = 5;
	vector[7] = 9;
	vector[8] = 10;
	vector[9] = 4;
	vector[10] = 7;
	
    Dimension ordenadoAscendetemente[n];
	OrdenarAscendentemente(vector, n, ordenadoAscendetemente);
	
	Dimension ordenadoDescendetemente[n];
	OrdenarDescendentemente(vector, n, ordenadoDescendetemente);
	
	Escribir "Vector Original"
	Para i<- 1 Hasta  n Hacer
		Escribir vector[i];
	FinPara
	
	Escribir "Vector Ordenado Ascendentemente"
	Para i<- 1 Hasta n Hacer
		Escribir ordenadoAscendetemente[i];
	FinPara
	
	Escribir "Vector Ordenado Descendentemente"
	Para i<- 1 Hasta n Con Paso 1 Hacer
		Escribir ordenadoDescendetemente[i];
	FinPara
FinAlgoritmo