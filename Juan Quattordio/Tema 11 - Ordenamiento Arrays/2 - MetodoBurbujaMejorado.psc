Funcion OrdenBurbujaAsc (arregloDesordenado Por Referencia, n Por Valor)
	Definir i Como Entero;
	Definir j Como Entero;
	Definir aux Como Entero;
	Para i = 1 Hasta n Con Paso 1 Hacer
		Escribir "El valor de i es ", i;
		Para j=1 Hasta n-i Con Paso 1 Hacer	// no recorre hasta el final porque total al final ya tir� el mayor (o menor si es descendente)
			Escribir "El valor de j es ", j;
			Si(arregloDesordenado[j]>arregloDesordenado[j+1]) Entonces	// Si se quiere hacer descendente, cambiar el signo ">" por "<"
				Escribir "Cambio ",arregloDesordenado[j], " por ", arregloDesordenado[j+1];
				aux = arregloDesordenado[j];
				arregloDesordenado[j] = arregloDesordenado[j+1];
				arregloDesordenado[j+1] = aux;
			FinSi
		Fin Para
	Fin Para
FinFuncion

Algoritmo OrdenarArreglo
	
	Dimension arregloDesordenado[5];
	
	arregloDesordenado[1]=4;
	arregloDesordenado[2]=5;
	arregloDesordenado[3]=2;
	arregloDesordenado[4]=1;
	arregloDesordenado[5]=3;
	
	Definir sizeArray Como Entero;
	sizeArray=5;
	
	OrdenBurbujaAsc(arregloDesordenado, sizeArray);
	
	Escribir "El arreglo ordenado es";
	Para i=1 Hasta sizeArray Hacer
		Escribir arregloDesordenado[i];
	FinPara
	
FinAlgoritmo
