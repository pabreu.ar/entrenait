Proceso CiudadMasPoblada
	//Ejercicio 5: Se quiere saber cu�l es la ciudad con la poblaci�n de m�s personas por provincia, son tres
	//provincias y once ciudades en cada provincia, hacer un algoritmo en Pseint que nos permita saber	eso.
	
	Definir provincia Como Cadena;	
	Definir ciudad Como Cadena;
	Definir cantHabitantes Como Entero;	
	Definir ciudadMasPobladaPorProvincia Como Cadena;
	Definir mayorCantHabitantesPorProvincia Como Entero;
	Definir provinciaConCiudadMasPoblada Como Cadena;
	Definir ciudadMasPobladaTotal Como Cadena;
	Definir mayorCantHabitantesTotal Como Entero;
	
	mayorCantHabitantesTotal = 0;
	Definir i, j, cantidadMaxProvincias, cantidadMaxCiudades Como Entero;
	cantidadMaxProvincias=3;
	cantidadMaxCiudades=3;
	Para i=1 Hasta cantidadMaxProvincias Con Paso 1 Hacer
		Escribir "Ingrese el nombre de la provincia a evaluar.", i, " de ", cantidadMaxProvincias, " provincias";
		Leer provincia;
		mayorCantHabitantesPorProvincia=0;
		Para j=1 Hasta cantidadMaxCiudades Con Paso 1 Hacer
			Escribir "Ingrese el nombre de la ciudad a evaluar.", j, " de ", cantidadMaxCiudades, " ciudades";
			Leer ciudad;
			Escribir "Ingrese la cantidad de habitantes que posee ", ciudad;
			Leer cantHabitantes;
			Si cantHabitantes>mayorCantHabitantesPorProvincia Entonces
				mayorCantHabitantesPorProvincia=cantHabitantes;
				ciudadMasPobladaPorProvincia=ciudad;
			FinSi
		FinPara
		Escribir "Para la provincia ", provincia, " la ciudad con m�s habitantes es ", ciudadMasPobladaPorProvincia, " con ", mayorCantHabitantesPorProvincia, " habitantes";
		Si (mayorCantHabitantesPorProvincia>mayorCantHabitantesTotal) Entonces
			provinciaConCiudadMasPoblada = provincia;
			ciudadMasPobladaTotal = ciudadMasPobladaPorProvincia;
			mayorCantHabitantesTotal = mayorCantHabitantesPorProvincia;
		FinSi
	FinPara
	Escribir "La ciudad m�s poblada entre todas es ", ciudadMasPobladaTotal, " de la provincia ", provinciaConCiudadMasPoblada, " con ", mayorCantHabitantesTotal, " habitantes";
	
FinProceso
