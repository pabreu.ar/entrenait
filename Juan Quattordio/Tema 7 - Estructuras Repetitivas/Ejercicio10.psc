Proceso SumatoriaImpares
	//Ejercicio 10: Hacer un algoritmo en Pseint para calcular la suma de los n�meros impares	menores o iguales a n.
	Definir n, i, suma Como Entero;
	Escribir "Ingresar un numero n";
	Leer n;
	suma=0;
	Para i=0 Hasta n Hacer
		Si !(i%2=0) Entonces
			suma=suma+i;
		FinSi
	FinPara
	Escribir "La suma de los numeros impares menores o iguales a ", n, " es ", suma;
	
FinProceso
