Proceso SerieFibonacci
	// Ejercicio 7: Hacer un algoritmo en Pseint para calcular la serie de Fibonacci.
	Definir iteraciones Como Entero;
	Escribir "Ingrese la cantidad de iteraciones deseadas";
	Leer iteraciones;
	
	Definir i, A, B, resultado Como Entero;
	A = 0;
	B=1;
	Escribir A;
	Escribir B;
	Para i = 1 Hasta iteraciones-2 Hacer
		resultado = A + B;
		A = B;
		B = resultado;
		Escribir resultado;
	FinPara
	
FinProceso
