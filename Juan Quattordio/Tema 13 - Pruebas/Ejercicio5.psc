// Prueba 1: Carga 1, -1, 0, 3, 5. Devuelve -1
// Prueba 2: Carga 10, 1, 0, -3, -5. Devuelve -1
// Prueba 3: Carga 0, 0, 1, 0, 0. Devuelve 0

Funcion OrdenarPorInsercionAsc(arreglo Por Referencia, n Por Valor) 
	Definir i, j, valorActual Como Entero;
	Para i <- 2 Hasta n Con Paso 1 Hacer
		valorActual <- arreglo[i];	
		j <- i;
		Mientras j > 1 y valorActual < arreglo[j - 1] Hacer
			arreglo[j] = arreglo[j - 1];
			j = j - 1;
		Fin Mientras
		arreglo[j] = valorActual;
	Fin Para
FinFuncion

Algoritmo determinarMenorNumero
	//Ejercicio 5: Leer 5 n�meros e indicar finalmente cu�l fue el menor
	Definir numeroIngresado, sizeArray, i Como Entero;
	sizeArray = 5;
	Dimension arreglo[sizeArray];
	Para i = 1 Hasta sizeArray Con Paso 1 Hacer
		Escribir "Ingrese un valor";
		Leer numeroIngresado;
		arreglo[i] = numeroIngresado;
	Fin Para
	OrdenarPorInsercionAsc(arreglo, sizeArray);
	
	Escribir "El menor numero ingresado es ", arreglo[1];
	
FinAlgoritmo
