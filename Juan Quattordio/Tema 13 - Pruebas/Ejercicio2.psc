Funcion resultado <- esPar (num Por Valor)
	resultado = num % 2 = 0;
FinFuncion

Funcion clasificarNumeros(arreglo Por Referencia, sizeArray Por Valor, arregloResultado Por Referencia, indiceTotalNumeros Por Valor, indiceCantImpares Por Valor, indiceCantPares Por Valor, indiceParMayorA100 Por Valor, indiceCantNegativos Por Valor)
	arregloResultado[indiceTotalNumeros] = sizeArray;
	
	Definir i Como Entero;
	Para i = 1 Hasta sizeArray Hacer
		Si arreglo[i] < 0 Entonces
			arregloResultado[indiceCantNegativos] = arregloResultado[indiceCantNegativos] + 1;
		SiNo
			Si esPar(arreglo[i]) Entonces
				Si arreglo[i] > 100 Entonces
					arregloResultado[indiceParMayorA100] = arregloResultado[indiceParMayorA100] + 1;
				SiNo
					arregloResultado[indiceCantPares] = arregloResultado[indiceCantPares] + 1;
				Fin Si
			SiNo
				arregloResultado[indiceCantImpares] = arregloResultado[indiceCantImpares] + 1;
			Fin Si
		FinSi
	Fin Para
FinFuncion

Algoritmo cargaYClasificacionNumeros
	//Ejercicio 2: Construya un algoritmo que permita ingresar n�meros hasta que se lean 7
	//n�meros pares (positivos) o 10 n�meros negativos. Indicar finalmente del total de n�meros le�dos
	//cu�ntos fueron impares (positivos), cu�ntos fueron pares (positivos), cu�ntos fueron pares y mayores que 100,
	//y cu�ntos fueron negativos.
	
	// Caso de Prueba:se le pasa un arreglo y devuelve un arregloResultado con estos elementos:
		// elemento 1 del resultado	es total de n�meros le�dos;
		// elemento 2 del resultado	es cantidad de impares
		// elemento 3 del resultado	es cantidad de pares
		// elemento 4 del resultado	es cantidad de pares mayores que 100
		// elemento 5 del resultado	es cantidad de negativos
	// Prueba 1: todos negativos. Ej: -1, -2, -3, -4, -5, -6, -7, -8, -9, -10. Devuelve arregloResultado = [10, 0, 0, 0, 10]
	// Prueba 2: todos positivos y pares. Ej: 2, 4, 6, 8, 10, 12, 14. Devuelve arregloResultado = [7, 0, 7, 0, 0]
	// Prueba 3: aleatorio. Ej: -1, 200, 178, 5, -6, 11, 300, 150, 321, -5, 0, 0, 0. Devuelve arregloResultado = [13, 2, 7, 4, 3]
	
	Dimension arregloResultado[5];	
	Definir indiceTotalNumeros, indiceCantImpares, indiceCantPares, indiceParMayorA100, indiceCantNegativos Como Entero;
	indiceTotalNumeros = 1;
	indiceCantImpares = 2;
	indiceCantPares = 3;
	indiceParMayorA100 = 4;
	indiceCantNegativos = 5;
	
	Definir tamanioArreglo, maxIngresos, cantNegativos, cantPares Como Entero;
	tamanioArreglo = 0;
	maxIngresos = 100;
	cantNegativos = 0;
	cantPares = 0;
	
	Definir numIngresado Como Entero;
	Dimension arreglo[maxIngresos];
	Repetir 
		Escribir "Ingrese un valor";
		Leer numIngresado;
		tamanioArreglo = tamanioArreglo + 1;
		arreglo[tamanioArreglo] = numIngresado;
		Si numIngresado < 0 Entonces
			cantNegativos = cantNegativos + 1;
		SiNo
			Si esPar(numIngresado) Entonces
				cantPares = cantPares + 1; 
			FinSi
		FinSi
	Hasta Que cantPares = 7 O cantNegativos = 10 O tamanioArreglo = 100
	
	clasificarNumeros(arreglo, tamanioArreglo, arregloResultado, indiceTotalNumeros, indiceCantImpares, indiceCantPares, indiceParMayorA100, indiceCantNegativos);
	
	Escribir "La cantidad de numeros ingresados es ", arregloResultado[indiceTotalNumeros];
	Escribir "La cantidad de numeros negativos es ", arregloResultado[indiceCantNegativos];
	Escribir "Del total de positivos igual a ", (arregloResultado[indiceParMayorA100] + arregloResultado[indiceCantImpares] + arregloResultado[indiceCantPares]);
	Escribir "la cantidad de numeros impares es ", arregloResultado[indiceCantImpares];
	Escribir "la cantidad de numeros pares es ", arregloResultado[indiceCantPares] + arregloResultado[indiceParMayorA100], " de los cuales ", arregloResultado[indiceParMayorA100], " son mayores que 100";

FinAlgoritmo
