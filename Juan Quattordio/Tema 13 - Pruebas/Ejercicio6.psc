// Prueba 1: Recibe arreglo con tiempos en segundos:
//			[12, 19, 12, 9, 13, 19, 18, 12, 17, 17]. Devuelve: Cuerpo mas rapido: 4, velocidad 80 km/h. Promedio de velocidades: 48.64
//			[20	, 17, 16, 8, 7, 14, 10, 14, 13, 15] Devuelve: Cuerpo m�s rapido: 5, velocidad 102.85 km/h. Promedio de velocidades: 53.73

Funcion cargarMediciones(mediciones Por Referencia, cantidadCuerpos Por Valor)
	Definir i, tiempoMedido Como Entero;
	Para i = 1 Hasta cantidadCuerpos Con Paso 1 Hacer
		Escribir "Ingresar el tiempo en segundos del objeto ", i;
		Leer tiempoMedido;
		mediciones[i] = tiempoMedido;
	Fin Para
	
FinFuncion

Funcion procesarMediciones(distanciaMetros Por Valor, mediciones Por Referencia, cantidadCuerpos Por Valor, objetoMasVeloz Por Referencia, maxVelocidadMedida Por Referencia, promVelocidad Por Referencia)
	Definir i Como Entero;
	Definir menorTiempo, sumaMediciones, tiempoPromedio Como Real;
	menorTiempo = mediciones[1];
	Para i = 1 Hasta cantidadCuerpos Con Paso 1 Hacer
		Si mediciones[i] <= menorTiempo Entonces
			objetoMasVeloz = i;
			menorTiempo = mediciones[i];
		FinSi
		sumaMediciones = sumaMediciones + mediciones[i]
	Fin Para
	// Se convierte a de m/s a Km/h
	maxVelocidadMedida = 3600 * distanciaMetros / menorTiempo / 1000;
	tiempoPromedio = sumaMediciones / cantidadCuerpos;
	promVelocidad = (3600 * distanciaMetros / tiempoPromedio / 1000);
FinFuncion

Algoritmo digitalizadorOptico
	//Ejercicio 6: Un digitalizador �ptico permite medir el tiempo que demora un cuerpo en
	//pasar entre un punto A y un punto B. Un digitalizador �ptico es empleado para medir el
	//tiempo que demoran diez cuerpos en recorrer un trayecto de 200 metros. Construya un
	//algoritmo que permita determinar de los diez cuerpos, cu�l fue el m�s veloz, y a cu�nto
	//ascendi� esta velocidad. Adem�s indique la velocidad promedio de los cuerpos.
	
	Definir cantidadCuerpos, distanciaMetros Como Entero;
	cantidadCuerpos = 10;
	distanciaMetros = 200;
	Dimension mediciones[cantidadCuerpos];
	cargarMediciones(mediciones, cantidadCuerpos);
	Definir objetoMasVeloz Como Entero;
	Definir maxVelocidadMedida, promVelocidad Como Real; 
	procesarMediciones(distanciaMetros, mediciones, cantidadCuerpos, objetoMasVeloz, maxVelocidadMedida, promVelocidad);
	
	Escribir "El objeto m�s veloz fue el ", objetoMasVeloz, " alcanzando los ", maxVelocidadMedida, "km/h. El promedio de velocidades es ", promVelocidad, "km/h";
	
FinAlgoritmo
