// Prueba:
// Intentar cargar estos paquetes: 10, 500, 501, 27, 1000, 0. 
// Cambiar limiteCargaAvion a 2000 e intentar cargar estos paquetes: 500, 500, 501, 250, 300, 0. 
// Cambiar limiteCargaAvion a 2000 e intentar cargar estos paquetes: 500, 500, 501, 200, 300, 500, 1, 100, 0.

Funcion resultado <- controlCargaAvion(cargaAvion Por Valor, pesoBulto Por Valor, limiteCargaAvion Por Valor, limitePesoPaquete Por Valor)
	resultado = Falso;
	Si (pesoBulto <= limitePesoPaquete Y (cargaAvion + pesoBulto) <= limiteCargaAvion )
		Escribir "Peso bulto ", pesoBulto, "cargaAvion ", cargaAvion;
		resultado = Verdadero;
	FinSi
FinFuncion

Funcion contabilizarPaquete(cargaAvion Por Referencia, pesoBulto Por Valor, cantidadBultos Por Referencia, mayorPesoBulto Por Referencia, menorPesoBulto Por Referencia, totalARS Por Referencia, tarifa1 Por Valor, tarifa2 Por Valor)
	cantidadBultos = cantidadBultos + 1;
	cargaAvion = cargaAvion + pesoBulto;
	Si cantidadBultos = 1 Entonces
		mayorPesoBulto = pesoBulto;
		menorPesoBulto = pesoBulto;
	SiNo
		Si pesoBulto > mayorPesoBulto Entonces
			mayorPesoBulto = pesoBulto;
		SiNo
			Si pesoBulto < menorPesoBulto Entonces
				menorPesoBulto = pesoBulto;
			FinSi
		FinSi
	FinSi
	
	Si pesoBulto >= 26 Y pesoBulto <= 300 Entonces
		totalARS = totalARS + pesoBulto * tarifa1;
	SiNo
		Si pesoBulto > 300 Y pesoBulto <= 500 Entonces
			totalARS = totalARS + pesoBulto * tarifa2;
		FinSi
	FinSi
FinFuncion

Algoritmo sin_titulo
//Ejercicio 7: Un BOING 747 tiene una capacidad de carga para equipaje de
//aproximadamente 18.000 kgr. Confeccione un algoritmo que controle la recepci�n de
//equipajes para este avi�n, sabiendo :
//		* Un bulto no puede exceder la capacidad de carga del avi�n ni tampoco exceder los 500 Kg.
	//	* El valor por kilo del bulto es : 
	//		- de 0 a 25 Kg. cero pesos 
	//		- de 26 a 300 Kg. 1500 pesos por kilo de equipaje. 
	//		- de 301 a 500 Kg. 2500 pesos por kilo de equipaje
//Para un vuelo cualquiera se pide:
//	A. N�mero total de bultos ingresados para el vuelo
//	B. Peso del bulto m�s pesado y del m�s liviano
//	C. Peso promedio de los bultos
//	D. Ingreso en pesos y en d�lares por concepto de carga.
	
	
	
	// Se definen constantes 
	Definir limiteCargaAvion, limitePesoPaquete Como Entero;
	Definir tarifa1, tarifa2, valorDolar Como Real;
	limiteCargaAvion = 2000; // en Kgs.
	limitePesoPaquete = 500; // en Kgs.
	tarifa1 = 1500; // es en $ARS, aplica a paquetes entre 26 y 300 kgs, inclusive
	tarifa2 = 2500; // es en $ARS, aplica a paquetes entre 301 y 500 kgs, inclusive.
	valorDolar = 100; // significa que USD 1 = $ARS 100
	
	Definir cantidadBultos, pesoBulto, mayorPesoBulto, menorPesoBulto Como Entero;
	Definir cargaAvion, totalARS Como Real;
	Definir cargaAutorizada Como Logico;
	Repetir
		Escribir "Ingrese el peso del paquete. Ingrese 0 si no hay m�s paquetes que cargar";
		Leer pesoBulto;
		Si pesoBulto <> 0 Entonces
			cargaAutorizada = controlCargaAvion(cargaAvion, pesoBulto, limiteCargaAvion, limitePesoPaquete);
			Escribir cargaAutorizada;
			Si cargaAutorizada Entonces
				contabilizarPaquete(cargaAvion, pesoBulto, cantidadBultos, mayorPesoBulto, menorPesoBulto, totalARS, tarifa1, tarifa2);
			SiNo
				Escribir "No es posible agregar este bulto de ", pesoBulto, "kgs.";
			FinSi
		FinSi
	Hasta Que cargaAvion >= limiteCargaAvion O pesoBulto=0
	
	Escribir "El numero total de bultos cargados es ", cantidadBultos;
	Escribir "El bulto m�s pesado es de ", mayorPesoBulto, "kgs, y el menor es de ", menorPesoBulto, "kgs";
	Escribir "El peso promedio de los bultos cargados es ", cargaAvion/cantidadBultos, "kgs";
	Escribir "El ingreso en $ARS es ", totalARS, " equivalente a USD", totalARS/valorDolar;
	
FinAlgoritmo
