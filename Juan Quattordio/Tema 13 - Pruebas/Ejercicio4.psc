// Prueba ingresarNumerosHastaMultiplo5
// Carga 1, -2, -6, -11, 0. cantidadIngresada = 4
// Carga 1, -2, -6, -10. cantidadIngresada = 3
// Carga 1, -2, 1, 11, 5. cantidadIngresada = 4

Funcion resultado <- esMultiplo5(num Por Valor)
	resultado = num % 5 = 0;
FinFuncion

Algoritmo ingresarNumerosHastaMultiplo5
	//Ejercicio 4: Leer una cantidad de n�meros variables hasta que se ingrese un n�mero
	//m�ltiplo de 5. Indicar el n�mero de datos que fueron ingresados, sin contar el m�ltiplo de la
	//condici�n de t�rmino.
	
	Definir numIngresado, cantidadIngresada Como Entero;
	cantidadIngresada = 0;
	Repetir
		Escribir Sin Saltar "Ingrese un numero. El ingreso finaliza cuando ingrese un multiplo de 5: ";
		Leer numIngresado;
		Si !esMultiplo5(numIngresado) Entonces
			cantidadIngresada = cantidadIngresada + 1;
		FinSi
	Hasta Que esMultiplo5(numIngresado)
	
	Escribir "La cantidad de elementos ingresados es: ", cantidadIngresada;
	
FinAlgoritmo
