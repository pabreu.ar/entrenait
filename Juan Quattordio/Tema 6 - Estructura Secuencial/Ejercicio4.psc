Proceso CapitalizacionSimple
	//Ejercicio 4: Calcula el Monto a devolver si nos prestan un capital c, a una tasa de interés t% durante n periodos.
	// Se considera una Capitalización simple donde:
	// I = Co * i * t  -->" I " son los intereses que se generan, " Co " es el capital inicial (en el momento t=0), " i " es la tasa de interés que se aplica," t " es el tiempo que dura la inversión
	// Cf = Co + I	---> "Cf" es el capital final.

	Definir capitalInicial, interesAnual Como Real;
	Escribir "Ingrese el capital inicial";
	Leer capitalInicial;
	Escribir "Ingrese el % de interes anual";
	Leer interesAnual;
	Definir meses Como Entero;
	Escribir "Ingrese la cantidad de meses para capitalizar";
	Leer meses;
	
	Definir capitalFinal, montoIntereses Como Real;
	montoIntereses = capitalInicial * interesAnual/100/12 * meses;
	capitalFinal = capitalInicial + montoIntereses;
	Escribir "En caso de una capitalizacion simple, el monto a devolver es: $", capitalFinal;
	
	
FinProceso
