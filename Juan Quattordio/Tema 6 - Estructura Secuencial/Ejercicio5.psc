Proceso ConvertirGradosSexag
	//Ejercicio 5: Escribir un programa para la conversión de grados sexagesimales a radianes	y Centesimales..
	Definir temperaturaSexagesimal Como Real;
	Escribir "Ingrese la temperatura en grados sexagesimales";
	Leer temperaturaSexagesimal;
	
	Definir unidad Como Caracter;
	
	Definir conversionRadianes Como Real;
	
	conversionRadianes = temperaturaSexagesimal * PI / 180;
	Escribir "La temperatura en radianes es: ", conversionRadianes;
	
	Definir conversionCentesimales Como Real;
	conversionCentesimales = temperaturaSexagesimal * 200 / 180;
	Escribir "La temperatura en centesimales: ", conversionCentesimales;
	
		
FinProceso
