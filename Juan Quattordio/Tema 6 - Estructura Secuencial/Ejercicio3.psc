Algoritmo ConvirtiendoTemperatura
	//Una temperatura Celsius (Centígrados) puede ser convertida a una
	//temperatura equivalente F. Escriba un programa para calcular ambos valores.
	
	//Ingresar temperatura
	//Preguntar en que sistema esta la temp. Celsius (c) o Farenheit (f) ?
	//Si escribio C calculo a F =  (gradosCelsius * 9/5) + 32
	//Si escribio F calculo a C =  ( 1gradoF - 32) * 5/9
	//Sino encribio C ni F, error
	
	Definir temperaturaInicial Como Real;
	Escribir "Ingrese la temperatura";
	Leer temperaturaInicial;
	
	Definir unidad Como Caracter;
	Escribir "Ingrese si la unidad es Celsius (c) o Farenheit (f) ";
	Leer unidad;
	
	Definir resultado Como Real;
	Si Mayusculas(unidad) = "C" Entonces
		resultado <- (temperaturaInicial * 9/5) + 32;
		Escribir "La temperatura en grados Farenheit es: ", resultado;
	FinSi
	
	Si Mayusculas(unidad) = "F" Entonces
		resultado <- (temperaturaInicial - 32) * 5/9;
		Escribir "La temperatura en grados Celsius es: ", resultado;
	FinSi
	
	Si Mayusculas(unidad) <> "C" y Mayusculas(unidad) <> "F" Entonces
		Escribir "La temperatura no es grados Celsius ni Farenheit";
	FinSi
FinAlgoritmo
