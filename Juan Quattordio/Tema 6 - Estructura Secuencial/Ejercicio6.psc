Proceso CapitalizacionMensual
	//Ejercicio 6: Suponga que un individuo desea invertir su capital en un banco y desea saber cu�nto dinero ganar� despu�s de un mes si el banco paga a raz�n de 2% mensual.
	
	Definir capital Como Real;
	Escribir "Ingrese el capital a invertir";
	Leer capital;
	Definir tasaMensual Como Real;
	tasaMensual = 2 / 100;
	Definir cantMeses Como Entero;
	cantMeses=1;
	
	Definir interesesGanados Como Real;
	interesesGanados = capital*tasaMensual*cantMeses;
	Definir capitalFinal Como Real;
	capitalFinal = capital + interesesGanados;
	
	Escribir "Ganar� $", interesesGanados, " en intereses. Por lo que su capital final ser� $", capitalFinal;
	
	
FinProceso
