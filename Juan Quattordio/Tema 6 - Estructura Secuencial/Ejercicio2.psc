Algoritmo SumaDeLasCifras
	//Escribir un programa que determine la suma de las cifras de un entero positivo de 4 cifras..
	//valores permitidos numeros entre 1000 y 9999
	//desglosar el numero ingresado, dividir entre 1000
	//me quedo con el resto y lo divido entre 100
	//me quedo con el resto y lo divido entre 10 y este resto es el valor de las unidades
	
	//Entrada
	Definir num Como Entero;
	Escribir "Ingrese un numero de 4 cifras entre 1000 y 9999";
	Leer num;
	//Proceso 
	Definir millares, centena, decena, unidad, resto Como Entero;
	millares <- Trunc(num / 1000);
	resto <- num MOD 1000;
	centena <- Trunc(resto / 100);
	resto <- resto MOD 100;
	decena <- Trunc(resto / 10);
	unidad <- resto MOD 10;
	//Salida
	Definir suma Como Entero;
	suma <- millares + centena + decena + unidad;
	Escribir "La suma de las 4 cifras es: ", suma;
FinAlgoritmo
