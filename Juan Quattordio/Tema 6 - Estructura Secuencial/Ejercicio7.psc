Proceso CalcularNotaFinal
//Ejercicio 7: Un alumno desea saber cu�l ser� su calificaci�n final en la materia de
//Algoritmos. Dicha calificaci�n se compone de los siguientes porcentajes:
	//55% del promedio de sus tres calificaciones parciales.
	//30% de la calificaci�n del ex�men final.
	//15% de la calificaci�n de un trabajo final.
	
	Definir parcial1, parcial2, parcial3, examenFinal, trabajoFinal Como Entero;
	Escribir "Ingrese la calificacion del parcial 1";
	Leer parcial1;
	Escribir "Ingrese la calificacion del parcial 2";
	Leer parcial2;
	Escribir "Ingrese la calificacion del parcial 3";
	Leer parcial3;
	Escribir "Ingrese la calificacion del examen final";
	Leer examenFinal;
	Escribir "Ingrese la calificacion del trabajo final";
	Leer trabajoFinal;
	
	Definir notaFinal Como Real;
	notaFinal = 55/100 * (parcial1+parcial2+parcial3)/3 + 30/100 * examenFinal + 15/100 * trabajoFinal;
	Escribir "La nota final sera: ", notaFinal;
	
FinProceso
