Proceso CalcularHipotenusa
	// Segun teorema de Pitagoras
	Definir cateto1,cateto2 Como Real;
	Escribir 'Ingrese el valor del cateto 1';
	Leer cateto1;
	Escribir 'Ingrese el valor del cateto 2';
	Leer cateto2;
	Definir hipotenusa Como Real;
	hipotenusa <- Raiz(cateto1^2+cateto2^2);
	Escribir 'La hipotenusa es: ',hipotenusa;
FinProceso
