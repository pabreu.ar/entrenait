Proceso CalcularPorcentajeVarones
	Definir cantVarones,cantMujeres Como Entero;
	Escribir 'Ingrese la cantidad de varones en el curso';
	Leer cantVarones;
	Escribir 'Ingrese la cantidad de mujeres en el curso';
	Leer cantMujeres;
	Definir totalAlumnos Como Entero;
	totalAlumnos <- cantVarones+cantMujeres;
	Definir porcentajeVarones Como Real;
	porcentajeVarones <- cantVarones*100/totalAlumnos;
	Escribir 'El porcentaje de varones en el curso es: ',porcentajeVarones,'%';
	Escribir 'El porcentaje de mujeres en el curso es: ',100-porcentajeVarones,'%';
FinProceso
