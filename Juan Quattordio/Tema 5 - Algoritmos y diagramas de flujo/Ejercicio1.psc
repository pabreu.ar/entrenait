Proceso DeterminarMayorValor
	Definir valor1,valor2 Como Real;
	Escribir 'Ingrese un valor';
	Leer valor1;
	Escribir 'Ingrese otro valor';
	Leer valor2;
	Definir mayor Como Real;
	mayor <- valor1;
	Si valor1<valor2 Entonces
		mayor <- valor2;
	FinSi
	Escribir 'El mayor de los dos valores es: ',mayor;
FinProceso
