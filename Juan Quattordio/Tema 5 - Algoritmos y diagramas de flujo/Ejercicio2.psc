Proceso MostrarMayorMenorValor
	Definir A,B,C Como Real;
	Escribir 'Ingrese un valor para A';
	Leer A;
	Escribir 'Ingrese un valor para B';
	Leer B;
	Mientras A=B Hacer
		Escribir 'Ingrese un valor de B diferente de A';
		Leer B;
	FinMientras
	Escribir 'Ingrese un valor para C';
	Leer C;
	Mientras A=C O B=C Hacer
		Escribir 'Ingrese un valor de C diferente de A y B';
		Leer C;
	FinMientras
	Definir max,min Como Real;
	Si A>B Entonces
		max <- A;
		min <- B;
	SiNo
		max <- B;
		min <- A;
	FinSi
	Si C>max Entonces
		max <- C;
	SiNo
		Si C<min Entonces
			min <- C;
		FinSi
	FinSi
	Escribir 'El mayor valor es ',max;
	Escribir 'El menor valor es ',min;
FinProceso
