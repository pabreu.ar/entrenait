Proceso AdivinarPalabra
	Definir palabraIngresada, palabraClave como Cadena;
	palabraClave = 'eureka';
	Definir intentos Como Entero;
	intentos = 1;
	Escribir "Ingrese la palabra clave";
	Leer palabraIngresada;
	Mientras intentos<3 Y palabraIngresada<>palabraClave Hacer
		Escribir "No ha descubierto la palabra clave. Ingrese nuevamente";
		Leer palabraIngresada;
		intentos = intentos + 1;
	FinMientras
	Si palabraIngresada=palabraClave Entonces
		Escribir "Ha descubierto la palabra clave: eureka";
	SiNo
		Escribir "No ha descubierto la palabra y se agotaron los intentos";
	FinSi
FinProceso
