Proceso DeterminarNumeroPrimo
	Definir num Como Entero;
	Repetir
		Escribir "Ingresar un numero mayor a 1";
		Leer num;
		// Numero primo es un numero mayor que 1 divisible s�lo por 1 y por s� mismo.
	Hasta Que num>1
	Definir esPrimo Como Logico;
	esPrimo = Verdadero;
	Definir divisor Como Numero;
	divisor = 2;
	Mientras esPrimo Y divisor<num Hacer
		esPrimo = num%divisor<>0;
		divisor = divisor+1;
	FinMientras
	Si esPrimo Entonces
		Escribir "El numero es primo";
	SiNo
		Escribir "El numero no es primo";
	FinSi
FinProceso
