Proceso CalculoAniosMesesSemanasDias
	Escribir "Ingrese los dias para convertir en a�os, meses, semanas y dias";
	Definir cantidadDias Como Entero;
	Leer cantidadDias;
	Definir diasRestantes Como Entero;
	Definir anios Como Entero;
	anios = TRUNC(cantidadDias / 365);
	diasRestantes = cantidadDias - anios * 365;
	Definir meses Como Entero;
	// Se considera un promedio de 30 d�as por mes.
	meses =  TRUNC(diasRestantes / 30);
	diasRestantes = diasRestantes - meses * 30;
	Definir semanas Como Entero;
	semanas = TRUNC(diasRestantes / 7);
	diasRestantes = diasRestantes - semanas*7;
	Escribir "Es equivalente a ", anios, " a�os, ", meses, " meses, ", semanas, " semanas y ", diasRestantes, " dias.";
FinProceso
