Funcion suma <- SumarValoresDeArreglo(arreglo Por Referencia, sizeArreglo Por Valor)
	Definir suma Como Entero;
	suma = 0;
	Definir i Como Entero;
	Para i=1 Hasta sizeArreglo Con Paso  1 Hacer
			suma = suma + arreglo[i];
	FinPara
FinFuncion

Funcion IncrementeArregloEnUno(arreglo Por Referencia, sizeArreglo Por Valor)
	Definir i Como Entero;
	Para i <- 1 Hasta sizeArreglo Con Paso 1 Hacer
		arreglo[i] <- i + 1;
	FinPara
FinFuncion

Funcion InicializarArregloConsecutivoEn1 (arreglo Por Referencia, sizeArreglo Por Valor)
	Definir i Como Entero;
	Para i <- 1 Hasta sizeArreglo Con Paso 1 Hacer
		arreglo[i] <- i;
	FinPara
FinFuncion

Algoritmo EjemploSumaArregloDinamico
		Definir sizeArreglo Como Entero;
		sizeArreglo = 10;
		Dimension arreglo[sizeArreglo];
		// Ejemplo de matriz o array multidimensional
		//Dimension arreglo[i, j];
		
		// Los valores ingresados por teclado, siempre pasarlos Por Valor, NO Por Referencia, para evitar que sea modificado y que el programa se comporte de manera inesperada.
		// Inicializar el arreglo con valores del 1 al tama�o del arreglo
		InicializarArregloConsecutivoEn1(arreglo, sizeArreglo);
		
		// Incrementar en uno el valor inicial del arreglo
		IncrementeArregloEnUno(arreglo, sizeArreglo);
		
		Definir resultado Como Entero;
		resultado = SumarValoresDeArreglo(arreglo, sizeArreglo);
		Escribir "El resultado de la suma es: ", resultado;
FinAlgoritmo


