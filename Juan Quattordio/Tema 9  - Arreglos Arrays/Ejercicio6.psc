Funcion factorial <- CalcularFactorial (num por valor)
	Definir i, factorial Como Entero;
	factorial=1;
	Para i=1 Hasta num Hacer
		factorial=factorial*i;
	FinPara
FinFuncion

Proceso AlmacenarFactorial
	//Ejercicio 6: Empleando un arreglo que almacena el factorial de un n�mero, para luego mostrarlo.
	Definir n Como Entero;
	Escribir "Ingresar un numero n";
	Leer n;
	Dimension resultadoFactorial[1];
	resultadoFactorial[1]=CalcularFactorial(n);
	
	Escribir "El factorial de ", n, " es ", resultadoFactorial[1];
	
FinProceso
