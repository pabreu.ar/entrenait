Algoritmo IngresoNumerosMostrarMayores
	//Ejercicio 4: Ingresar n�meros, almacenarlos en un arreglo y mostrar el n�mero o elemento mayor, as� como el segundo mayor.
	
	Definir i Como Entero;
	Definir cantidadValor Como Entero;
	cantidadValor = 5;
	Dimension arreglo[cantidadValor];
	Para i <- 1 Hasta cantidadValor Con Paso 1 Hacer
		Escribir "Ingrese un numero";
		Leer arreglo[i];
	FinPara
	
	Definir numeroMayor Como Entero;
	Definir numeroSegundoMayor Como Entero;
	i=1;
	
	Si arreglo[1] >= arreglo[2] Entonces
		numeroMayor = arreglo[1];
		numeroSegundoMayor= arreglo[2];
	FinSi
	
	Para i <- 3 Hasta cantidadValor Con Paso 1 Hacer
		Si arreglo[i]>numeroMayor Entonces
			numeroSegundoMayor=numeroMayor;
			numeroMayor=arreglo[i];
		SiNo
			Si arreglo[i]>numeroSegundoMayor Entonces
				numeroSegundoMayor=arreglo[i];
			FinSi
		FinSi
	FinPara
	
	Escribir "El mayor valor ingresado es ", numeroMayor;
	Escribir "El segundo mayor valor ingresado es ", numeroSegundoMayor;
		
FinAlgoritmo
