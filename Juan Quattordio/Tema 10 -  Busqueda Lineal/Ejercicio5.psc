Funcion InicializarBaseDeDatos(numTelefonosOrigen Por Referencia, numTelefonosDestino Por Referencia, fechaLlamadas Por Referencia, horaInicioLlamadas Por Referencia, cantMinutosLlamadas Por Referencia)
	numTelefonosOrigen[1] = "431567";
	numTelefonosDestino[1] = "503987";
	fechaLlamadas[1] = "20/04/06";
	horaInicioLlamadas[1] = "09:15";
	cantMinutosLlamadas[1] = 3;
	
	numTelefonosOrigen[2] = "1513567";
	numTelefonosDestino[2] = "1531414";
	fechaLlamadas[2] = "21/06/06";
	horaInicioLlamadas[2] = "19:15";
	cantMinutosLlamadas[2] = 23;
	
	numTelefonosOrigen[3] = "428159";
	numTelefonosDestino[3] = "427654";
	fechaLlamadas[3] = "21/09/10";
	horaInicioLlamadas[3] = "20:15";
	cantMinutosLlamadas[3] = 60;
	
	numTelefonosOrigen[4] = "448959";
	numTelefonosDestino[4] = "416554";
	fechaLlamadas[4] = "22/10/10";
	horaInicioLlamadas[4] = "10:14";
	cantMinutosLlamadas[4] = 20;	
FinFuncion

Funcion position <- BusquedaSecuencial(arreglo1 Por Referencia, arreglo2 Por Referencia, llave1 Por Valor, llave2 Por Valor, n Por Valor)
	Definir position Como Entero;
	position = -1;
	Definir i Como Entero;
	i=1;
	Mientras (position = -1 Y i <= n) Hacer
		Si (arreglo1[i] = llave1 Y arreglo2[i] = llave2) Entonces
			position = i;
		FinSi
		i = i + 1;
	FinMientras
FinFuncion

Algoritmo BuscarDatosLlamada
	//Ejercicio 5: La empresa de comunicaciones L�nea Segura registra el origen, el destino, la
	//fecha, la hora y la duraci�n de todas las llamadas que hacen sus usuarios, para esto hace
	//uso de cinco vectores. Se requiere un algoritmo para saber si desde un tel�fono
	//determinado se ha efectuado alguna llamada a un destino y en caso afirmativo conocer la
	//fecha, la hora y la duraci�n de la llamada.
	Definir n Como Entero;
	n = 4;
	Dimension numTelefonosOrigen[n];
	Dimension numTelefonosDestino[n];
	Dimension fechaLlamadas[n];
	Dimension horaInicioLlamadas[n];
	Dimension cantMinutosLlamadas[n];
	
	InicializarBaseDeDatos(numTelefonosOrigen, numTelefonosDestino, fechaLlamadas, horaInicioLlamadas, cantMinutosLlamadas);
	
	Definir numOrigen como Cadena;
	Escribir "Ingrese el numero de telefono de origen buscado";
	Leer numOrigen;
	Definir numDestino como Cadena;
	Escribir "Ingrese el numero de telefono de destino buscado";
	Leer numDestino;
	
	Definir posicion Como Entero;
	posicion = BusquedaSecuencial(numTelefonosOrigen, numTelefonosDestino, numOrigen, numDestino, n);
	
	Si posicion <> -1 Entonces
		Escribir "El numero ",numTelefonosOrigen[posicion], " llam� a ", numTelefonosDestino[posicion], " el d�a ", fechaLlamadas[posicion], " a las ", horaInicioLlamadas[posicion], "horas y dur� ", cantMinutosLlamadas[posicion], " minutos";
	SiNo
		Escribir "No se encontr� un llamado de ",numTelefonosOrigen[posicion], " a ", numTelefonosDestino[posicion];
	FinSi
	
FinAlgoritmo
