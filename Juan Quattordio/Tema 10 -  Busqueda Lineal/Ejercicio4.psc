Funcion  InicializarBaseDeDatos(facturas Por Referencia, clientes Por Referencia, valoresFacturas Por Referencia, fechasFacturas Por Referencia)
	facturas[1] = 'F00-001';
	clientes[1] = 'Leopoldo Baggio';
	valoresFacturas[1] = 196;
	fechasFacturas[1] = '24-01-2020';
	
	facturas[2] = 'F00-002';
	clientes[2] = 'Leopoldo Baggio';
	valoresFacturas[2] = 200;
	fechasFacturas[2] = '28-01-2020';
	
	facturas[3] = 'F00-003';
	clientes[3] = 'Claudio Paul';
	valoresFacturas[3] = 400;
	fechasFacturas[3] = '20-03-2021';
	
	facturas[4] = 'F00-004';
	clientes[4] = 'Carlos Bou';
	valoresFacturas[4] = 600;
	fechasFacturas[4] = '24-03-2021';
	
	facturas[5] = 'F00-005';
	clientes[5] = 'Claudio Paul';
	valoresFacturas[5] = 100;
	fechasFacturas[5] = '30-03-2021';
FinFuncion

Funcion resultado <- EvaluacionBusquedaSecuencial(arreglo Por Referencia, i Por Valor, llave Por Valor)
	resultado = Falso;
	Si (arreglo[i] = llave) Entonces
		resultado = Verdadero;
	FinSi
FinFuncion

Funcion position <- BusquedaSecuencial(arreglo Por Referencia, llave Por Valor, n Por Valor)
	Definir position Como Entero;
	position = -1;
	Definir i Como Entero;
	i = 1;
	Mientras (position = -1 Y i <= n) Hacer
		Si ( EvaluacionBusquedaSecuencial(arreglo, i, llave) ) Entonces
			position = i;
		FinSi
		i = i + 1;
	FinMientras
FinFuncion

Funcion fila <- busquedaMatrizVuelos(matriz Por Referencia, origen Por Valor, destino Por Valor, colOrigen Por Valor, colDestino Por Valor n Por Valor)
	Definir fila Como Entero;
	fila = -1;
	Definir i Como Entero;
	i=1;
	Mientras (fila = -1 Y i <= n) Hacer
		Si (matriz[i, colOrigen] = origen Y matriz[i, colDestino] = destino) Entonces
			fila = i;
		FinSi
		i = i + 1;
	FinMientras
FinFuncion

	
Algoritmo BuscarFacturacion
	//Ejercicio 4: En el almac�n TodoCaro se cuenta con los datos: n�mero de factura, nombre
	//del cliente, fecha de facturaci�n y valor de la factura, almacenados en vectores. Se desea
	//un algoritmo que lea el n�mero de factura y muestre los dem�s datos.
	Definir n Como Entero;
	n = 5;
	Dimension facturas[n];
	Dimension clientes[n];
	Dimension valoresFacturas[n];
	Dimension fechasFacturas[n]
		
	InicializarBaseDeDatos(facturas, clientes, valoresFacturas, fechasFacturas);
	
	Definir facturaBuscada como Cadena;
	Escribir "Ingrese el codigo de factura buscado";
	Leer facturaBuscada;
	
	Definir posicion Como Entero;
	posicion = BusquedaSecuencial(facturas, facturaBuscada, n);
	
	Si posicion <> -1 Entonces
		Escribir "Factura codigo ", facturas(posicion);
		Escribir "Cliente ", clientes(posicion);
		Escribir "Valor de factura $", valoresFacturas(posicion);
		Escribir "Fecha factura ", fechasFacturas(posicion);
	SiNo
		Escribir "La factura ", facturaBuscada, " no se encontr�";
	FinSi
	
FinAlgoritmo
