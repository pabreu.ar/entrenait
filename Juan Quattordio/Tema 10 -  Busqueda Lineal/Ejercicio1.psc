// Esta funcion carga los valores segun una BD
Funcion  InicializarBaseDeDatos(codigos Por Referencia, nombres Por Referencia, notas Por Referencia)
	notas[1]=8;
	codigos[1]=1234;
	nombres[1]="Juan";
	notas[2]=9;
	codigos[2]=5678;
	nombres[2]="Anto";
	notas[3]=7;
	codigos[3]=2134;
	nombres[3]="Bosca";
	notas[4]=6;
	codigos[4]=7531;
	nombres[4]="Carlos";
FinFuncion

Funcion position <- busquedaSecuencial(arreglo Por Referencia, llave Por Valor, n Por Valor)
	Definir position Como Entero;
	position = -1;
	Definir i Como Entero;
	i=1;
	Mientras (position = -1 Y i<=n) Hacer
		Si (arreglo[i]=llave) Entonces
			position=i;
		FinSi
		i=i+1;
	FinMientras
FinFuncion

Algoritmo DatosDeEstudiantesDeSecundaria
	//Un profesor guarda los datos de sus estudiantes en tres vectores: c�digo,
	//nombre y nota. Se requiere un algoritmo para consultar la nota de un estudiante a partir de su c�digo.
	
	Definir n Como Entero;
	n = 4;
	Dimension notas[n];
	Dimension codigos[n];
	Dimension nombres[n];
	
	// Se inicializan los arreglos
	InicializarBaseDeDatos(codigos, nombres, notas);
	
	Definir opcionMenu Como Entero;
	Escribir "Ingrese 1 si desea buscar por codigo. Ingrese 2 si desea buscar por nombre. Otro ingreso sale del programa";
	Leer opcionMenu;
	Definir valorBuscado Como Cadena;
	Segun opcionMenu Hacer
		1:
			Escribir "Ingrese el c�digo del alumno cuya nota se quiere saber";
			Leer valorBuscado;
			position = busquedaSecuencial(codigos, valorBuscado, n);
			Si position=-1 Entonces
				Escribir "No se ha encontrado el alumno con el c�digo ", valorBuscado;
			SiNo
				Escribir "La nota del alumno ", nombres[position], " es ", notas[position];
			FinSi
		2:
			Escribir "Ingrese el nombre del alumno cuya nota se quiere saber";
			Leer valorBuscado;
			position = busquedaSecuencial(nombres, valorBuscado, n);
			Si position=-1 Entonces
				Escribir "No se ha encontrado el alumno con el nombre ", valorBuscado;
			SiNo
				Escribir "La nota del alumno ", nombres[position], " es ", notas[position];
			FinSi
		De Otro Modo:
			Escribir "Gracias por usar el programa de busqueda secuencial";
	Fin Segun
	
FinAlgoritmo
