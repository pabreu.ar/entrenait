Proceso EsDivisiblePor14
	//Ejercicio 11: Escribir un programa que indique si un n�mero es divisible entre 14
	
	definir num Como Entero;
	escribir "Elija un numero ";
	Leer num;
	
	definir resultado Como Texto;
	si num%14=0 Entonces
		resultado="SI";
	SiNo
		resultado="NO";
	FinSi
	Escribir "El numero ", num," ", resultado, " es divisible por 14";

FinProceso
