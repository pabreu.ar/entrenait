Proceso AnalizarResultadoSuma
	//Ejercicio 12: Escribir un programa que indique si la suma de dos valores es positiva,	negativa o cero.
	
	definir num1, num2 Como Real;
	escribir "Elija 2 numeros";
	escribir "Elija numero 1: ";
	Leer num1;
	escribir "elija numero 2: ";
	Leer num2;
	
	Definir resultado Como Cadena;
	si num1+num2=0 Entonces
		resultado="La suma es 0";
	SiNo
		Si num1+num2>0 Entonces
			resultado="La suma es positiva";
		SiNo
				resultado="La suma es negativa";
		FinSi
	FinSi	

	Escribir resultado;
FinProceso
