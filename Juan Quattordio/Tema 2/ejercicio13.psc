Proceso EsDivisiblePor2y5
	//Ejercicio 13: Escribir un programa que indique si un n�mero es divisible entre dos y cinco (a la vez)
	definir num Como Entero;
	escribir "Elija un numero ";
	Leer num;
	
	definir resultado Como Texto;
	si num%2=0 y num%5=0 Entonces
		resultado="SI";
	SiNo
		resultado="NO";
	FinSi
	Escribir "El numero ", num," ", resultado, " es divisible por 2 y por 5";
FinProceso
