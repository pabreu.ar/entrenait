Proceso MostrarInvertidoMarcaModeloVehiculo
	//Escribir un programa que lea de teclado la marca y modelo de un auto e imprima en pantalla el modelo y la marca (orden invertido a lo que se lee)
	
	Definir marca, modelo Como Cadena;
	Escribir "Ingrese marca";
	Leer marca;
	Escribir "Ingrese modelo";
	Leer modelo;
	
	Definir resultado Como Cadena;
	resultado = Concatenar(resultado,modelo);
	resultado = Concatenar(resultado,",");
	resultado = Concatenar(resultado,marca);
	
	Escribir "El resultado es ", resultado;
FinProceso
