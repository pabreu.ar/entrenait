Algoritmo InversionDeCapitalEnBanco
	//Suponga que un individuo desea invertir su capital en un banco y desea
	//saber cu�nto dinero ganar� despu�s de un mes si el banco paga a raz�n de 2% mensual.
	
	Definir capitalInvertido Como Entero;
	Escribir "Ingrese el capital que desea invertir en el banco";
	Leer capitalInvertido;
	
	Definir porcentajeMensual Como Entero;
	porcentajeMensual <- ((capitalInvertido * 2) / 100);
	
	Definir capitalConGanancia Como Entero;
	capitalConGanancia <- capitalInvertido + porcentajeMensual;
	
	Escribir "El capital que usted invirti� fue de: $" capitalInvertido;
	Escribir "La ganancia que le da el banco ser� de: $", porcentajeMensual;
	Escribir "El total de su inversi�n con la ganancia seria de: $", capitalConGanancia;

FinAlgoritmo
