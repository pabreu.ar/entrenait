Algoritmo GradosCelsiusAFarenheit
	//Una temperatura Celsius (Centígrados) puede ser convertida a una
	//temperatura equivalente F. Escriba un programa para calcular ambos valores.
	
	Definir grado Como Caracter;
	Escribir "Ingrese C si va a ingresar grados Celsius o ingrese F si va a ingresar grados Fahrenheit";
	Leer grado;
	
	Definir tempCelsius, calculoCelsius Como Real;
	
	Definir tempFahrenheit, calculoFahren Como Real;
	
	Si grado = "C" Entonces
		Escribir "Ingrese la temperatura en grados Celsius";
		Leer tempCelsius;
		calculoCelsius <- (tempCelsius * 1.8) + 32;
		Escribir "La temperatura en grados F seria de: ", calculoCelsius, "°";
	Fin Si
	
	Si grado = "F" Entonces
		Escribir "Ingrese la temperatura en grados Fahrenheits";
		Leer tempFahrenheit;
		calculoFahren <- (tempFahrenheit - 32) / 1.8;
		Escribir "La temperatura en grados C seria de: ", calculoFahren, "°";
	Fin Si
	
FinAlgoritmo
