Algoritmo numDePulsaciones
	//Calcular el n�mero de pulsaciones que una persona debe tener por cada 10
	//segundos de ejercicio, si la f�rmula es: num. pulsaciones = (220 - edad)/10
	
	Definir edad Como Entero;
	Escribir "Ingrese su edad";
	Leer edad;
	
	Definir numPulsaciones Como Real;
	
	
	Si edad > 4 Entonces
		numPulsaciones <- (220 - edad) / 10;
		Escribir "El numero de pulsaciones que debe tener por cada 10 seg de ejercicio es: ", numPulsaciones;
	SiNo
		Escribir "Usted no tiene edad para hacer ejercicio";
	Fin Si

FinAlgoritmo
