Algoritmo CalculoNotaFinal
	//Un alumno desea saber cu�l ser� su calificaci�n final en la materia de
	//Algoritmos. Dicha calificaci�n se compone de los siguientes porcentajes:
	//55% del promedio de sus tres calificaciones parciales.
	//30% de la calificaci�n del ex�men final.
	//15% de la calificaci�n de un trabajo final.
	
	Definir notaParcial1, notaParcial2, notaParcial3 Como Real;
	Escribir "Ingrese las notas de los 3 parciales";
	Leer notaParcial1, notaParcial2, notaParcial3;
	
	Definir notaExamenFinal Como Real;
	Escribir "Ingrese la nota del examen final";
	Leer notaExamenFinal;
	
	Definir notaTrabajoFinal Como Real;
	Escribir "Ingrese la nota del trabajo final";
	Leer notaTrabajoFinal;
	
	Definir promedioNotasParcial Como Real;
	promedioNotasParcial <- ((notaParcial1 + notaParcial2 + notaParcial3) /3);
	
	Definir porcentajeParcial, porcentajeExamen, porcentajeTrabajo Como Real;
	porcentajeParcial <- ((promedioNotasParcial * 55) / 100);
	porcentajeExamen <- ((notaExamenFinal * 30) / 100);
	porcentajeTrabajo <- ((notaTrabajoFinal * 15) / 100);
	
	Definir calificacionFinal Como Real;
	calificacionFinal <- porcentajeParcial + porcentajeExamen + porcentajeTrabajo;
	
	Escribir "Su calificaci�n final es: ", calificacionFinal;

FinAlgoritmo
