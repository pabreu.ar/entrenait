Algoritmo PresupuestoAnualHospital
	//En un hospital existen tres �reas: Ginecolog�a, Pediatr�a, Traumatolog�a. El
    //presupuesto anual del hospital se reparte conforme a la sig. tabla:
    //�rea: % del presupuesto:
		//Ginecolog�a 40%
		//Traumatolog�a 30%
	    //Pediatr�a 30%
	//Obtener la cantidad de dinero que recibir� cada �rea, para cualquier monto presupuestal.
	
	Definir presupuesto Como Real;
	Escribir "Ingrese el monto presupuestal a invertir";
	Leer presupuesto;
	
	Definir porcGinecologia, porcTraumatologia, porcPediatria Como Real;
	porcGinecologia <- ((presupuesto * 40) / 100);
	porcPediatria <- ((presupuesto * 30) / 100);
	porcTraumatologia <- ((presupuesto * 30) / 100);
	
	Escribir "El monto que se invirti� fue de: $", presupuesto;
	Escribir "El area de Ginecolog�a recibir�: $", porcGinecologia;
	Escribir "El area de Traumatolog�a recibir�: $", porcTraumatologia;
	Escribir "El area de Pediatr�a recibir�: $", porcPediatria;
	
FinAlgoritmo
