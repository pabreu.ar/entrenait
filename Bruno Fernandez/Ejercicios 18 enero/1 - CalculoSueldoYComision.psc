Algoritmo CalculoSueldoYComision
	//Un vendedor recibe un sueldo base m�s un 10% extra por comisi�n de sus
	//ventas. El vendedor desea saber cu�nto dinero obtendr� por concepto de comisiones por
	//las tres ventas que realiza en el mes y el total que recibir� en el mes tomando en cuenta su
	//sueldo base y sus comisiones.
	
	Definir sueldoBase Como Entero;
	Escribir "Ingrese su sueldo base";
	Leer sueldoBase;
	
	Definir porcentajeComision Como Entero;
	porcentajeComision <- (sueldoBase * 10) / 100
	
	Definir canVentas Como Entero;
	Escribir "Ingrese la cantidad de ventas del mes";
	Leer canVentas;
	
	Definir comisionFinal Como Entero;
	comisionFinal <- porcentajeComision * canVentas;
	
	Definir sueldoFinal Como Entero;
	sueldoFinal <- sueldoBase + comisionFinal
	
	Escribir "La comision final ser� de: $", comisionFinal;
	Escribir "El sueldo final ser� de: $", sueldoFinal;
FinAlgoritmo
