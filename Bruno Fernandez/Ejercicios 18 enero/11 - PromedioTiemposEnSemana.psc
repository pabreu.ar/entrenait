Algoritmo PromedioTiemposEnSemana
	//Todos los lunes, mi�rcoles y viernes, una persona corre la misma ruta y
	//cronometra los tiempos obtenidos. Determinar el tiempo promedio que la persona tarda en
	//recorrer la ruta en una semana cualquiera.
	
	Definir tiempoLunes, tiempoMiercoles, tiempoViernes Como Real;
	Escribir "Ingrese el tiempo cronometrado el dia lunes";
	Leer tiempoLunes;
	
	Escribir "Ingrese el tiempo cronometrado el dia miercoles";
	Leer tiempoMiercoles;
	
	Escribir "Ingrese el tiempo cronometrado el dia viernes";
	Leer tiempoViernes
	
	Definir promedioTiempos Como Real;
	promedioTiempos <- trunc((tiempoLunes + tiempoMiercoles + tiempoViernes) /3);
	
	Escribir "El tiempo promedio es de: ", promedioTiempos, " min";
FinAlgoritmo
