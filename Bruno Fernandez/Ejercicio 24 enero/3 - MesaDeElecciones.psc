Funcion posicion <- BusquedaSecuencial(Arreglo, numMax, llave)
	posicion <- -1;
	Definir i Como Entero;
	i <- 1;
	Mientras posicion = -1 Y i <= numMax Hacer
		Si Arreglo[i] = llave Entonces
			posicion = i;
		Fin Si
		i <- i + 1;
	Fin Mientras
Fin Funcion

Funcion  InicioBaseDeDatos(Dni, nombres)
	Dni[1] = 11111111;
	nombres[1] ="Juan Martinez";
	
	Dni[2] = 22222222;
	nombres[2] ="Alberto Fernandez";
	
	Dni[3] = 33333333;
	nombres[3] ="Daniel Juarez";
Fin Funcion

Algoritmo MesaDeElecciones
	//El d�a de elecciones, en cada mesa se cuenta con un listado de las personas
	//que votar�n en dicha mesa. Los datos est�n guardados en dos vectores, en el primero la
	//c�dula y en el segundo el nombre. Se requiere un algoritmo que ingrese la c�dula de una
	//persona e informe si puede sufragar en ese puesto de votaci�n.
	
	Definir cedula Como Entero;
	Escribir "Ingrese la cedula de la persona";
	Leer cedula;
	
	Definir cantidad Como Entero;
	cantidad <- 5
	
	Dimension Dni[cantidad];
	Dimension nombres[cantidad];
	
	InicioBaseDeDatos(Dni, nombres);
	
	Definir resultado Como Entero;
	resultado <- BusquedaSecuencial(Dni, cantidad, cedula);
	
	Si resultado <> -1 Entonces
		Escribir "Usted ", nombres[resultado] " Vota en esta mesa";
	SiNo
		Escribir "La persona ingresada no vota en esta mesa";
	Fin Si

FinAlgoritmo
