Funcion posicion <- BusquedaSecuencial(arreglo, numMax, llave)
	posicion <- -1;
	Definir i Como Entero;
	i <- 1;
	Mientras posicion = -1 Y i <= numMax Hacer
		Si arreglo[i] = llave Entonces
			posicion = i;
		Fin Si
		i <- i + 1;
	Fin Mientras
Fin Funcion

Funcion InicioBaseDeDatos(numeroDeFactura, nombreCliente, fechaFactura, valorFactura)
	numeroDeFactura[1] = 00001;
	nombreCliente[1] = "Alberto Celis";
	fechaFactura[1] = "20/01/2022";
	valorFactura[1] = 4000;
	
	numeroDeFactura[2] = 00002;
	nombreCliente[2] = "Juan Carlos Cerrano";
	fechaFactura[2] = "22/01/2022";
	valorFactura[2] = 900;
	
	numeroDeFactura[3] = 00003;
	nombreCliente[3] = "Ricardo Sanchez";
	fechaFactura[3] = "23/01/2022";
	valorFactura[3] = 6500;
Fin Funcion

Algoritmo AlmacenTodoCaroFacturas
	//En el almac�n TodoCaro se cuenta con los datos: n�mero de factura, nombre
	//del cliente, fecha de facturaci�n y valor de la factura, almacenados en vectores. Se desea
	//un algoritmo que lea el n�mero de factura y muestre los dem�s datos.
	
	Definir numDeFactura Como Entero;
	Escribir "Ingrese el numero de la factura";
	Leer numDeFactura;
	
	Definir cantMaxClientes Como Entero;
	cantMaxClientes <- 3
	
	Dimension numeroDeFactura[cantMaxClientes];
	Dimension nombreCliente[cantMaxClientes];
	Dimension fechaFactura[cantMaxClientes];
	Dimension valorFactura[cantMaxClientes];
	
	InicioBaseDeDatos(numeroDeFactura, nombreCliente, fechaFactura, valorFactura)
	
	Definir resultado Como Entero;
	resultado <- BusquedaSecuencial(numeroDeFactura, cantMaxClientes, numDeFactura);
	
	Si resultado <> -1 Entonces
		Escribir "El numero de factura es: ", numeroDeFactura[resultado], " El nombre es: ", nombreCliente[resultado], " La fecha es: ", fechaFactura[resultado], " Y el valor es: $", valorFactura[resultado];
	SiNo
		Escribir "No se encuentra la factura";
	Fin Si

FinAlgoritmo
