Funcion posicion <- BusquedaSecuencial(arreglo, numMax, key)
	posicion <- -1;
	Definir i Como Entero;
	i <- 1;
	Mientras posicion = -1 Y i <= numMax Hacer
		Si arreglo[i] = key Entonces
			posicion = i;
		Fin Si
		i <- i + 1;
	Fin Mientras
Fin Funcion

Funcion  InicioBaseDeDatos(origen, destino, fechaLlamada, horaLlamada, duracionLlamada)
	origen[1] = 3493444444;;
	destino[1] = 3492555555;
	fechaLlamada[1] = "02/01/2022";
	horaLlamada[1] = "16:30";
	duracionLlamada[1] = "30 min";
	
	origen[2] = 3493656565;
	destino[2] = 3406212121;
	fechaLlamada[2] = "21/01/2022";
	horaLlamada[2] = "19:30";
	duracionLlamada[2] = "15 min";
Fin Funcion

Algoritmo InfoAtravesDeNumDeTelefono
	//La empresa de comunicaciones L�nea Segura registra el origen, el destino, la
	//fecha, la hora y la duraci�n de todas las llamadas que hacen sus usuarios, para esto hace
	//uso de cinco vectores. Se requiere un algoritmo para saber si desde un tel�fono
	//determinado se ha efectuado alguna llamada a un destino y en caso afirmativo conocer la
	//fecha, la hora y la duraci�n de la llamada.
	
	Definir numTelefono Como Entero;
	Escribir "Ingrese el numero del telefono";
	Leer numTelefono;
	
	Definir cantMaxLlamadas Como Entero;
	cantMaxLlamadas <- 5;
	
	Dimension telefonoOrigen[cantMaxLlamadas];
	Dimension telefonoDestino[cantMaxLlamadas];
	Dimension fechaLlamada[cantMaxLlamadas];
	Dimension horaLlamada[cantMaxLlamadas];
	Dimension duracionLlamada[cantMaxLlamadas];
	
	InicioBaseDeDatos( telefonoOrigen, telefonoDestino, fechaLlamada, horaLlamada, duracionLlamada)
	
	Definir resultado Como Entero;
	resultado <- BusquedaSecuencial(telefonoOrigen, cantMaxLlamadas, numTelefono);
	
	Si resultado <> -1 Entonces
		Escribir "El n�mero: ", telefonoOrigen[resultado], " Llam� al n�mero ", telefonoDestino[resultado], " En la fecha: ", fechaLlamada[resultado], " Y en la hora: ", horaLlamada[resultado], " Con una duraci�n de ", duracionLlamada[resultado];
	SiNo
		Escribir "No se han efectuado llamadas desde este telefono";
	Fin Si

FinAlgoritmo
