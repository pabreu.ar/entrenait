Funcion posicion <- BusquedaSecuencial(arreglo, maxCant, key )
	posicion <- -1;
	Definir i Como Entero
	i <- 1
	Mientras posicion = -1 Y i <= maxCant Hacer
		Si arreglo[i] = key Entonces
			pos = i;
		Fin Si
		i = i + 1;
	Fin Mientras
Fin Funcion

Funcion IncializarBaseDeDatos ( DNI, nombres, apellidos, carrera)
	DNI[1] = "11111111";
	nombres[1] = "Juan";
	apellidos[1] = "Salas";
	carrera[1] = "Programacion";
	
	DNI[2] = "22222222";
	nombres[2] = "Maria";
	apellidos[2] = "Gonzales";
	carrera[2] = "Contabilididad";
Fin Funcion

Algoritmo BusquedaDatosDeEstudiante
	Definir nombre Como Cadena;
	Escribir "Ingrese el nombre";
	Leer nombre;
	
	//Cantidad maxima de estudiantes
	Definir matricula Como Entero;
	matricula <- 10
	
	//Declaramos los arreglos
	Dimension DNI[matricula];
	Dimension nombres[matricula];
	Dimension apellidos[matricula];
	Dimension carrera[matricula];
	
	IncializarBaseDeDatos(DNI, nombres, apellidos, carrera);
	
	Definir indice Como Entero
	indice <- BusquedaSecuencial(nombres, matricula, nombre)
	Si indice <> -1 Entonces
		Escribir DNI[indice];
	SiNo
		Escribir "No se encuentra";
	Fin Si
FinAlgoritmo
