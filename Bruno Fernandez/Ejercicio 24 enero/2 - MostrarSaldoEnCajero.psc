Funcion posicion <- BusquedaSecuencial(arreglo, numMax, llave)
	posicion <- -1;
	Definir i Como Entero;
	i <- 1;
	Mientras posicion = -1 Y i <= numMax Hacer
		Si arreglo[i] = llave Entonces
			posicion = i;
		Fin Si
		i <- i + 1;
	Fin Mientras
Fin Funcion

Funcion InicioBaseDeDatos(saldo, NumeroDeCuenta, titular)
	NumeroDeCuenta[1] = 0001;
	titular[1] = "Juan Perez";
	saldo[1] = 25000;
	
	NumeroDeCuenta[2] = 0002;
	titular[2] = "Jos� Hernandez";
	saldo[2] = 15000;
	
	NumeroDeCuenta[3] = 0003;
	titular[3] = "Maria Casco";
	saldo[3] = 45000;
Fin Funcion

Algoritmo MostrarSaldoEnCajero
	//En un hipot�tico cajero electr�nico, cuando un cliente solicita el saldo de su
	//cuenta desliza la tarjeta, el lector toma el n�mero de cuenta y el sistema busca dicho
	//n�mero en la base de datos. Si lo encuentra reporta el saldo, si no muestra un mensaje
	//de error. Suponiendo que la base de datos consiste en tres vectores: n�mero de cuenta,
	//titular y saldo. Dise�ar un algoritmo para consultar el saldo de una cuenta.
	
	Definir numDeCuenta Como Entero;
	Escribir "Ingrese el numero de cuenta de su tarjeta";
	Leer numDeCuenta;
	
	Definir cantMax Como Entero;
	cantMax <- 5
	
	Dimension saldo[cantMax];
	Dimension NumeroDeCuenta[cantMax];
	Dimension titular[cantMax];
	
	InicioBaseDeDatos(saldo, NumeroDeCuenta, titular);
	
	Definir resultado Como Entero;
	resultado <- BusquedaSecuencial(NumeroDeCuenta, cantMax, numDeCuenta);
	
	Si resultado <> -1 Entonces
		Escribir "Su saldo es: $", saldo[resultado];
	SiNo
		Escribir "Esta persona no contiene saldo en su cuenta";
	Fin Si

FinAlgoritmo
