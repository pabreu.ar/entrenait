Algoritmo CalculoDeSuma
	//Hacer un algoritmo en Pseint para calcular la suma de los n primeros n�meros
	
	Definir num Como Entero;
	Escribir "Ingrese un numero a sumar con los anteriores";
	Leer num;
	
	Definir i, suma Como Entero;
	i <- 1;
	suma <- 0;
	
	Mientras num >= i Hacer
		suma = suma + i;
		i = i + 1
	Fin Mientras
	
	Escribir "La suma de los primeros numeros es ", suma;
FinAlgoritmo
