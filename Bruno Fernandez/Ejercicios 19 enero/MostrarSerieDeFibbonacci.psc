Algoritmo MostrarSerieDeFibbonacci
	//Hacer un algoritmo en Pseint para calcular la serie de Fibonacci
	
	Definir i, j, N Como Entero;
	i <- 0;
	j <- 1;
	
	Escribir "Indique el numero limite";
	Leer N;
	Definir contador, suma Como Entero;
	
	Para contador <- 1 Hasta N Con Paso 1 Hacer
		Escribir i
		suma <- i + j
		i <- j
		j <- suma
	Fin Para
FinAlgoritmo
