Algoritmo ImprimirTablasDeMultiplicar
	//Hacer un algoritmo en Pseint que imprima la tabla de multiplicar de los n�meros del uno al nueve.
	
	Definir a, b, resultado Como Entero;
	
	Para a <- 1 Hasta 9 Hacer
		Escribir "La tabla del ", a;
		Para b <- 1 Hasta 10 Hacer
			resultado <- a * b;
			Escribir a " * " b " es ", resultado;
		Fin Para
	Fin Para
	
FinAlgoritmo
