Algoritmo SumaNumerosPares
	//Hacer un algoritmo en Pseint para realizar la suma de todos los n�meros pares hasta el 1000.
	
	Definir i, suma Como Entero;
	
	Para i <- 2 Hasta 1000 Hacer
		Si  (i MOD 2 = 0) Entonces
			suma <- suma + i;
		Fin Si
		i <- i + 1;
	Fin Para
    Escribir "La suma es de ", suma;
FinAlgoritmo
