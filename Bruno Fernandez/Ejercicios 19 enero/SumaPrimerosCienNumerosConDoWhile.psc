Algoritmo SumaPrimerosCienNumerosConDoWhile
	//Hacer un algoritmo en Pseint para calcular la suma de los primeros cien n�meros con un ciclo Do-While.
	
	Definir i, suma Como Entero;
	i <- 0;
	Repetir
		i <- i + 1;
		suma <- suma + i;
	Hasta Que i >= 100
	
	Escribir " La suma es de ", suma;

FinAlgoritmo
