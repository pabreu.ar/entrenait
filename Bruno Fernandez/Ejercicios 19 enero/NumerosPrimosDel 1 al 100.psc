Algoritmo NumerosPrimosDel1Al100
	// Hacer un algoritmo que escriba todos los n�meros primos del 1 al 100.
	
		Definir num Como Entero
		Definir esPrimo Como Logico
		Definir divisor Como Entero
		
		Para num <- 2 Hasta 100 Hacer
			esPrimo <- Verdadero
			divisor <- 2
			Mientras num > divisor Y esPrimo Hacer
				esPrimo <- (num MOD divisor <> 0)
				divisor <- divisor + 1
			FinMientras
			
			Si esPrimo Entonces
				Escribir num;
			FinSi
		Fin Para
		
FinAlgoritmo
