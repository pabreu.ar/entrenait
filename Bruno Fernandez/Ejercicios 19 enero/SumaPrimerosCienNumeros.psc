Algoritmo SumaPrimerosCienNumerosWhile
	//Hacer un algoritmo en Pseint para calcular la suma de los primeros cien n�meros con un ciclo While.
	
	Definir i, suma Como Entero;
	i <- 0;
	Mientras i <= 100 Hacer
		i <- i + 1;
		suma <- suma + i;
	Fin Mientras
	
	Escribir " La suma es de ", suma;
FinAlgoritmo
