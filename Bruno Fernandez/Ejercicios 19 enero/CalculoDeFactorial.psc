Algoritmo CalculoDeFactorial
	//Hacer un algoritmo en Pseint que permita calcular el factorial de un n�mero.
	
	Definir num Como Entero;
	Leer num;
	
	Definir j, factorial Como Entero;
	j <- 1;
	factorial <- 1;
	Mientras num >= j Hacer
		factorial <- factorial * j;
			j<- j + 1;
	Fin Mientras
	
	Escribir "El factorial del numero ", num, " Es ", factorial;
FinAlgoritmo
