Algoritmo SumaPrimerosCienNumerosConFor
	//Hacer un algoritmo en Pseint para calcular la suma de los primeros cien n�meros con un ciclo For
	
	Definir i, suma Como Entero;

	Para i <- 1 Hasta 100 Hacer
		Si i <= 100 Entonces
			suma <- suma + i;
		Fin Si
	Fin Para
	
	Escribir " La suma es de ", suma;
FinAlgoritmo
