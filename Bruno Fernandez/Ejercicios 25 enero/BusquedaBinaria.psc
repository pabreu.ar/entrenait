Algoritmo BusquedaBinaria
	Definir puntero Como Entero;
	puntero <- 1
	Definir final Como Entero;
	final <- 5;
	
	Dimension vec[5];
	vec[1] = 3;
	vec[2] = 8;
	vec[3] = 11;
	vec[4] = 22;
	vec[5] = 13;
	
	Definir encontro Como Logico;
	encontro <- Falso
	
	Definir numero1 Como Entero;
	Escribir "Por favor ingrese el numero a buscar";
	Leer numero1;
	
	Definir mitad Como Entero;
	
	Mientras encontro = Falso Y puntero <= final Hacer
		mitad <- trunc ((puntero + final) / 2);
		Si expresion_logica Entonces
			acciones_por_verdadero
		SiNo
			acciones_por_falso
		Fin Si
	Fin Mientras
FinAlgoritmo
