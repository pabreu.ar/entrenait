Funcion OrdenarAscendentementePorBurbuja(arreglo Por Referencia, n Por Valor)
	Definir i, j Como Entero;
	
	Para i <- 1 Hasta n - 1 Con Paso 1 Hacer
		Para j <- i + 1 Hasta n Con Paso 1 Hacer
			Si arreglo[i] > arreglo[j] Entonces
				numMin <- arreglo[i];
				arreglo[i] <- arreglo[j];
				arreglo[j] <- numMin;
			Fin Si
		Fin Para
	Fin Para
Fin Funcion

Algoritmo OrdenamientoPorBurbuja
	Definir numMax como Entero;
	numMax <- 5;
	Dimension vector[numMax];
	vector[1] = 1;
	vector[2] = 4;
	vector[3] = 5;
	vector[4] = 2;
	vector[5] = 3;
	
	OrdenarAscendentementePorBurbuja(vector, numMax)
	
	Para i <- 1 Hasta numMax Con Paso 1 Hacer
		Escribir vector[i];
	Fin Para
	
FinAlgoritmo
