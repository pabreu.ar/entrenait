Algoritmo NumeroPrimo
	// Desarrole un algoritmo que permita leer un numero y determine si es primo o no
	Definir num Como Entero
	Escribir "Ingrese un numero mayor que 1"
	Leer num
	Definir esPrimo Como Logico
	esPrimo <- Verdadero
	Definir divisor Como Entero
	divisor <- 2
	Mientras num > divisor Y esPrimo Hacer
		esPrimo <- (num MOD divisor <> 0)
		divisor <- divisor + 1
	FinMientras
	Si esPrimo Entonces
		Escribir "El numero ingresado es primo"
	SiNo
		Escribir "El numero ingresado no es primo"
	FinSi
FinAlgoritmo
