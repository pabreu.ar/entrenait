Algoritmo MayorYMenorDeTresValores
	// Desarrolle un algoritmo que permita leer tres valores y almacenarlos en las varaibles A, B y C. El algoritmo debe imprimir cual es el mayor y cual es el menor.
	Definir A, B, C Como Numerico
	Escribir "Ingrese los tres valores"
	Leer A, B, C
	Definir variableMayor, variableMenor Como Numerico
	Si A <> B Y B <> C Y C <> A Entonces
		Si A > B Entonces
			variableMayor <- A
			variableMenos <- B
		SiNo
			variableMayor <- B
			variableMenor <- A
		FinSi
		Si variableMayor > C Entonces
			Si variableMenor > C Entonces
				variableMenor <- C
			FinSi
		SiNo
			variableMayor <- C
		FinSi
		Escribir variableMayor, "," variableMenor
	SiNo
		Escribir "Los numeros son iguales"
	FinSi
FinAlgoritmo
