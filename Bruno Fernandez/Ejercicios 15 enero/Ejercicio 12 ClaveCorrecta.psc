Algoritmo IngresarClaveCorrecta
	// Escribir un algoritmo que nos pida una clave (Eureka) y que nos de 3 intentos de acertar.
	Definir claveCorrecta Como Cadena
	claveCorrecta <- "Eureka"
	Definir cantIntentos Como Entero
	cantIntentos <- 1
	Definir claveIngresada Como Cadena
	Escribir "Ingrese una clave"
	Leer claveIngresada
	Mientras claveIngresada <> claveCorrecta Y cantIntentos < 3 Hacer
		Escribir "La Clave ingresada es incorrecta, intentelo de nuevo"
		Leer claveIngresada
		cantIntentos <- cantIntentos + 1
	FinMientras
	Si claveIngresada = claveCorrecta Entonces
		Escribir "La clave ingresada es correcta"
	SiNo
		Escribir "Ha fallado los 3 intentos, Intentelo m�s tarde"
	FinSi
FinAlgoritmo
