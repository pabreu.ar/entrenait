Algoritmo DescuentoEnTienda
	// Una tienda ofrece un descuento del 15% sobre el total de la compra durante el mes de octubre. Dado un mes y un importe, calcular cual es la cantidad que se debe cobrar al cliente.
	Definir Importe Como Real
	Definir Mes Como Entero
	Escribir "Ingrese el importe de la compra"
	Leer importe
	Escribir "Ingrese el numero del mes de la compra"
	Leer mes
	Si mes = 10 Entonces
		Definir descuento Como Real
		descuento <- (importe * 15) / 100
		Definir importeConDescuento Como Real
		importeConDescuento <- importe - descuento
		Escribir importeConDescuento
	SiNo
		Escribir "El mes no es octubre, no hay descuento"
		Escribir importe
	FinSi
FinAlgoritmo
