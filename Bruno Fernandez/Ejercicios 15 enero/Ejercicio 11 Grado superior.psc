Algoritmo CursarGradoSuperiorONo
	// Desarrolar un algoritmo que nos diga si una persona puede cursar un grado superior o no
	Definir tituloBachiller Como Logico
	tituloBachiller <- Verdadero
	Escribir "Ingrese diciendo Verdadero o Falso si usted posee un titulo de Bachiller"
	Leer tituloBachiller
	Definir pruebaDeAcceso Como Logico
	pruebaDeAcceso <- Verdadero
	Si tituloBachiller = Verdadero Entonces
		Escribir "Puede Acceder al Grado superior"
	SiNo
		Escribir "Ingrese diciendo Verdadero o Falso si ha pasado la prueba de acceso"
		Leer pruebaDeAcceso
		Si pruebaDeAcceso = Verdadero Entonces
			Escribir "Puede acceder al grado superior"
		SiNo
			Escribir "No puede acceder al grado superior"
		FinSi
	FinSi
FinAlgoritmo
