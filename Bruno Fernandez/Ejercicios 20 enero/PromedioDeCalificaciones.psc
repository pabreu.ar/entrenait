Algoritmo PromedioDeCalificaciones
	// Un estudiante realiza cuatro ex�menes, los cuales tienen la misma
	// ponderaci�n. Realizar el pseudoc�digo y el diagrama de flujo que representen el algoritmo
	// correspondiente para obtener el promedio de las calificaciones obtenidas
	
	Definir nota1,nota2,nota3,nota4 Como Real
	Escribir 'Ingrese las 4 notas de los examenes para comprobar su promedio'
	Leer nota1,nota2,nota3,nota4
	
	Definir promedio Como Real
	
	promedio <- (nota1+nota2+nota3+nota4)/4
	Si promedio>=6 Entonces
		Escribir 'est� aprobado'
	SiNo
		Escribir 'est� desaprobado'
	FinSi
	
	Escribir 'El promedio es de ',promedio
FinAlgoritmo
