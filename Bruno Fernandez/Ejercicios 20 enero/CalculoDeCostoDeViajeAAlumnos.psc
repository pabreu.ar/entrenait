Algoritmo CalculoDeCostoDeViajeAAlumnos
	// Realiza un algoritmo que permita determinar el pago a la agencia de alquiler de autobuses y 
	// lo que debe pagar cada alumno por el viaje.
	
	Definir cantAlumnos Como Entero
	Escribir 'Ingrese la cantidad de alumnos que van a viajar'
	Leer cantAlumnos
	
	Definir costoDelViaje Como Entero
	
	Si cantAlumnos>100 Entonces
		costoDelViaje <- 65
	FinSi
	Si cantAlumnos>=50 Y cantAlumnos<=100 Entonces
		costoDelViaje <- 70
	FinSi
	Si cantAlumnos>=30 Y cantAlumnos<=49 Entonces
		costoDelViaje <- 95
	FinSi
	Si cantAlumnos<30 Entonces
		costoDelViaje <- 4000
	FinSi
	
	Escribir 'El importe a cobrar es $',costoDelViaje
FinAlgoritmo
