Algoritmo InversionDeTresPersonas
	// Tres personas deciden invertir su dinero para fundar una empresa. Cada una de ellas 
	// invierte una cantidad distinta. Obtener el porcentaje que cada quien invierte
	// con respecto a la cantidad total invertida.
	
	Definir persona1,persona2,persona3 Como Caracter
	Definir inversion1,inversion2,inversion3 Como Real
	
	Escribir 'Ingrese el Nombre de la primer persona y el monto que invirti�'
	Leer persona1,inversion1
	
	Escribir 'Ingrese el Nombre de la segunda persona y el monto que invirti�'
	Leer persona2,inversion2
	
	Escribir 'Ingrese el Nombre de la tercer persona y el monto que invirti�'
	Leer persona3,inversion3
	
	Definir inversionTotal Como Real
	inversionTotal <- inversion1+inversion2+inversion3
	
	Definir porcentajeInv1,porcentajeInv2,porcentajeInv3 Como Real
	porcentajeInv1 <- redon((inversion1*100)/inversionTotal)
	porcentajeInv2 <- redon((inversion2*100)/inversionTotal)
	porcentajeInv3 <- redon((inversion3*100)/inversionTotal)
	
	Escribir persona1,' Invirti� un %',porcentajeInv1
	Escribir persona2,' Invirti� un %',porcentajeInv2
	Escribir persona3,' Invirti� un %',porcentajeInv3
FinAlgoritmo
