Algoritmo CalculoDeSalario
	// Calcular el nuevo salario de un empleado si obtuvo un incremento del 8%
	// sobre su salario actual y un descuento de 2,5%  por servicios.
	
	Definir salario Como Entero
	
	Escribir 'Ingrese su salario actual'
	Leer salario
	
	Definir incremento,descuentoPorServicios Como Entero
	incremento <- (salario*8)/100
	descuentoPorServicios <- (salario*2.5)/100
	
	Definir salarioFinal1,salarioFinal2 Como Entero
	
	salarioFinal1 <- salario+incremento
	salarioFinal2 <- salarioFinal1-descuentoPorServicios
	
	Escribir 'Su salario con el aumento es $',salarioFinal1
	Escribir 'Y con el descuento por servicios queda $',salarioFinal2
FinAlgoritmo
