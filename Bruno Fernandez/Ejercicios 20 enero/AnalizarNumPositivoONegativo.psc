Algoritmo AnalizarNumPositivoONegativo
	// Determinar el algoritmo para saber si un n�mero es positivo o negativo.
	
	Definir num Como Real
	Escribir 'Ingrese el numero a analizar'
	Leer num
	
	Si num>0 Entonces
		Escribir 'El numero es positivo'
	SiNo
		Escribir 'El numero es negativo'
	FinSi
	
FinAlgoritmo
