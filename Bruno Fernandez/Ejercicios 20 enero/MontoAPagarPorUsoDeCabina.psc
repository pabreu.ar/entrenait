Algoritmo MontoAPagarPorUsoDeCabina
	// Calcular el monto a pagar en una cabina de Internet si el costo por hora es de
	// S/.1,5 y por cada 5 horas te dan una hora de promoción gratis, sabiendo que la
	// permanencia en la cabina es de 12 horas.
	
	Definir cantDeHoras Como Entero
	Escribir 'Ingrese la cantidad de horas que va a utilizar la cabina'
	Leer cantDeHoras
	
	Definir costo,horaGratis Como Real
	horaGratis <- trunc(cantDeHoras/6)
	costo <- (cantDeHoras-horaGratis)*1.5
	
	Escribir 'El total a pagar es $',costo
FinAlgoritmo
