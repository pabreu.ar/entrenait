Algoritmo CalculoDeEdad
	// Mostrar la edad de una persona, ingresando el a�o de nacimiento y el a�o actual.
	
	Definir a�oNacimiento,a�oActual Como Entero
	
	Escribir 'Ingrese el a�o de nacimiento'
	Leer a�oNacimiento
	Escribir 'Ingrese el a�o actual'
	Leer a�oActual
	
	Definir edad Como Entero
	
	Si a�oActual<2023 Y a�oNacimiento>1900 Entonces
		edad <- a�oActual-a�oNacimiento
		Escribir 'En el a�o actual cumplirias o cumpliste ',edad,' a�os'
	FinSi
	
FinAlgoritmo
