Algoritmo CostoDeCantDeLapices
	// Realice un algoritmo para determinar cu�nto se debe pagar por una cantidad
	// de l�pices considerando que si son 1000 o m�s el costo es de 85 pesos; de lo contrario, el precio es de 90 pesos.
	
	Definir cantLapices Como Entero
	Escribir 'Ingrese la cantidad de lapices que compr�'
	Leer cantLapices
	
	Definir costo Como Entero
	Si cantLapices>1000 Entonces
		costo <- 85
	SiNo
		costo <- 90
	FinSi
	
	Escribir 'El costo es de $',costo
FinAlgoritmo
