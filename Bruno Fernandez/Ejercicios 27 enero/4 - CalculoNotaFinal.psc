Algoritmo CalculoNotaFinal
	//Escribir un algoritmo para calcular la nota final de un estudiante, considerando
    //que: por cada respuesta correcta 5 puntos, por una incorrecta -1 y por respuestas en blanco 0.
	//Imprime el resultado obtenido por el estudiante.
	
	Definir respuestasCorrectas, respuestasIncorrectas Como Entero;
	
	Escribir "Ingrese la cantidad de respuestas correctas que tuvo";
	Leer respuestasCorrectas;
	Escribir "Ingrese la cantidad de respuestas incorrectas que tuvo";
	Leer respuestasIncorrectas;
	
	Definir puntosCorrectas, puntosIncorrectas Como Entero;
	puntosCorrectas <- 5;
	puntosIncorrectas <- -1;
	
	Definir notaFinal Como Entero;
	notaFinal <- (respuestasCorrectas * puntosCorrectas) + (respuestasIncorrectas * puntosIncorrectas);
	
	Escribir "La nota final es: ", notaFinal;

FinAlgoritmo
