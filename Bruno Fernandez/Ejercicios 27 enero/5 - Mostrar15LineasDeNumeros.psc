Algoritmo Mostrar15LineasDeNumeros
	//Programa que muestra 15 l�neas como estas: 1 12 123 1234.
	
	Definir cantNum Como Entero;
	cantNum <- 15;
	
	Definir resultadoFinal Como Cadena;
	resultadoFinal <- "1";
	
	Para i <- 2 Hasta cantNum Hacer
		resultadoFinal <- resultadoFinal + ConvertirATexto(i)
		Escribir resultadoFinal;
	Fin Para
	
FinAlgoritmo
