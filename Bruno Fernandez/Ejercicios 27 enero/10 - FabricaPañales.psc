Algoritmo FabricaPa�ales
	//Juan Carlos es jefe de bodega en una f�brica de pa�ales desechables y
	//sabe que la producci�n diaria es de 744 pa�ales y que en cada caja donde se empacan
	//para la venta caben 12 pa�ales. �Cu�ntas cajas debe conseguir Juan Carlos para empacar
	//los pa�ales fabricados en una semana (5 d�as)?
	
	Definir cantPa�ales Como Entero;
	Escribir "Ingrese la cantidad de pa�ales producidos en el dia de hoy";
	Leer cantPa�ales;
	
	Definir cajas Como Entero;
	cajas <- 12;
	
	Definir cajasVentaDiaria, cajasVentaSemanal Como Real;
	cajasVentaDiaria <- trunc(cantPa�ales / cajas);
	cajasVentaSemanal <- trunc(cajasVentaDiaria * 5);
	
	Escribir "La cantidad de cajas que se necesitan para el empaque de hoy son: ", cajasVentaDiaria;
	Escribir "La cantidad de cajas que se necesitan para el empaque semanal son: ", cajasVentaSemanal;
FinAlgoritmo
