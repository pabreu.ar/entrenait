Algoritmo CalculoTiempoCiclista
	//Un ciclista parte de una ciudad A a las HH horas, MM minutos y SS
	//segundos. El tiempo de viaje hasta llegar a otra ciudad B es de T segundos. Escribir un
	//algoritmo que determine la hora de llegada a la ciudad B.
	
	Definir horas, minutos, seg Como Entero;
	Escribir "Ingrese las horas, minutos y segundos en las que parti� de la ciudad A";
	Leer horas, minutos, seg;
	
	Definir tiempoViaje Como Entero;
	Escribir "Ingrese cuantos segundos tard� en llegar a la ciudad B";
	Leer tiempoViaje;
	
	Definir horaLlegada, minLlegada, segLlegada, segLlegada2 Como Real;
	
	horaLlegada <- trunc(horas + (tiempoViaje / 3600));
	segLlegada <- trunc(seg + (tiempoViaje % 3600));
	minLlegada <- trunc(minutos + (segLlegada / 60));
	segLlegada2 <- trunc(segLlegada % 60);
	
	Escribir "Usted sali� a las: ", horas ":" , minutos ":" , seg, " Y lleg� a la otra ciudad a las: ", horaLlegada ":", minLlegada ":", segLlegada2;
	
FinAlgoritmo
