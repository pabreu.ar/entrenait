Funcion  OrdenarAscendenPorInsercion ( arreglo Por Referencia, n Por Valor )
	Definir i, aux, valorActual Como Entero;

	Para i <- 2 Hasta n Hacer
		valorActual <- arreglo[i];
		aux <- i - 1;
		Mientras aux > 0 Y arreglo[aux] > valorActual  Hacer
			arreglo[aux - 1] <- arreglo[aux];
			aux <- aux -1
		Fin Mientras
		arreglo[aux + 1] <- valorActual
	Fin Para
Fin Funcion

Algoritmo MenorDeCincoNumeros
	//Leer 5 n�meros e indicar finalmente cu�l fue el menor
	
	//Casos de prueba:
	//1: Ingresar: 1,3,10,6,2 y que devuelva 1
	//2: Ingresar: 20,25,5,-2,1 y que devuelva -2
	//3: Ingrsar: 9,5,13,0,22 y que devuelva 0
	
	Definir CantNums, i, numsIngresado Como Entero;
	CantNums <- 5
	
	Dimension vector[CantNums]
	
	Para i <- 1 Hasta CantNums Con Paso 1 Hacer
		Escribir "Ingrese los numeros";
		Leer numsIngresado;
		vector[i] = numsIngresado
	Fin Para
	
	OrdenarAscendenPorInsercion(vector, CantNums)
	
	Escribir "el menor numero es: ", vector[1];
FinAlgoritmo
