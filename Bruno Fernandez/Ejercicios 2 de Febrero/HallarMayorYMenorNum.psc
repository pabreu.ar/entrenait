Funcion resta <- restaMayorYMenor (arreglo Por Referencia, n Por Valor)
	resta <- arreglo[n] - arreglo[1] 
Fin Funcion

Funcion OrdenarPorInsercionAsc (arreglo Por Referencia, n Por Valor)
	Definir i, aux, numActual Como Entero;
	
	Para i <- 2 Hasta n Con Paso 1 Hacer
		numActual = arreglo[i]
		aux = i - 1;
		Mientras aux > 0 Y arreglo[aux] > numActual Hacer
			arreglo[aux + 1] <- arreglo[aux] 
			aux <- aux - 1;
		Fin Mientras
		arreglo[aux + 1] <- numActual
	Fin Para
Fin Funcion

Algoritmo HallarMayorYMenorNum
	// Algoritmo para Numero mayor - Numero menor
	
	//Casos de prueba:
	//1: Ingresar 1,5,9,3,0 y que devuelva 0 como menor y 9 como mayor
	//2: Ingresar 13,2,6,7,1 y que devuelva 1 como menor y 13 como mayor
	//3: Ingresar 0,1,-2,15,9 y que devuelva -2 como menor y 15 como mayor
	
	Definir cantNumeros, i, numerosIngresados Como Entero;
	cantNumeros <- 5;
	
	Dimension vector[cantNumeros];
	
	Para i <- 1 Hasta cantNumeros Hacer
		Escribir "Ingrese los numeros";
		Leer numerosIngresados;
		vector[i] <- numerosIngresados;
	Fin Para
	
	
	OrdenarPorInsercionAsc(vector, cantNumeros)
	Definir resta Como Entero;
	resta <- restaMayorYMenor(vector, cantNumeros)
//	Para i <- 1 Hasta cantNumeros Hacer
//		Escribir vector[i]
//	Fin Para
	
	Escribir "El menor numero es: ", vector[1]
	Escribir "El mayor numero es: ", vector[cantNumeros]
	Escribir "La resta de ambos es: ", resta
FinAlgoritmo
