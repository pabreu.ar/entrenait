Algoritmo AumentoDeSueldo
	//Hallar Aumento al Sueldo de un empleado; si el sueldo es mayor a $500.000
	//su aumento ser� del 12%, pero si su sueldo es menor El aumento ser� del 15%
	
	//Casos de prueba:
	//1: Ingresar $100.000 y que devuelva $115.000
	//2: Ingresar $600.000 y que devuelva $672.000
	
	Definir sueldoEmpleado Como Entero;
	Escribir "Ingrese el sueldo del empleado";
	Leer sueldoEmpleado
	
	Definir porcentSueldoMayor, porcentSueldoMenor Como Entero
	porcentSueldoMayor <- ((sueldoEmpleado * 12) / 100);
	porcentSueldoMenor <- ((sueldoEmpleado * 15) / 100);
	
	Definir sueldoCon12, sueldoCon15 Como Entero;
	sueldoCon12 <- sueldoEmpleado + porcentSueldoMayor;
	sueldoCon15 <- sueldoEmpleado + porcentSueldoMenor;
	
	Si sueldoEmpleado > 500000 Entonces
		Escribir "El sueldo con aumento seria de: ", sueldoCon12;
	SiNo
		Escribir "El sueldo con aumento seria de: ", sueldoCon15;
	Fin Si
FinAlgoritmo
