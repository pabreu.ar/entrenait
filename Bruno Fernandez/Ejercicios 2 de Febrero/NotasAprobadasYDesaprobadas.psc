Funcion aprobadosTotal <- notasAprobadas (arreglo Por Referencia, n Por Valor)
	Definir i Como Entero;
	aprobadosTotal <- 0;
	i <- 1
	Para i <- 1 Hasta n Hacer
		Si arreglo[i] >= 5 Y arreglo[i] <= 7 Entonces
			aprobadosTotal <- aprobadosTotal + 1;
		Fin Si
	Fin Para
	i = i + 1;
Fin Funcion

Algoritmo NotasAprobadasYDesaprobadas
	//Construya un algoritmo que permita ingresar 90 notas entre uno y siete,
	//indicando finalmente cu�ntos alumnos aprobaron y cu�ntos reprobaron.
	
	Definir cantNotas Como Entero;
	cantNotas <- 5
	
	Dimension calificaciones[cantNotas];
	Definir notas Como Entero

	Para i <- 1 Hasta cantNotas Hacer
		Escribir "Ingrese las notas";
		Leer notas;
		Si notas <= 7 Y notas > 0 Y i <= cantNotas Entonces
			calificaciones[i] = notas;
		SiNo
			Escribir "ingrese una nota entre 1 y 7";
		Fin Si
	Fin Para
	
	aprobadosTotal <- notasAprobadas(calificaciones, cantNotas)
	
	Definir desaprobadosTotal Como Entero;
	desaprobadosTotal <- cantNotas - aprobadosTotal;
	
	Escribir "La cantidad de alumnos aprobados es: ", aprobadosTotal;
	Escribir "La cantidad de alumnos desaprobados es: ", desaprobadosTotal

FinAlgoritmo
