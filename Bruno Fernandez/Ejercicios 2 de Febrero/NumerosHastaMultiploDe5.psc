Funcion multiplo1 = esMultiplo(n Por Valor)
	multiplo1 <- n MOD 5 = 0;
Fin Funcion

Algoritmo NumerosHastaMultiploDe5
//Leer una cantidad de n�meros variables hasta que se ingrese un n�mero
//m�ltiplo de 5. Indicar el n�mero de datos que fueron ingresados, sin contar el m�ltiplo de la
//condici�n de t�rmino.
	
	//Casos de prueba:
	//1- Ingresar un numero que sea multiplo de 5 y que diga que se ingres� "cero" numeros.
	//Ejemplo: 25
	//2- Ingresar 5 numeros y que el ultimo sea multiplo de 5 y que diga que se ingresaron 4 numeros.
	//Ejemplo: 1, 3, 4, 8, 15
	//3- Ingresar una cantidad aleatoria de numeros y que cuando se ingrese un multiplo de 5 corte y me diga la cantidad de numeros que fueron ingresados menos el multiplo.
	//Ejemplo: 1, 4, 6, 13, 17, 22, 23, 29, 30
	
	Definir cantNumeros, numsIngresados Como Entero;
	cantNumeros <- 0
	
	Repetir
		Escribir "ingrese los numeros";
		Leer numsIngresados;
		
		Si numsIngresados > 0 Y !esMultiplo(numsIngresados) Entonces
			cantNumeros <- cantNumeros + 1;
		Fin Si
	Hasta Que esMultiplo(numsIngresados) ;
	
	Escribir "La cantidad de numeros ingresados fue: ", cantNumeros
FinAlgoritmo
