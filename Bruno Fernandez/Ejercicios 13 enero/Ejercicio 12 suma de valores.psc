Algoritmo SumaDeValores
	//Ejercicio 12
	//Escribir un programa que indique si la suma de dos valores es positiva, negativa o cero.
	
	Definir Valor1, Valor2 Como Entero;
	Escribir "Ingrese los dos valores a sumar"
	Leer Valor1;
	Leer Valor2;
	
	Definir suma Como Entero;
	suma <- Valor1+Valor2;
	Escribir suma;
	
	Si suma > 0 Entonces
		Escribir "Es positiva";
	FinSi
	
		Si Suma = 0 Entonces
			Escribir "La suma da cero"
		FinSi
		
			Si Suma < 0 Entonces
			Escribir "La suma es negativa";
			Fin Si
FinAlgoritmo
