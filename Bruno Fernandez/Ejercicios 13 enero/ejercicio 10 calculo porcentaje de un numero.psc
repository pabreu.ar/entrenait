Algoritmo CalculoPorcentajeDeUnNumero
	//Ejercicio 10
	//Escribir un programa que calcula el porcentaje de una cantidad
	
	Definir num, porcentaje Como Entero;
	
	//Ingresamos el numero y el porcentaje a calcular
	Escribir "Ingrese el numero";
	Leer num;
	Escribir "Ingrese el porcentaje a calcular";
	Leer porcentaje;
	
	//Se calcula ese porcentaje
	Definir calculo Como Entero;
	calculo <- (num*porcentaje) / 100;
	
	//Se muestra el porcentaje
	Escribir "El porcentaje del numero es", "," calculo;
FinAlgoritmo
