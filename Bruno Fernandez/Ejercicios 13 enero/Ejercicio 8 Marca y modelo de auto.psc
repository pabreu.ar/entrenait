Algoritmo MostrarMarcaYModeloDeAuto
	//Ejercicio 8
	//Escribir un programa que lea de teclado la marca y modelo de un auto e imprima en pantalla el modelo y la marca (orden invertido a lo que se lee)
	
	Definir Marca, Modelo Como Cadena;
	
	Escribir "Ingrese la marca del auto";
	Leer Marca;
	
	Escribir "Ingrese el modelo del auto";
	Leer Modelo;
	
	Escribir "El modelo es", ":" Modelo, " " "Y la marca es",":" Marca;
FinAlgoritmo
