Algoritmo CalculoDeCuadrado
	//Ejercicio 5 13/01
	//Escribir un programa que calcule el cuadrado de 243.
	
	Definir num, calculo como numerico;
	//Ingresamos el numero de entrada
	num <- 243;
	
	//Lo mostramos
	Escribir "El numero es","," Num;
	
	//Calculamos el cuadrado de ese numero
	calculo <- Num^2;
	
	//Mostramos el cuadrado
	Escribir "Su cuadrado es","," calculo;
FinAlgoritmo
