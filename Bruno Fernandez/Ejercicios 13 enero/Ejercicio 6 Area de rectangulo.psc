Algoritmo AreaDeRectangulo
	//Ejercicio 6
	//Escribir un programa que calcule el per�metro y �rea de un rect�ngulo.
	
	Definir largo, ancho Como Numerico;
	Escribir "Ingrese el largo del rectangulo";
	Leer largo;
	
	Escribir "Ingrese el ancho del rectangulo";
	Leer ancho;
	
	Definir area, perimetro Como Numerico;
	area <- largo * ancho;
	Escribir "El area es", ":" area;
	
	perimetro <- (2 * largo) + (2 * ancho);
	Escribir "El perimetro es", ":" perimetro;
FinAlgoritmo
