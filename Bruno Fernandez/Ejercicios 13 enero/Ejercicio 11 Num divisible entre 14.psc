Algoritmo NumDivisibleEntre14
	//Ejercicio 11
	//Escribir un programa que indique si un n�mero es divisible entre 14
	
	Definir Num Como Entero;
	
	Escribir "Ingrese el numero a dividir";
	Leer Num;
	
	Definir resultado, resto Como Numerico;
	resultado <- Num / 14;
	Escribir "El resultado es", ":" resultado;
	
	resto <- Num MOD 14;
	Escribir "El resto es", ":" resto;
	
	Si resto = 0 Entonces
		Escribir "Es divisible entre 14";
	Fin Si
	
	Si resto > 0 Entonces
		Escribir "No es divisible entre 14"
	Fin Si
FinAlgoritmo
