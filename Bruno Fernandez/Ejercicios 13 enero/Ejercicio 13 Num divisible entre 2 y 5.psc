Algoritmo NumDivisibleEntre2Y5
	//Ejercicio 13
	// Escribir un programa que indique si un n�mero es divisible entre dos y cinco (a la vez).
	
	Definir Num Como Entero;
	
	Escribir "Ingrese el numero a dividir";
	Leer Num;
	
	Definir resultado1, resto1 Como Numerico;
	resultado1 <- Num / 2;
	Escribir "El resultado dividido 2 es", ":" resultado1;
	
	resto1 <- Num MOD 2;
	Escribir "El resto es", ":" resto1;
	
	Si resto1 = 0 Entonces
		Escribir "Es divisible entre 2";
	Fin Si
	
	Si resto1 > 0 Entonces
		Escribir "No es divisible entre 2"
	Fin Si
	
	Definir resultado2, resto2 Como Numerico;
	resultado2 <- Num / 5;
	Escribir "El resultado divido 5 es", ":" resultado2;
	
	resto2 <- Num MOD 5;
	Escribir "El resto es", ":" resto2;
	
	Si resto2 = 0 Entonces
		Escribir "Es divisible entre 5";
	Fin Si
	
	Si resto2 > 0 Entonces
		Escribir "No es divisible entre 5"
	Fin Si
FinAlgoritmo
