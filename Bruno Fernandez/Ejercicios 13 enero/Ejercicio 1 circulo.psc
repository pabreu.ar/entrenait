Algoritmo AreaDeCirculo
	//Ejercicio 1, 13/01
	//Escribir un programa que calcule al per�metro y �rea de un c�rculo dado su radio.
	
	Definir area, perimetro, radio Como numerico;
	
	Escribir "Ingrese el radio del circulo";
	Leer radio;
	area <- PI * radio ^ 2;
	Escribir "El area es", area;
	
	perimetro <- 2 * PI * radio;
	Escribir "El Perimetro es", perimetro;
FinAlgoritmo
