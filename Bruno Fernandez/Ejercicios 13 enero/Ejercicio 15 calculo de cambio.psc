Algoritmo calculoDeCambio
	//Ejercicio 15
	//Escribir un programa que calcule el cambio que debe darse a un cliente
	Definir PrecioTotal, DineroEntregado Como Real;
	
	Escribir "Ingrese el precio total";
	Leer PrecioTotal;
	Escribir "Ingrese la cantidad de dinero entregada por el cliente";
	Leer DineroEntregado;
	
	Definir Cambio Como Real;
	Cambio <- DineroEntregado - PrecioTotal;
	Escribir "EL cambio es de", ":" Cambio;
FinAlgoritmo
