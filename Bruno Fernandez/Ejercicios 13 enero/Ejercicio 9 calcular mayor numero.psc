Algoritmo CalcularMayorNumero
	//Ejercicio 9
	//Escribir un programa que indique cu�l es el mayor de cuatro n�meros enteros
	
	Definir num1, num2, num3, num4 Como Entero;
	
	Escribir "Ingrese los cuatro numeros";
	Leer num1;
	Leer num2;
	Leer num3;
	Leer num4;
	
	Definir resultado Como Entero;
	resultado <- num1;
	
	Si resultado < num2 Entonces
		resultado = num2;
	FinSi
	
	Si resultado < num3 Entonces
		resultado = num3;
	FinSi
	
	Si resultado < num4 Entonces
		resultado = num4;
	FinSi
	
	Escribir "El numero mayor es", ":" resultado;
FinAlgoritmo
