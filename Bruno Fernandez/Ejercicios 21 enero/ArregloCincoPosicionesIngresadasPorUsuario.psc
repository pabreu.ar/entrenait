Algoritmo ArregloCincoPosicionesIngresadasPorUsuario
	//Crear un arreglo de 5 posiciones y ll�nelo con los n�meros que el usuario desee.
	
	Dimension arreglo[5];
	
	Definir i, numsIngresados Como Entero;
	Escribir "Ingrese 5 numeros";
	
	Para i <- 1 Hasta 5 Con Paso 1 Hacer
		Leer numsIngresados;
		arreglo[i] <- numsIngresados
	Fin Para
	
	Para i <- 1 Hasta 5 Con Paso 1 Hacer
		Escribir "Los 5 numeros ingresados fueron: ", arreglo[i];
	Fin Para
	
FinAlgoritmo
