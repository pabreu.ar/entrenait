Algoritmo SumaDeDosArreglos
	//Sumar los elementos de dos arreglos y guardar el resultado en otro arreglo de 1 posici�n
	Definir maxValor Como Entero;
	Escribir "Ingrese de que tama�o desea que sea el arreglo";
	Leer maxValor;
	
	Dimension arreglo1[maxValor];
	Para i <- 1 Hasta maxValor Con Paso 1 Hacer
		Escribir "Ingrese el primer arreglo";
		Leer arreglo1[i]
	Fin Para
	
	Dimension arreglo2[maxValor];
	Para i <- 1 Hasta maxValor Con Paso 1 Hacer
		Escribir "Ingrese el segundo arreglo";
		Leer arreglo2[i]
	Fin Para
	
	Dimension arreglo3[1];
	Para i <- 1 Hasta maxValor Hacer
		suma1 <-  suma1 + arreglo1[i];
		suma2 <- suma2 + arreglo2[i];
		resultado <- suma1 + suma2;
	Fin Para
	
	Para i <- 1 Hasta 1 Hacer
		Escribir "La suma de los dos arreglos es: ", resultado;
	Fin Para
FinAlgoritmo
