Algoritmo ArregloFactorialNumero
	//Empleando un arreglo que almacena el factorial de un n�mero, para luego mostrarlo.
	
	Definir num Como Entero;
	Escribir "Ingrese el numero";
	Leer num;
	
	factorial <- 1;
	Dimension arreglo[factorial]
	
	Definir i Como Entero;
	i <- 1;
	
	Mientras num >= i Hacer
		factorial <- factorial * i;
		i<- i + 1;
	Fin Mientras
	
	Para i <- 1 Hasta factorial Hacer
		arreglo[i] <- factorial
		Escribir "El factorial de ", num " es: ", arreglo[i]
	Fin Para
FinAlgoritmo
