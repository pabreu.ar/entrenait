Algoritmo IngredientesYPreciosParaUnPlato
	//Dise�e un arreglo en el que se ingrese la cantidad de productos y sus
	//respectivos precios, para la preparaci�n de un plato, tambi�n se debe mostrar al final el costo a gastar.
	
	Definir plato Como Cadena;
	Escribir "Ingrese el nombre del plato";
	Leer plato;
	
	Definir cantidad Como Entero;
	Escribir "Ingrese la cantidad de ingredientes a utilizar";
	Leer cantidad
	
	Dimension ingredientes[cantidad];
	Dimension Precio[cantidad];
	
	Definir CostoTotal Como Real;
	CostoTotal <- 0;
	
	Para i <- 1 Hasta cantidad Hacer
		Escribir "Ingrese el ingrediente";
		Leer ingredientes[cantidad];
		Escribir "Ingrese su precio";
		Leer Precio[i];
		CostoTotal <- CostoTotal + Precio[i];
	Fin Para
	
	Escribir "El costo total ser� de: $" CostoTotal;
FinAlgoritmo
