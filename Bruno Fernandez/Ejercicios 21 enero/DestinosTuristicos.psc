Algoritmo DestinosTuristicos
	//Escribir 3 destinos tur�sticos, la distancia desde Sunchales, el costo del litro
	//as� como la distancia . Se debe mostrar el gasto que demanda en viajar a cualquiera de
	//dichos lugares tur�sticos.
	
	//Teniendo en cuenta que con 1 litro se hace un Km:
	
	Dimension Destinos[3];
	Definir precioLitro, distancia, costoDestino Como Real;
	Para i <- 1 Hasta 3 Hacer
		Escribir "Ingrese el nombre del destino al que desea ir";
		Leer Destinos[i];
		Escribir "Ingrese la distancia que hay hasta llegar al destino";
		Leer distancia;
		Escribir "Ingrese el precio del litro de combustible";
		Leer precioLitro;
		costoDestino <- distancia * precioLitro;
		Escribir "El costo del viaje es de $", costoDestino;
	Fin Para
	
FinAlgoritmo
