Funcion elevarCuadrado( arreglo Por Referencia, maxValor Por Valor )
	Definir i Como Entero;
	Para i <- 1 Hasta maxValor Con Paso 1 Hacer
		arreglo[i] <- (arreglo[i]) ^2;
		
	Fin Para
Fin Funcion

//Crear un arreglo con n n�meros, ingresados por teclado y mostrar sus valores elevados al cuadrado

Algoritmo NumerosAlCuadrado
	Definir maxValor Como Entero;
	Escribir "Ingrese de que tama�o desea que sea el arreglo";
	Leer maxValor;
	
	Dimension arreglo[maxValor];
	
	Para i <- 1 Hasta maxValor Con Paso 1 Hacer
		Escribir "Ingrese los numeros";
		Leer arreglo[i]
		Escribir "El numero ingresado fue: ", arreglo[i] ;
	Fin Para
	
	elevarCuadrado(arreglo, maxValor)
	
	Para i <- 1 Hasta maxValor Con Paso 1 Hacer
		Escribir "El cuadrado del numero es ", arreglo[i];
	Fin Para
	
	
FinAlgoritmo
