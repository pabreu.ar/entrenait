Proceso SerieDeFibonacci
	//Ejercicio 7: Hacer un algoritmo en Pseint para calcular la serie de Fibonacci.
	
	Definir primerNum, segundoNum, ProxNum, n Como Entero;
	primerNum <- 0;
	segundoNum <- 1;
	Escribir "Ingrese hasta que posicion quiere ver el la serie de fibonacci ";
	Leer n;
	Para  i<- 1 Hasta n Hacer
		Escribir primerNum;
		ProxNum <- primerNum + segundoNum;
		ProxNum <- segundoNum;
		segundoNum <- ProxNum;
	FinPara

FinProceso
