Proceso NumeroPrimosDel1Al100
	//Ejercicio 8: Hacer un algoritmo que escriba todos los n�meros primos del 1 al 100.
	
	Definir i Como Entero;
	Definir esPrimo Como Logico;
	Definir divisor Como Numero;
	
	Para i <- 2 Hasta 100 Hacer
		esPrimo <- Verdadero;
		divisor <- 2;
		Mientras esPrimo y divisor < i Hacer
			esPrimo <- i % divisor <> 0;
			divisor <- divisor + 1;
		FinMientras
		si esPrimo Entonces
			Escribir i, " - ";
		FinSi
	FinPara
FinProceso