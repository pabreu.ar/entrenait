//Metodo de busqueda secuencial generico
funcion pos <- BusquedaSecuencial(arreglo, n, key)
	pos <- -1;
	Definir i Como Entero;
	i <- 1;
	Mientras pos = -1 y i < n  Hacer
		si  arreglo[i] = key Entonces
			pos <- i;
		FinSi
		i <- i +1;
	FinMientras
FinFuncion

//En casos futros agregar un resultado logico que nos permita saber si 
//se inicializo exitosamente 
Funcion InicializarBaseDeDatos(documentos, nombres, apellidos, carrera)
	documentos[1] <- "11111111";
	nombres[1] <- "Juan";
	apellidos[1] <- "Salas";
	carrera[1] <- "Programacion";
	
	documentos[1] <- "22222222";
	nombres[1] <- "Maria";
	apellidos[1] <- "Gonzales";
	carrera[1] <- "Contabilidad";
	
	documentos[1] <- "33333333";
	nombres[1] <- "Alejandro";
	apellidos[1] <- "Hernandez";
	carrera[1] <- "RRHH";
	
	documentos[1] <- "44444444";
	nombres[1] <- "Pablo";
	apellidos[1] <- "Abreu";
	carrera[1] <- "Dise�o Grafico";
FinFuncion



Proceso BusquedaSecuencialEnUnaUnuversidad
	Definir nombre Como Cadena;
	Escribir "Ingrese el nombre del alumno que busca";
	Leer nombre;
	
	//cantidad maxima de estudiates que pueden matricular en la universidad 
	Definir matricula Como Entero;
	matricula <- 1000;
	
	//declaramos los arreglos "matrix" o base de datos de la escuela
	Dimension documentos[matricula];
	Dimension nombres[matricula];
	Dimension apellidos[matricula];
	Dimension carrera[matricula];
	
	//inicializar valores
	InicializarBaseDeDatos(documentos, nombres, apellidos, carrera);
	
	//Buscar por nombre
	Definir indice Como Entero;
	indice <- BusquedaSecuencial(nombres, matricula, nombre);
	si indice <> -1 Entonces
		Escribir "Su documento es: " , documentos[incice];
	SiNo
		Escribir "Ese nombre no se encuentra en la base de datos de la universidad";
	FinSi
	
	
FinProceso
