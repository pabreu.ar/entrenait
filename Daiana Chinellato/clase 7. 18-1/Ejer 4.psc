Proceso MontoADevolver 
	//Ejercicio 4: Calcula el Monto a devolver si nos prestan un capital c, a una tasa de inter�s
	//t% durante n periodos.
	
	Definir prestamo, interes Como Real;
	Escribir "Ingrese el capital prestado";
	Leer prestamo;
	Escribir "	Ingrese el interes";
	leer interes;
	
	Definir tiempo Como Entero;
	Escribir "Ingrese el tiempo en cantidad de meses";
	Leer tiempo;
	
	Definir montoInetres, montoTotal Como Real;
	montoInetres <- (prestamo * (interes / 100) * tiempo);
	montoTotal <- montoInetres + prestamo;
	
	Escribir "El monto a devolver es igual a = ", montoTotal;	
FinProceso
