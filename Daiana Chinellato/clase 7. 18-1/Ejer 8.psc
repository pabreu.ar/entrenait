Proceso MasaDeAireDeUnVehiculo
//Ejercicio 8: La presi�n, el vol�men y la temperatura de una masa de aire se relacionan por
//la f�rmula: masa = (presi�n * vol�men)/(0.37 * (temperatura + 460)). Calcular la masa de
//aire de un neum�tico de un veh�culo que est� en compostura en un servicio de alineaci�n y balanceo.

	Definir presion, volumen, temperatura como Real;
	
	Escribir "Ingrese la presi�n";
	Leer presion;
	Escribir "Ingrese el volumen";
	Leer volumen;
	Escribir "Ingrese la temperatura";
	Leer temperatura;
	
	Definir masa Como Real;
	masa <- (presion * volumen)/(0.37 * (temperatura + 460));
	
	Escribir  "La masa de aire del neum�tico del vehiculo es de: ", masa;	
FinProceso
