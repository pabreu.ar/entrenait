Proceso InvercionBamcoRazon2PorcioentoMensual
	//Ejercicio 6: Suponga que un individuo desea invertir su capital en un banco y desea
	//saber cu�nto dinero ganar� despu�s de un mes si el banco paga a raz�n de 2% mensual
	
	Definir inversion Como Real;
	Escribir "Ingrese el monto que desee invertir";
	Leer inversion;
	
	Definir mesesInversion Como Entero;
	Escribir "Ingrese la cantidad de meses para conocer el retorno de su inversi�n";
	Leer mesesInversion;
	
	Definir porcentajeMensual, retornoInversion, totalRetorno Como Real;
	porcentajeMensual <- 0.02 * mesesInversion;
	retornoInversion <- inversion * porcentajeMensual;
	totalRetorno <- inversion + retornoInversion;
	
	Escribir  "El dinero generado en ", mesesInversion, " meses es de $", retornoInversion, " dando un total de $", totalRetorno;
FinProceso
