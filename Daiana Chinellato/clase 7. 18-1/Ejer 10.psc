Proceso MontoPresupuestalDeUnHospital
//Ejercicio 10: En un hospital existen tres �reas: Ginecolog�a, Pediatr�a, Traumatolog�a. El
//presupuesto anual del hospital se reparte conforme a la sig. tabla:
//�rea: % del presupuesto:
//Ginecolog�a 40%
//Traumatolog�a 30%
//Pediatr�a 30%
//Obtener la cantidad de dinero que recibir� cada �rea, para cualquier monto presupuestal.
	
	Definir presupuestoTotal Como Entero;
	Escribir "Ingrese el pregupuesto para este mes";
	Leer presupuestoTotal;
	
	Definir ginecologia, traumatologia, pediatria Como Real;
	ginecologia <- presupuestoTotal * 0.40;
	traumatologia <- presupuestoTotal * 0.30;
	pediatria <- presupuestoTotal * 0.30;
	
	Escribir  "El dinero del �rea de Ginecolog�a es de: $", ginecologia;
	Escribir  "El dinero del �rea de Traumatolog�a es de: $", traumatologia;
	Escribir  "El dinero del �rea de Pediatr�a es de: $", pediatria;
FinProceso
