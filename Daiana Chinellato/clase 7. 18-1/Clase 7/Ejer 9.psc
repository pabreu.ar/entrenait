Proceso PulsacionesDeUnaPersona
//Ejercicio 9: Calcular el n�mero de pulsaciones que una persona debe tener por cada 10
//segundos de ejercicio, si la f�rmula es: num. pulsaciones = (220 - edad)/10
	
	Definir edad Como Entero;
	Escribir  "Ingrese la edad para poder calcular las pulsaciones";
	Leer edad;
	
	Definir numPulsaciones Como Real;
	numPulsaciones <- (220 - edad) /10;
	
	Escribir "El numero de pulsaciones de una personapor cada 10 segundos de ejercicio es de: ", numPulsaciones;
FinProceso
