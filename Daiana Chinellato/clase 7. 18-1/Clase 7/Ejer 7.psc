Proceso CalificacionFinalDeLaMateria
	//Ejercicio 7: Un alumno desea saber cu�l ser� su calificaci�n final en la materia de
//Algoritmos. Dicha calificaci�n se compone de los siguientes porcentajes:
	//55% del promedio de sus tres calificaciones parciales.
	//30% de la calificaci�n del ex�men final.
	//15% de la calificaci�n de un trabajo final.
	
	Definir promedioParciales, calFinal, calTrabajoF Como Entero;
	promedioParciales <- 55;
	calExamenFinal <- 30;
	calTrabajoF <- 15;
	
	Definir calificacionTotal Como Real;
	calificacionTotal <- (promedioParciales + calExamenFinal + calTrabajoF) / 3;
	
	Escribir "La calificaci�n final es de ", calificacionTotal;
FinProceso
