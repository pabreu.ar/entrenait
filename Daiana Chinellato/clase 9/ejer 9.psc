Proceso DivisionDeUnTerreno
	//Ejercicio 9: Un empresario decide heredar su terreno a sus 3 hijos, tomando como
	//referencia la cantidad de hijos, al mayor que tiene 6 hijos le otorga el doble del menor, al
	//que tiene 2 hijos le corresponde 125.2 mt2, al menor le otorga el 27% del total del terreno.
	//Calcular el total del terreno y la cantidad que le corresponde a cada hijo.
	
	Definir porcentajeH1, porcentajeH2, porcentajeH3 Como Entero;
	porcentajeH3 <- 27;
	porcentajeH1 <- porcentajeH3 * 2;
	porcentajeH2 <- 100 - (porcentajeH1 + porcentajeH3);
	
	Definir porcionH1, porcionH2, porcionH3, totalTerreno Como Real;
	porcionH3 <- 125.2;
	porcionH1 <- porcionH3 * 2;
	totalTerreno <- (porcionH3 * 100) / 27;
	porcionH2 <- totalTerreno - (porcionH1 + porcionH3) ;
	
	Escribir "Al hijo 1 le correponde el ", porcentajeH1, " %, lo que equivale a ", porcionH1 ," mt2 de terreno";
	Escribir "Al hijo 2 le correponde el ", porcentajeH2, " %, lo que equivale a ", porcionH2 ," mt2 de terreno";
	Escribir "Al hijo 3 le correponde el ", porcentajeH3, " %, lo que equivale a ", porcionH3 ," mt2 de terreno";
FinProceso
