Proceso MontoAPagarDeUnaCabinaDeInternet
	//Ejercicio 6: Calcular el monto a pagar en una cabina de Internet si el costo por hora es de
	//S/.1,5 y por cada 5 horas te dan una hora de promoción gratis, sabiendo que la
	//permanencia en la cabina fue de 12 horas.
	
	Definir horas Como Entero;
	Escribir "Ingrese la cantidad de horas que va a estar en la cabina";
	Leer horas;
	
	Definir paga Como Real;
	Definir horasGratis Como Entero;
	horasGratis <- Trunc( horas / 6);
	paga <- (horas - horasGratis) * 1.5;
	
	Escribir  "El monto a pagar es de: ", paga;
FinProceso
