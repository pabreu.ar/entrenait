Proceso NuevoSalarioDeEmpleado
	//Ejercicio 8: Calcular el nuevo salario de un empleado si obtuvo un incremento del 8%
	//sobre su salario actual y un descuento de 2,5% por servicios
	
	Definir salarioViejo Como Entero;
	Escribir "Ingrese el salario actual";
	Leer salarioViejo;
	
	Definir aumento, salarioNuevo, descuentoServicios, salarioTotal Como Real;
	aumento <- salarioViejo * 0.08;
	salarioNuevo <- salarioViejo + aumento;
	descuentoServicios <- salarioNuevo * 0.025;
	salarioTotal <- salarioNuevo - descuentoServicios;
	
	Escribir "El nuevo salario es de: $", salarioTotal;
	
FinProceso
