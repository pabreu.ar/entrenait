Proceso AreaDeUnTrapecio
	//Ejercicio 7: Calcular el �rea de un trapecio
	
	Definir base1, base2, altura Como Real;
	Escribir "Ingrese la base m�s grande";
	Leer base1;
	Escribir "Ingrese la siguiente base";
	Leer base2;
	Escribir "Ingrese la altura";
	Leer altura;
	
	Definir area Como Real;
	area <- ((base1 + base2) * altura) / 2;
	
	Escribir "El �rea del trapecio es de: ", area, " cm2";
FinProceso
