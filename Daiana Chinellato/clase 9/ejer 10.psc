Proceso EdadDeUnaPersona
	//Ejercicio 10: Mostrar la edad de una persona, ingresando el a�o de nacimiento y el a�o actual.
	
	Definir anioNacimiento, a�oActual Como Entero;
	Escribir "Ingrese el a�o de nacimiento";
	Leer anioNacimiento;
	Escribir "Ingrese el a�o actual";
	Leer anioActual;
	
	Definir edad Como Entero;
	edad <- anioActual - a�oNacimiento
	Si edad <= 120
		Escribir  "La edad de la persona es: ", edad
	SiNo
		Escribir "La edad ingresada es mayor a los 120 a�os, ingrese una edad v�lida"
	FinSi
	
FinProceso
