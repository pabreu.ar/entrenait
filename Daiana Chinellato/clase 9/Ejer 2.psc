Proceso AreaDeUnTerreno
	//Ejercicio 2: Una empresa constructora vende terrenos con la forma A de la figura. Realizar
	//un algoritmo y repres�ntelo mediante un diagrama de flujo y el pseudoc�digo para obtener
	//el �rea respectiva de un terreno de medidas de cualquier valor
	
	Definir ladoA, ladoB, ladoC Como Real;
	Escribir  "Ingrese la logitud del lado A";
	Leer ladoA;
	Escribir  "Ingrese la logitud del lado B";
	Leer ladoB;
	Escribir  "Ingrese la logitud del lado C";
	Leer ladoC;
	
	Definir ladoD, areaRect, areaTria Como Real;
	ladoD <- ladoA - ladoC;
	areaRect <- ladoB * ladoC;
	areaTria <- (ladoB * ladoD) / 2;
	
	Definir areaTotal Como Real;
	areaTotal <- areaRect + areaTria;
	
	Escribir  "El �rea total del terreno es de: ", areaTotal;
FinProceso
