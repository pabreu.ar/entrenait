Algoritmo CalcularPrecioTotalDeProductosDePlato
	//Dise�e un arreglo en el que se ingrese la cantidad de productos y sus
	//respectivos precios, para la preparaci�n de un plato, tambi�n se debe mostrar al final el costo a gastar.
	
	Definir plato Como Cadena;
	Escribir "Ingrese el nombre del plato: ";
	Leer plato;
	Definir cantidad Como Entero;
	Escribir "Ingrese la cantidad de productos: ";
	Leer cantidad;
	
	//Definir dos arreglos
	Dimension productos[cantidad];
	Dimension precios[cantidad];
	
	Definir i Como Entero;
	Para i <- 1 Hasta cantidad Con Paso 1 Hacer
		Escribir "Ingrese el producto ", i, ":";
		Leer productos[i];
		Escribir "Ingrese el precio del ", i " producto:";
		Leer precios[i];
	FinPara
	
	Definir costo Como Real;
	costo <- 0
	Para i <- 1 Hasta cantidad Con Paso 1 Hacer
		costo <- costo + precios[i];
	FinPara
	
	Escribir "El costo total del plato: ", plato ", es: $", costo;
FinAlgoritmo
