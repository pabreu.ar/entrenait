Algoritmo MostrarMayorYSegundoMayorValor
	//Ingresar n�meros, almacenarlos en un arreglo y mostrar el n�mero o elemento
	//mayor, as� como el segundo mayor.
	
	Definir n, i Como Entero;
	Escribir "Ingrese la cantidad de elementos:";
	Leer n;
	Dimension valores[n];
	Para i<-1 Hasta n Hacer
		Escribir "Ingrese el " i " numero";
		Leer valores[i];
	FinPara
		
	Definir max1, max2 Como Entero
	Si valores[1] > valores[2] Entonces
		max1 <- valores[1];
		max2 <- valores[2];
	Fin si
		
	Si valores[2] > valores[1]
		max1 <- valores[2];
		max2 <- valores[1];
	FinSi
		
	Para i <- 3 Hasta n Hacer
		Si valores[i] > max1 Entonces
			max2 <- max1;
			max1<- valores[i];
		Sino
			Si valores[i] > max2 Entonces
				max2 <- valores[i];
			FinSi
		FinSi
	FinPara
		
	Escribir "El mayor n�mero es: ",max1;
	Escribir "El segundo mayor numero es: ",max2
FinAlgoritmo
