Algoritmo AlmacenarYMostrarFactorialEnArreglo
	//Empleando un arreglo que almacena el factorial de un n�mero, para luego mostrarlo.
	
	Definir num Como Entero;
	Escribir "Ingrese el n�mero del cual desea calcular el factorial: ";
	Leer num;
	
	Dimension factorial[1];
	factorial[1] <- 1;
	
	Definir i Como Entero;
	Para i <- 1 Hasta num Con Paso 1 Hacer
		factorial[1] <- factorial[1] * i;
	FinPara
	
	Escribir "El factorial de ", num " es: ", factorial[1];
FinAlgoritmo
