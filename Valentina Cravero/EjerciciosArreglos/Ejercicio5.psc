Funcion OrdenamientoBurbujaAsc(arreglo Por Referencia, n Por valor)
	Definir i, j Como Entero;
	Para i <- 1 Hasta n - 1 Con Paso 1 Hacer
		Para j <- i + 1 Hasta n Con Paso 1 Hacer
			Si arreglo[i] > arreglo[j] Entonces
				auxChange <- arreglo[i]; 
				arreglo[i] <- arreglo[j]; 
				arreglo[j] <- auxChange;
			FinSi
		FinPara
	FinPara
FinFuncion

Algoritmo MostrarAscendentePartesComputadora
	//Ingresar por teclado el nombre de las partes de una computadora hasta ingresar un dato vaci�
	//y mostrarlos en forma ordenada de manera ascendente.
	
	Definir parteComputadora, datoVacio Como Cadena;
	datoVacio <- "";
	
	Definir cantidad Como Entero;
	Escribir "Indique la cantidad de partes a ingresar: ";
	Leer cantidadPartes
	
	Dimension componentes[cantidadPartes];
	
	Definir i Como Entero;
	i <- 1;
	
	Repetir 
		Escribir "Ingrese una parte de la computadora: ";
		Leer parteComputadora;
		componentes[i] <- parteComputadora;
		i <- i + 1;
	Hasta que parteComputadora = datoVacio o i > cantidadPartes
	
	OrdenamientoBurbujaAsc(componentes,cantidadPartes)
	
	Escribir "Partes de la computadora ordenadas de manera ascendente: ";
	Para i <- 1 Hasta cantidadPartes Con Paso 1 Hacer
		Escribir componentes[i]
	FinPara
FinAlgoritmo
