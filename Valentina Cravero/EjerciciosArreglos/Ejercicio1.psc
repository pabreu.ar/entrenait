Algoritmo IngresarAlmacenarEnArregloYMostrarNumeros
	//Ingresar 5 n�meros, almacenarlos en un arreglo y mostrarlos.
	
	Definir size, i Como Entero;
	size <- 5;
	Dimension array[size];
	
	//Ingresar n�meros y almacenarlos en arreglo
	Para i <- 1 Hasta size Con Paso 1 Hacer
		Escribir "Ingrese el n�mero ", i, " :";
		Leer array[i];
	FinPara
	
	//Imprimir numeros por pantalla
	Para i <- 1 Hasta size Con Paso 1 Hacer
		Escribir "El n�mero ", i, " es: " array[i]
	FinPara
FinAlgoritmo
