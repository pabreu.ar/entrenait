Algoritmo SumarDosArreglosAlmacenarResultadoEnTercerArreglo
	//Sumar los elementos de dos arreglos y guardar el resultado en otro arreglo de 1 posici�n.
	
	Definir tamanio1 Como Entero;
	Escribir "Ingrese el tama�o del arreglo 1: ";
	Leer tamanio1;
	
	Dimension array1[tamanio1];
	
	Definir i Como Entero;
	Para i <- 1 Hasta tamanio1 Con Paso 1 Hacer
		Escribir "Ingrese el n�mero ", i, ":";
		Leer array1[i]; 
	FinPara
	
	Definir acumulador1 Como Entero;
	acumulador1 <- 0;
	Para i <- 1 Hasta tamanio1 Con Paso 1 Hacer
		acumulador1 <- acumulador1 + array1[i];
	FinPara
	
	Definir tamanio2 Como Entero;
	Escribir "Ingrese el tama�o del arreglo 2: ";
	Leer tamanio2;
	
	Dimension array2[tamanio2];
	
	Para i <- 1 Hasta tamanio2 Con Paso 1 Hacer
		Escribir "Ingrese el n�mero ", i, ":";
		Leer array2[i]; 
	FinPara
	
	Definir acumulador2 Como Entero;
	acumulador2 <- 0;
	Para i <- 1 Hasta tamanio2 Con Paso 1 Hacer
		acumulador2 <- acumulador2 + array2[i];
	FinPara
	
	Dimension array3[1];
	array3[1] <- acumulador1 + acumulador2;
	
	Escribir "La suma total de los " tamanio1, " elementos del arrelo 1 y los ", tamanio2, " elementos del arreglo 2 es: ", array3[1];
FinAlgoritmo
