Algoritmo CuadradosNumerosDelArray
		//Crear un arreglo con n n�meros, ingresados por teclado y mostrar sus valores elevados al cuadrado.
		
		Escribir "Ingresar la cantidad de n�meros que desea que contega el arreglo: ";
		Leer n;
		
		Dimension valores[n];
		Dimension cuadrados[n];
		Definir i Como Entero;
		Para i<-1 Hasta n Hacer
			Escribir "Ingrese el " i " numero: ";
			Leer valores[i];
			cuadrados[i]<-valores[i]^2;
			Escribir "Elevado al cuadrado es : ", cuadrados[i];
		FinPara
		
FinAlgoritmo