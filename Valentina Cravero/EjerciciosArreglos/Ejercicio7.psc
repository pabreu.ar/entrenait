Algoritmo CalcularGastoADestinosTuristicos
	//Escribir 3 destinos tur�sticos, la distancia desde Sunchales, el costo del litro
	//as� como la distancia . Se debe mostrar el gasto que demanda en viajar a cualquiera de
	//dichos lugares tur�sticos.
	
	Definir cantidad, kmPorLitro Como Entero;
	cantidad <- 3;
	kmPorLitro <- 15;
	
	Definir costoLitro Como Real;
	costoLitro <- 110;
	
	Dimension destinosTuristicos[cantidad];
	Dimension distanciaSunchales[cantidad]
	Dimension gastoViaje[cantidad];
	
	destinosTuristicos[1] <- "Carlos Paz";
	distanciaSunchales[1] <- 340;
	gastoViaje[1] <- distanciaSunchales[1] / kmPorLitro * costoLitro;
	
	destinosTuristicos[2] <- "Bariloche";
	distanciaSunchales[2] <-  1811.4;
	gastoViaje[2] <- distanciaSunchales[2] / kmPorLitro * costoLitro;
	
	destinosTuristicos[3] <- "Termas de Rio Hondo";
	distanciaSunchales[3] <-  544;
	gastoViaje[3] <- distanciaSunchales[3] / kmPorLitro * costoLitro;;
	
	
	Para i <- 1 hasta cantidad Con Paso 1 Hacer
		Escribir "Destino tur�stico ", i, ": " destinosTuristicos[i];
		Escribir "El gasto de viaje es: ", gastoViaje[i];
		Escribir " ";
	FinPara
	
FinAlgoritmo
