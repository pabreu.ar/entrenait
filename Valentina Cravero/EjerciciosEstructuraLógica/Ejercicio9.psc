Algoritmo CalcularHerenciaParaTresHijos
	
	Definir porcentajeTotal, porcentajeHijoMayor, porcentajeHijoMenor, porcentajeHijoMedio Como Entero; 
	porcentajeTotal <- 100;
	porcentajeHijoMenor <- 27;
	porcentajeHijoMayor <- porcentajeHijoMenor * 2;
	porcentajeHijoMedio <- porcentajeTotal - (porcentajeHijoMenor + porcentajeHijoMayor);
	
	Definir terrenoTotal, terrenoHijoMayor, terrenoHijoMenor, terrenoHijoMedio Como Real;
	terrenoHijoMedio <- 125.2;
	terrenoTotal <- porcentajeTotal * terrenoHijoMedio / porcentajeHijoMedio;
	terrenoHijoMayor <- porcentajeHijoMayor * terrenoTotal / porcentajeTotal;
	terrenoHijoMenor <- porcentajeHijoMenor * terrenoTotal / porcentajeTotal;
	
	Escribir "El terreno toral es: ", terrenoTotal;
	Escribir "Al hijo mayor le corresponde: ", terrenoHijoMayor, " mt2.";
	Escribir "Al hijo del medio le corresponde: ", terrenoHijoMedio, " mt2.";
	Escribir "Al hijo menor le corresponde: ", terrenoHijoMenor, " mt2.";
	
FinAlgoritmo
