Algoritmo CalcularYMostrarEdadDePersonaConA�oNacimientoYActual
	//Mostrar la edad de una persona, ingresando el a�o de nacimiento y el a�o actual.
	
	Definir anioNacimiento, anioActual como Entero;
	Escribir "Ingrese la a�o de nacimiento: ";
	Leer anioNacimiento;
	Escribir "Ingrese el a�o actual: ";
	Leer anioActual;
	
	Definir edad como Entero;
	edad <- anioActual - anioNacimiento;

	Escribir "La edad de la persona es: ",edad," a�os.";
FinAlgoritmo
