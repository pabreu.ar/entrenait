Algoritmo CalcularPromedioCalificaciones
	//Un estudiante realiza cuatro ex�menes, los cuales tienen la misma ponderaci�n. Realizar el pseudoc�digo y el diagrama de flujo
	//que representen el algoritmo correspondiente para obtener el promedio de las calificaciones obtenidas. 
	
	Definir i como Entero;
	Definir acumulador, nota Como Real;
	acumulador <- 0;
	
	Para i <- 1 Hasta 4 Con Paso 1 Hacer
		Escribir "Ingrese la nota: ";
		Leer nota;
		acumulador <- acumulador + nota;
	Fin Para
	
	Definir promedio Como Real;
	Definir cantExamen Como Entero;
	cantExamen <- 4;
	promedio <- acumulador / cantExamen;
	
	Escribir "El promedio de las notas es: ", promedio;
FinAlgoritmo
