Algoritmo NumeroDivisiblePorDosYCinco
	//Escribir un programa que indique si un n�mero es divisible entre dos y cinco al mismo tiempo.
	
	//Definir variable
	Definir num Como Entero;
	
	//Entrada de datos
	Escribir "Ingrese un n�mero"
	Leer num;
	
	//Procesamiento
	Definir resultado como Cadena;
	Si num % 2 = 0 y num % 5 = 0 Entonces
		resultado = "El n�mero es divisible por 2 y 5";
	SiNo
		resultado = "El n�mero no es divisible por 2 y 5";
	FinSi
	
	//Imprimir salida
	Escribir resultado;
FinAlgoritmo
