Algoritmo CalcularPerimetroAreaDeRectángulo
	//Escribir un programa que calcule el perímetro y área de un rectángulo.
	
	//Definir variables
	Definir base, altura Como Real;
	
	//Entrada de datos
	Escribir "Ingrese la base del rectángulo";
	Leer base;
	Escribir "Ingrese la altura del rectángulo";
	Leer altura;
	
	//Proceso 
	Definir area, perimetro Como Real;
	area = base * altura;
	perimetro = base * 2 + altura * 2;
	
	//Imprimir salida
	Escribir "El área del rectángulo es: " area;
	Escribir "El perímetro del rectángulo es: " perimetro;
FinAlgoritmo
