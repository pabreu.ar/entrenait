Algoritmo CalcularPorcentajeDeCantidad
	//Escribir un programa que calcula el porcentaje de una cantidad.
	
	//Definir variables
	Definir alicuota, importe Como Real;
	
	//Entrada de datos
	Escribir "Ingrese el porcentaje a calcular";
	Leer alicuota;
	Escribir "Ingrese el importe";
	Leer importe;
	
	//Proceso 
	Definir resultado Como Real;
	resultado = importe * alicuota / 100
	
	//Imprimir salida
	Escribir "El " alicuota "% de " importe " es " resultado;
FinAlgoritmo
