Algoritmo NumeroDivisiblePor14
	//Escribir un programa que indique si un n�mero es divisible entre 14.
	//Definir variable
	Definir num Como Entero;
	
	//Entrada de datos
	Escribir "Ingrese un n�mero"
	Leer num
	
	//Procesamiento
	Definir resultado Como Cadena;
	si num % 14 = 0 Entonces
		resultado = "El n�mero es divisible por 14";
	SiNo
		resultado = "El n�mero no es divisible por 14";
	FinSi
	
	//Imprimir salida
	Escribir resultado;
FinAlgoritmo
