Algoritmo CalcularÁreaPerímetroDePentágono
	//Escribir un programa que calcule el área y perímetro de un pentágono.
	
	//Definir variables
	Definir length, apotema Como Real;
	
	//Entrada de datos
	Escribir "Ingrese la longitud de los lados del pentágono";
	Leer length;
	Escribir "Ingrese la apotema del pentágono";
	//apotema: distancia más corta entre el centro del pentágono y uno de sus lados.
	Leer apotema;
	
	//Proceso 
	Definir perimetro, area Como Real;
	perimetro = length * 5;
	area = (perimetro * apotema) / 2;
	
	//Imprimir salida
	Escribir "El perímetro del pentágono es: " perimetro;
	Escribir "El área del pentágono es: " area;

FinAlgoritmo
