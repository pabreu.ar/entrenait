Algoritmo CalcularCambio
	//Escribir un programa que calcule el cambio que debe darse a un cliente.
	
	//Definir variables
	Definir total_a_pagar, pago_cliente Como Real;
	
	//Entrada de datos
	Escribir "Ingrese el total que debe pagar el cliente por la compra";
	Leer total_a_pagar;
	Escribir "Ingrese el monto con el que pag� el cliente";
	Leer pago_cliente
	
	//Proceso
	//Definir
	Definir cambio Como Real;
	//Calcular cambio
	cambio = pago_cliente - total_a_pagar;
	
	//Imprimir salida
	Escribir "el cambio que debe darse al cliente es: " cambio;
	
FinAlgoritmo
