Algoritmo SumaPositivaNegativaCero
	// Escribir un programa que indique si la suma de dos valores es positiva, negativa o cero.
	
	//Definir variables
	Definir n1, n2 Como Entero;
	
	//Entrada de datos
	Escribir "Ingrese el primer numero";
	Leer n1;
	Escribir "Ingrese el segundo numero";
	Leer n2;
	
	//Proceso
	//Definir suma
	Definir suma como Entero;
	//Calcular suma
	suma = n1 + n2;
	
	//Imprimir salida
	Si suma > 0 Entonces
		Escribir "La suma es positiva";
	Fin si
	
	Si suma = 0 Entonces
			Escribir "La suma es igual a cero";
	Fin si
	
	Si suma < 0 Entonces
		Escribir "La suma es negativa"			
	Fin si
FinAlgoritmo
