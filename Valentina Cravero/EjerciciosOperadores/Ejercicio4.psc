Algoritmo SumarDosNumerosEnteros
	//Escribir un programa que realice la suma de dos n�meros enteros.
	
	//Definir variables
	Definir n1, n2 Como Entero;
	//Entrada de datos
	Escribir "Ingrese el primer numero";
	Leer n1;
	Escribir "Ingrese el segundo numero";
	Leer n2;
	
	//Proceso
	//Definir suma
	Definir suma como Entero;
	//Calcular suma
	suma = n1 + n2;
	
	//Imprimir salida
	Escribir "la suma de: " n1 " y " n2 " es: " suma;
FinAlgoritmo
