Algoritmo CalcularAreaYPerimetroDeCirculo
	// Escribir un programa que calcule al per�metro y �rea de un c�rculo dado su radio.

	//Definir variables
	Definir radio, perimetro, area Como Real;
	
	//Entrada de datos
	Escribir "Ingrese el radio del circulo";
	Leer radio
	
	//Proceso1
	//Calcular perimetro
	perimetro = 2 * pi * radio;
	//Imprimir salida
	Escribir "El perimetro del circulo es: " perimetro;
	
	//Proceso2
	//Calcular area
	area = pi * radio ^ 2
	//Imprimir salida
	Escribir "el area del circulo es: " area;
FinAlgoritmo
