Algoritmo MayorDeCuatroNumeros
	//Escribir un programa que indique cu�l es el mayor de cuatro n�meros enteros.
	
	//Definir variables
	Definir n Como Entero;
	Definir max Como Entero;
	max = 0;
	Definir i Como Entero;
	
	//Proceso
	Para i <- 1 Hasta 4 Con Paso 1 Hacer
		Escribir "Ingrese un numero";
		Leer n;
		Si n > max Entonces
			max <- n
		FinSi
	FinPara
	
	//Imprimir salida
	Escribir "el mayor numero es: " max;
FinAlgoritmo
