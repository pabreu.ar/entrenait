Algoritmo CalcularCocienteResiduoDeNúmerosEnteros
	//Escribir un programa que calcule el cociente y el residuo dados dos números enteros.
	
	//Definir variables
	Definir numero1, numero2 Como Entero;
	
	//Entrada de datos
	Escribir "Ingrese el primer número";
	Leer numero1;
	Escribir "Ingrese el segundo número";
	Leer numero2;
	
	//Proceso 
	Definir cociente, resto Como Real;
	cociente = numero1 / numero2;
	resto = numero1 % numero2;
	
	//Imprimir salida
	Escribir "El cociente de la división es: " cociente;
	Escribir "El resto de la división es: " resto;
FinAlgoritmo
