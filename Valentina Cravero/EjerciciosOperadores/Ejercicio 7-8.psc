Algoritmo ImprimirInvertidoMarcaModeloDeVehiculo
	// Escribir un programa que lea de teclado la marca y modelo de un auto e imprima en pantalla el modelo y la marca (orden invertido a lo que se lee).
	
	//Definir variables
	Definir marca, modelo Como Cadena;
	
	//Entrada de datos
	Escribir "Ingrese la marca del veh�culo";
	Leer marca;
	Escribir "Ingrese el modelo del veh�culo";
	Leer modelo;
	
	//Proceso 
	Definir result Como Cadena;
	result = Concatenar(result,modelo);
	result = Concatenar(result,",");
	result = Concatenar(result,marca);

	//Imprimir salida
	Escribir result;
FinAlgoritmo
