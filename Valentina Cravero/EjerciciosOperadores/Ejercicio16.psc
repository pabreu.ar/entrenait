Algoritmo ObtenerD�aDeLaSemana
	//Escribir un programa que despliegue el d�a de la semana dado un n�mero de d�a.

	//Definir variable
	Definir num Como Entero;
	
	//Entrada de datos
	Escribir "Ingrese un n�mero del 1 al 7";
	Leer num
	
	//Proceso
	Definir dia como Cadena;
	Segun num Hacer
		1: 
			dia = "Domingo";
		2:
			dia = "Lunes";
		3: 
			dia = "Martes";
		4:
			dia = "Mi�rcoles";
		5:
			dia = "Jueves";
		6: 
			dia = "Viernes";
		7:
			dia = "S�bado"
	FinSegun
	
	//Imprimir salida
	Si num >= 1 y num <= 7 Entonces
		Escribir "El n�mero ingresado: " num " se corresponde con el d�a: " dia;
	Sino
		Escribir "El n�mero ingresado: " num " no se corresponde con un d�a de la semana";
	FinSi
FinAlgoritmo
