Algoritmo DeterminarSobrepesoEnFunciónDeAlturaYPeso
	//Escribir un programa que indique si una persona tiene sobrepeso (si se considera que debe pesar el número 
	//de centímetros que mide menos 100 o menos) Los datos deben ser positivos.
	
	//Definir variables
	Definir altura,peso Como Real
	
	//Entrada de datos - validar que sean positivos
	Repetir
		Escribir 'Ingrese su altura en centímetros'
		Leer altura
	Hasta Que altura>0
	Repetir
		Escribir 'Ingrese su peso'
		Leer peso
	Hasta Que peso>0
	
	//Proceso
	Definir resultado Como Cadena
	Si peso >= (altura - 100) Entonces
		resultado = "Tiene sobrepeso"
	FinSi
	
	Si peso <= (altura - 100) Entonces
		resultado = "No tiene sobrepeso"
	FinSi
	
	//Imprimir salida
	Escribir resultado
FinAlgoritmo
