Funcion posicion <- BusquedaSecuencial(arreglo, n, key)
	posicion = -1;
	Definir i Como Entero;
	i <- 1;
	Mientras posicion = -1 y i <= n Hacer
		Si arreglo[i] = key Entonces
			posicion <- i;
		FinSi
		i <- i + 1;
	FinMientras
FinFuncion

Funcion InicializarBaseDeDatosElecciones(cedulas, nombresVotantes)
	cedulas[1] <- "20259501";
	nombresVotantes[1] <-  "Mateo Delloni";
	
	cedulas[2] <- "18540178";
	nombresVotantes[2] <-  "Lucas Romero";
	
	cedulas[3] <- "14569874";
	nombresVotantes[3] <-  "Miguel Molina";
	
	cedulas[4] <- "40156230";
	nombresVotantes[4] <-  "Paola Scarafia";
	
	cedulas[5] <- "25469100";
	nombresVotantes[5] <-  "Juan Perez";
	
	cedulas[6] <- "36258996";
	nombresVotantes[6] <-  "Jonatan Giraudo";
FinFuncion

Algoritmo DeterminarPersonasHabilitadasAVotarEnMesa
	//El d�a de elecciones, en cada mesa se cuenta con un listado de las personas
	//que votar�n en dicha mesa. Los datos est�n guardados en dos vectores, en el primero la
	//c�dula y en el segundo el nombre. Se requiere un algoritmo que ingrese la c�dula de una
	//persona e informe si puede sufragar en ese puesto de votaci�n.
	
	Definir cedulaBuscada Como Cadena;
	Escribir "Ingrese la c�dula de la persona a buscar: ";
	Leer cedulaBuscada;
	
	//Definir Cantidad m�xima de votantes
	Definir cantidadVotantes Como Entero;
	cantidadVotantes <- 600;
	
	//Declarar vectores
	Dimension cedulas[cantidadVotantes];
	Dimension nombresVotantes[cantidadVotantes];
	
	//Inicializar valores
	InicializarBaseDeDatosElecciones(cedulas, nombresVotantes)
	
	//Buscar por c�dula de persona
	Definir indice Como Entero;
	indice <- BusquedaSecuencial(cedulas, cantidadVotantes, cedulaBuscada)
	Si indice <> -1 Entonces
		Escribir "La c�dula ", cedulaBuscada " se encuentra en la lista de la mesa. Corresponde a ", nombresVotantes[indice], ". Puede votar aqu�.";
	SiNo
		Escribir "La c�dula ", cedulaBuscada " no se encuentra en la lista de la mesa. No puede votar aqu�.";
	FinSi
FinAlgoritmo
