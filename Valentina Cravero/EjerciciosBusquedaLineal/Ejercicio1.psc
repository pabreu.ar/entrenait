Funcion posicion <- BusquedaSecuencial(arreglo, n, key)
	posicion = -1;
	Definir i Como Entero;
	i <- 1;
	Mientras posicion = -1 y i <= n Hacer
		Si arreglo[i] = key Entonces
			posicion <- i;
		FinSi
		i <- i + 1;
	FinMientras
FinFuncion

Funcion InicializarBaseDeDatosDelProfesor(codigos, nombres, notas)
	codigos[1] <- "01";
	nombres[1] <-  "Mateo";
	notas[1] <- 9;
	
	codigos[2] <- "02";
	nombres[2] <-  "Valentina";
	notas[2] <- 9.5;
	
	codigos[3] <- "03";
	nombres[3] <-  "Benjam�n";
	notas[3] <- 8;
	
	codigos[4] <- "04";
	nombres[4] <-  "Luciana";
	notas[4] <- 7.5;
	
	codigos[5] <- "05";
	nombres[5] <-  "Marcos";
	notas[5] <- 6;
FinFuncion

Algoritmo ConsultarNotaAPartirDelCodigo
	//Un profesor guarda los datos de sus estudiantes en tres vectores: c�digo,
	//nombre y nota. Se requiere un algoritmo para consultar la nota de un estudiante a partir de su c�digo.
	
	Definir codigoBuscado Como Cadena;
	Escribir "Ingrese el codigo del alumno a buscar: ";
	Leer codigoBuscado;
	
	//Cantidad m�xima de alumnos
	Definir cantidad Como Entero;
	cantidad <- 15;
	
	//Declarar vectores
	Dimension codigos[cantidad];
	Dimension nombres[cantidad];
	Dimension notas[cantidad];
	
	//Inicializar valores
	InicializarBaseDeDatosDelProfesor(codigos, nombres, notas)
	
	//Buscar por c�digo del alumno
	Definir indice Como Entero;
	indice <- BusquedaSecuencial(codigos, cantidad, codigoBuscado)
	Si indice <> -1 Entonces
		Escribir "La nota del estudiante es: ", notas[indice];
	SiNo
		Escribir "El c�digo del alumno no se encuentra en la base de datos";
	FinSi
FinAlgoritmo
