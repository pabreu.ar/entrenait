//Metodo de busqueda generico
Funcion pos = BusquedaSecuencial(arreglo, n, key)
	pos = -1;
	Definir i Como Entero;
	i = 1;
	
	Mientras pos = -1 Y i <= n Hacer
		Si arreglo[i] = key Entonces
			pos = i;
		FinSi
		i = i + 1;
	Fin Mientras
FinFuncion

//Llenar la BD
Funcion InicializarBaseDeDatos (nroCuentas, titulares, saldos)
	nroCuentas[1] = "123456789";
	titulares[1] = "Juan";
	saldos[1] = 10000;
	
	nroCuentas[2] = "987654321";
	titulares[2] = "Maria";
	saldos[2] = 25000;
	
	nroCuentas[3] = "123459876";
	titulares[3] = "Alejandro";
	saldos[3] = 30250;
	
	nroCuentas[4] = "987612345";
	titulares[4] = "Pablo";
	saldos[4] = 14505;
FinFuncion

Algoritmo CajeroAutomatico
	//En un hipot�tico cajero electr�nico, cuando un cliente solicita el saldo de su
	//cuenta desliza la tarjeta, el lector toma el n�mero de cuenta y el sistema busca dicho
	//n�mero en la base de datos. Si lo encuentra reporta el saldo, si no muestra un mensaje
	//de error. Suponiendo que la base de datos consiste en tres vectores: n�mero de cuenta,
	//titular y saldo. Dise�ar un algoritmo para consultar el saldo de una cuenta.
	
	Definir nroCuentaBuscado Como Cadena;
	Escribir "Ingrese el numero de cuenta que desea buscar"
	Leer nroCuentaBuscado;
	
	Definir cantC Como Entero;
	cantC = 1000;
	Dimension nroCuentas[cantC];
	Dimension titulares[cantC];
	Dimension saldos[cantC];
	
	InicializarBaseDeDatos(nroCuentas, titulares, saldos);
	
	Definir indice Como Entero;
	indice = BusquedaSecuencial(nroCuentas, cantC, nroCuentaBuscado)
	Si indice <> -1 Entonces
		Escribir "Su saldo es: $", saldos[indice];
	FinSi
	Si indice = -1 Entonces
		Escribir "Error! no se encuentra el numero de cuenta";
	FinSi
	
FinAlgoritmo