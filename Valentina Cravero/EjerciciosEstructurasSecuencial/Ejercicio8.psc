Algoritmo CalcularMasa
	//La presi�n, el vol�men y la temperatura de una masa de aire se relacionan por
    //la f�rmula: masa = (presi�n * vol�men)/(0.37 * (temperatura + 460)). Calcular la masa de
	//aire de un neum�tico de un veh�culo que est� en compostura en un servicio de alineaci�n y
    //balanceo.
	
	Definir presion, volumen, temperatura Como Real;
	Escribir "Ingrese la presi�n: ";
	Leer presion;
	Escribir "Ingrese el vol�men: ";
	Leer volumen;
	Escribir "Ingrese la temperatura: ";
	Leer temperatura;
	
	Definir masa Como Real
	masa <- (presion * volumen) / (0.37 * (temperatura + 460));
	
	Escribir "La masa de aire del neum�tico es: ", masa;
FinAlgoritmo
