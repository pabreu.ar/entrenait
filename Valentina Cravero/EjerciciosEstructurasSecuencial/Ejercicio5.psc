Algoritmo ConvertirGradosSexagesimalesARadianesYCentesimales
	//Ejercicio 5: Escribir un programa para la conversión de grados sexagesimales a radianes y Centesimales.
	
	Definir gradoSexagesimal Como Real;
	Escribir "Ingrese los grados sexagesimales que desea convertir: ";
	Leer gradoSexagesimal;
	
	Definir radianes, centesimales Como Real;
	//1° × pi/180 = 0.01745rad 
	radianes <- gradoSexagesimal * pi / 180;
	//1° × 200/180 = 1.111 grados centesimales
	centesimales <- gradoSexagesimal * 200 / 180;
	
	Escribir "Grados sexagesimales a convertir: ", gradoSexagesimal;
	Escribir "Representan: ", radianes, " radianes";
	Escribir "Representan: ", centesimales, " centesimales";
FinAlgoritmo
