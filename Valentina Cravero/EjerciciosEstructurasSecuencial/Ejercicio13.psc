Algoritmo CalcularPromedioGeneralYDeCadaMateria
	//Un alumno desea saber cu�l ser� su promedio general en las tres materias
	//m�s dif�ciles que cursa y cu�l ser� el promedio que obtendr� en cada una de ellas. Estas
	//materias se eval�an como se muestra a continuaci�n:
	//La calificaci�n de Matem�ticas se obtiene de la sig. manera:
	//Ex�men 90%
	//Promedio de tareas 10%
	//En esta materia se pidi� un total de tres tareas.
	//La calificaci�n de F�sica se obtiene de la sig. manera:
	//Ex�men 80%
	//Promedio de tareas 20%
	//En esta materia se pidi� un total de dos tareas.
	//La calificaci�n de Qu�mica se obtiene de la sig. manera:
	//Ex�men 85%
	//Promedio de tareas 15%
	//En esta materia se pidi� un total de tres tareas.
	
	//Matem�ticas
	Definir notaTareasMatematica, sumaTareasMatematica Como Real
	Definir i, cantTareasMatematica Como Entero;
	cantTareasMatematica <- 3;
	sumaTareasMatematica <- 0;
	Para i <- 1 Hasta cantTareasMatematica Con Paso 1 Hacer
		Escribir "Ingrese la nota ", i " de la tarea de matem�tica: ";
		Leer notaTareasMatematica;
		sumaTareasMatematica <- sumaTareasMatematica + notaTareasMatematica;
	FinPara
	
	Definir promedioTareasMatematica Como Real
	promedioTareasMatematica <- sumaTareasMatematica / cantTareasMatematica;
	
	Definir notaExamenMatematica Como Real;
	Escribir "Ingrese la nota del examen de matem�tica:"
	Leer notaExamenMatematica;
	
	Definir pondExamenMatematica, pondTareasMatematica Como Real;
	pondExamenMatematica <- 0.9;
	pondTareasMatematica <- 0.1;
	
	Definir calificacionFinalMatematica Como Real;
	calificacionFinalMatematica <- (notaExamenMatematica * pondExamenMatematica) + (promedioTareasMatematica * pondTareasMatematica);
	Escribir "La calificaci�n final obtenida en matem�tica es: ", calificacionFinalMatematica;
	
	//F�sica
	Definir notaTareasFisica, sumaTareasFisica Como Real
	Definir cantTareasFisica Como Entero;
	cantTareasFisica <- 2;
	sumaTareasFisica <- 0;
	Para i <- 1 Hasta cantTareasFisica Con Paso 1 Hacer
		Escribir "Ingrese la nota ", i " de la tarea de f�sica: ";
		Leer notaTareasFisica;
		sumaTareasFisica <- sumaTareasFisica + notaTareasFisica;
	FinPara
	
	Definir promedioTareasFisica Como Real
	promedioTareasFisica <- sumaTareasFisica / cantTareasFisica;
	
	Definir notaExamenFisica Como Real;
	Escribir "Ingrese la nota del examen de f�sica:"
	Leer notaExamenFisica;
	
	Definir pondExamenFisica, pondTareasFisica Como Real;
	pondExamenFisica <- 0.8;
	pondTareasFisica <- 0.2;
	
	Definir calificacionFinalFisica Como Real;
	calificacionFinalFisica <- (notaExamenFisica * pondExamenFisica) + (promedioTareasFisica * pondTareasFisica);
	Escribir "La calificaci�n final obtenida en f�sica es: ", calificacionFinalFisica;
	
	//Qu�mica
	Definir notaTareasQuimica, sumaTareasQuimica Como Real
	Definir cantTareasQuimica Como Entero;
	cantTareasQuimica <- 3;
	sumaTareasQuimica <- 0;
	Para i <- 1 Hasta cantTareasQuimica Con Paso 1 Hacer
		Escribir "Ingrese la nota ", i " de la tarea de qu�mica: ";
		Leer notaTareasQuimica;
		sumaTareasQuimica <- sumaTareasQuimica + notaTareasQuimica;
	FinPara
	
	Definir promedioTareasQuimica Como Real
	promedioTareasQuimica <- sumaTareasQuimica / cantTareasQuimica;
	
	Definir notaExamenQuimica Como Real;
	Escribir "Ingrese la nota del examen de qu�mica:"
	Leer notaExamenQuimica;
	
	Definir pondExamenQuimica, pondTareasQuimica Como Real;
	pondExamenQuimica <- 0.85;
	pondTareasQuimica <- 0.15;
	
	Definir calificacionFinalQuimica Como Real;
	calificacionFinalQuimica <- (notaExamenQuimica * pondExamenQuimica) + (promedioTareasQuimica * pondTareasQuimica);
	Escribir "La calificaci�n final obtenida en Qu�mica es: ", calificacionFinalQuimica;
	
	Definir promedioGeneral Como Real;
	promedioGeneral <- (calificacionFinalMatematica + calificacionFinalFisica + calificacionFinalQuimica) / 3;
	
	Escribir "El promedio general de las tres materias es: ", promedioGeneral;
FinAlgoritmo
