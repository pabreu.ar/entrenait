Algoritmo CalcularDineroParaCadaAreaDeMontoPresupuestal
	//En un hospital existen tres �reas: Ginecolog�a, Pediatr�a, Traumatolog�a. El presupuesto anual del hospital se reparte conforme a la sig. tabla:
	//�rea: % del presupuesto:
	//Ginecolog�a 40%
	//Traumatolog�a 30%
    //Pediatr�a 30%
	//Obtener la cantidad de dinero que recibir� cada �rea, para cualquier monto presupuestal.
	
	Definir montoPresupuestal Como Real;
	Escribir "Ingrese el monto presupuestal anual: ";
	Leer montoPresupuestal;
	
	Definir porcGinecologia, porcTraumatologia, porcPediatria Como Real;
	porcGinecologia <- 0.4;
	porcTraumatologia <- 0.3;
	porcPediatria <- 0.3;
	
	Definir montoGinecologia, montoTraumatologia, montoPediatria Como Real;
	montoGinecologia <- porcGinecologia * montoPresupuestal;
	montoTraumatologia <- porcTraumatologia * montoPresupuestal;
	montoPediatria <- porcPediatria * montoPresupuestal;
	
	Escribir "El monto presupuestal es: $", montoPresupuestal, " y se divide en las �reas: ";
	Escribir "Ginecolog�a: recibe $", montoGinecologia;
	Escribir "Traumatolog�a: recibe $", montoTraumatologia;
	Escribir "Pediatr�a: recibe $", montoPediatria;
FinAlgoritmo
