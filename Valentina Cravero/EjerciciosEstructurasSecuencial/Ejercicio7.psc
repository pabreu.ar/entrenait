Algoritmo CalcularCalificacionFinal
	//Un alumno desea saber cuál será su calificación final en la materia de
	//Algoritmos. Dicha calificación se compone de los siguientes porcentajes:
	//55% del promedio de sus tres calificaciones parciales.
	//30% de la calificación del exámen final.
	//15% de la calificación de un trabajo final.
	
	Definir calificacion, suma Como Real;
	Definir i, cantCalificacion Como Entero;
	suma <- 0;
	cantCalificacion <- 3;
	Para i <- 1 Hasta cantCalificacion Con Paso 1 Hacer
		Escribir "Ingrese la calificación ", i, ":";
		Leer calificacion;
		suma <- suma + calificacion;
	FinPara
	
	Definir promedio Como Real;
	//Calcular promedio de las 3 calificaciones
	promedio <- suma / cantCalificacion;
	//Escribir suma;
	//Escribir promedio;
	
	Definir examenFinal, trabajoFinal Como Real
	Escribir "Ingrese la calificación del examen final: ";
	Leer examenFinal;
	Escribir  "Ingrese la calificación del trabajo final: ";
	Leer trabajoFinal;
	
	//Definir ponderaciones
	Definir pondPromedio, pondExamen, pondTrabajo Como Real;
	pondPromedio <- 0.55;
	pondExamen <- 0.3;
	pondTrabajo <- 0.15;
	
	Definir calificacionFinal Como Real;
	calificacionFinal <- (promedio * pondPromedio) + (examenFinal * pondExamen) + (trabajoFinal * pondTrabajo);
	Escribir "La calificación final de la materia es: ", calificacionFinal;
FinAlgoritmo
