Algoritmo DeterminarTiempoPromedioDeUnaSemana
	//Todos los lunes, mi�rcoles y viernes, una persona corre la misma ruta y
	//cronometra los tiempos obtenidos. Determinar el tiempo promedio que la persona tarda en
	//recorrer la ruta en una semana cualquiera.
	
	Definir tiempoLunes, tiempoMiercoles, tiempoViernes Como Real;
	
	Escribir "Ingrese el tiempo obtenido el d�a lunes: ";
	Leer tiempoLunes;
	Escribir "Ingrese el tiempo obtenido el d�a mi�rcoles: ";
	Leer tiempoMiercoles;
	Escribir "Ingrese el tiempo obtenido el d�a viernes: ";
	Leer tiempoViernes;
	
	Definir tiempoTotal Como Real;
	tiempoTotal <- tiempoLunes + tiempoMiercoles + tiempoViernes;
	
	Definir promedio Como Real;
	Definir cantidad Como Entero;
	cantidad <- 3;
	promedio <- tiempoTotal / cantidad;
	
	Escribir "El tiempo promedio en recorrer la ruta es: ", promedio;
FinAlgoritmo
