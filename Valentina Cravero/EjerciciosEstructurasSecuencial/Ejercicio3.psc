Algoritmo ConvertirCelsiusAFarenheitYViceversa
	// Una temperatura Celsius (Centígrados) puede ser convertida a una
	//temperatura equivalente F. Escriba un programa para calcular ambos valores.
	
	Definir tempInicial Como Real;
	Escribir "Ingrese la temperatura a convertir: ";
	Leer tempInicial;
	
	Definir unidad Como Caracter;
	Mientras Mayusculas(unidad) <> "C" y Mayusculas(unidad) <> "F" Hacer
	Escribir "Ingrese si la unidad de la temperatura es Celsius (C) o Farenheit (F): ";
	Leer unidad;
	Fin Mientras

	Definir resultado Como Real;
	Si Mayusculas(unidad) = "C" Entonces;
		resultado <- (tempInicial * 9/5) + 32;
		Escribir " La temperatura en grados Farenheit es: ", resultado;
	FinSi
	
	Si Mayusculas(unidad) = "F" Entonces;
		resultado <- (tempInicial - 32) * 5/9;
		Escribir "La temperatura en grados Celsius es: ", resultado;
	FinSi
	
FinAlgoritmo
