Algoritmo CalcularSueldoFinalConSueldoBaseYComisiones
	//Un vendedor recibe un sueldo base m�s un 10% extra por comisi�n de sus
	//ventas. El vendedor desea saber cu�nto dinero obtendr� por concepto de comisiones por
	//las tres ventas que realiza en el mes y el total que recibir� en el mes tomando en cuenta su
	//sueldo base y sus comisiones.
	
	Definir venta, sumaVenta Como Real;
	Definir i, cantidadVenta Como Entero;
	cantidadVenta <- 3;
	sumaVenta <- 0;
	Para i <- 1 Hasta cantidadVenta Con Paso 1 Hacer
		Escribir "Ingrese el monto de la venta ", i;
		Leer venta;
		sumaVenta <- sumaVenta + venta;
	FinPara
	
	Definir porcentajeComision, comision Como Real
	porcentajeComision <- 0.3;
	comision <- sumaVenta * porcentajeComision;
	
	Definir sueldoBase, sueldoFinal Como Real;
	Escribir "Ingrese su sueldo base: ";
	Leer sueldoBase;
	
	sueldoFinal <- sueldoBase + comision;
	Escribir "Su sueldo final es: ", sueldoFinal;
FinAlgoritmo
