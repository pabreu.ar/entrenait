Algoritmo CaclularMontoDadoCapitalTasaInterésYPeriodos
	//Calcula el Monto a devolver si nos prestan un capital c, a una tasa de interés
	//t% durante n periodos.
	
	Definir c, n, t Como Real;
	Escribir "Ingrese el capital a invertir: ";
	Leer c;
	Escribir "Ingrese el período en años: ";
	Leer n;
	Escribir "Ingrese la tasa de interés: ";
	Leer t;
	
	Definir interes, monto Como Real;
	//Caclular interés
	interes <- c * (t/100) * n;
	//Calcular monto
	monto <- c + interes;
	
	Escribir "El monto es: $", monto;
FinAlgoritmo
