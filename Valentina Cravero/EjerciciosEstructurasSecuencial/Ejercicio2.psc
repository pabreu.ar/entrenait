Algoritmo SumarCuatroCifrasDeEnteroPositivo
	//Escribir un programa que determine la suma de las cifras de un entero positivo de 4 cifras.
	
	Definir num Como Entero
	
	Mientras num < 1000 y num < 9999 Hacer
		Escribir "Ingrese un n�mero de cuatro cifras entre 1000 y 9999: ";
		Leer num;
	FinMientras
	
	Definir millar, centena, decena, unidad, resto Como Entero;
	millar <- Trunc(num / 1000);
	resto <- num % 1000;
	centena <- Trunc(resto / 100);
	resto <- resto % 100;
	decena <- Trunc(resto / 10);
	unidad <- resto % 10;
	
	Definir suma Como Entero;
	suma <- millar + centena + decena + unidad;
	Escribir "La suma de las cuatro cifras es: ", suma;
FinAlgoritmo
