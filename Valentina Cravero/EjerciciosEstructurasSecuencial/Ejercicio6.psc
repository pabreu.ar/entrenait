Algoritmo CalcularGanaciaSobreCapitalInvertido
	//Suponga que un individuo desea invertir su capital en un banco y desea
	//saber cu�nto dinero ganar� despu�s de un mes si el banco paga a raz�n de 2% mensual.
	
	Definir capitalInvertido Como Real;
	Escribir "Ingrese el capital a invertir: ";
	Leer capitalInvertido;
	
	Definir porcentajeGanancia, ganancia Como Real;
	porcentajeGanancia <- 0.02;
	ganancia <- capitalInvertido * porcentajeGanancia;
	
	Definir montoFinal Como Real;
	montoFinal <- capitalInvertido + ganancia;
	
	Escribir "La ganancia por el capital invertido es: $", ganancia;
	Escribir "El monto de dinero que ganar� despu�s de un mes es: $", montoFinal;
FinAlgoritmo
