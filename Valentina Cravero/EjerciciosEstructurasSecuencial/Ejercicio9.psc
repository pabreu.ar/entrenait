Algoritmo CalcularPulsasionesDeUnaPersona
	//Calcular el n�mero de pulsaciones que una persona debe tener por cada 10
    //segundos de ejercicio, si la f�rmula es: num. pulsaciones = (220 - edad)/10.
	
	Definir edad Como Entero;
	Escribir "Ingrese la edad: ";
	Leer edad;
	
	Definir numPulsasiones Como Real;
	numPulsasiones <- (220 - edad) / 10;
	
	Escribir "La persona de ", edad, " a�os, deber� tener ", numPulsasiones " pulsasiones por cada 10 segundos de ejercicio.";
FinAlgoritmo
