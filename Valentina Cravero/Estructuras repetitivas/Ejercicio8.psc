Algoritmo EscribirNumerosPrimosDel1Al100
	//Hacer un algoritmo que escriba todos los n�meros primos del 1 al 100.
	//N�mero primo: n�mero natural mayor que 1 que tiene �nicamente dos divisores positivos distintos: �l mismo y el 1.
	
	Definir esPrimo Como Logico;
	Definir divisor Como Numero;
	Definir i Como Entero;
	Para i <- 2 Hasta 100 Hacer
		esPrimo <- Verdadero;
		divisor <- 2;
		Mientras esPrimo Y divisor < i Hacer
			esPrimo <- i MOD divisor <> 0;
			divisor <- divisor + 1;
		FinMientras
		Si esPrimo Entonces
			Escribir i ",";
		FinSi
	FinPara
FinAlgoritmo
