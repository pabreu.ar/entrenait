Algoritmo CalcularSumaPrimerosCienNumConDoWhile
	//Hacer un algoritmo en Pseint para calcular la suma de los primeros cien n�meros con un ciclo Do-While.
	
	Definir suma, contador Como Entero;
	suma <- 0; contador <- 0;
	
	Hacer
		contador <- contador + 1;
		suma <- suma + contador;
	Mientras Que contador < 100
	
	Escribir "La suma de los primeros cien n�meros es: ", suma;
FinAlgoritmo
