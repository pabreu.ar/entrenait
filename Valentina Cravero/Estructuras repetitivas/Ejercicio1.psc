Algoritmo CalcularRestoCocienteConRestasSucesivas
	//Hacer un algoritmo en Pseint para calcular el resto y cociente por medio de restas sucesivas.
	
	Definir n, d Como Entero;
	Escribir "Ingrese el numerador";
	Leer n;
	Escribir "Ingrese el denominador";
	Leer d
	
	Definir resto, cont Como Entero;
	resto <- 0;
	Cont <- 0;
	
	Repetir
		n <- n-d;
		resto <- n;
		cont <- cont + 1;
	Hasta Que n < d
	
	Escribir "El cociente es: ", cont;
	Escribir "El resto es: ", resto;
	
	
	
FinAlgoritmo
