Algoritmo CalcularFactorialDeUnNúmero
	//Hacer un algoritmo en Pseint que permita calcular el factorial de un número.
	//Factorial de un número: producto de todos los números enteros positivos desde 1 hasta n.
	
	Definir n Como Entero;
	Escribir "Ingrese un número: ";
	Leer n;
	
	Definir i, factorial Como Entero;
	factorial <- 1;
	Para i<- 1 Hasta n Con Paso 1 Hacer
		factorial <- factorial*i;
	FinPara
	
	Escribir "El factorial de ", n " es: ", factorial;
FinAlgoritmo
