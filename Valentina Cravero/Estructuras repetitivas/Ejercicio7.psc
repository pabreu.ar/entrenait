Algoritmo MostrarSerieFibonacci
	// Hacer un algoritmo en Pseint para calcular la serie de Fibonacci.
	//sucesi�n de Fibonacci:
	
	Definir n Como Entero;
	Escribir "Ingrese el n�mero n hasta donde desea imprimir la serie: ";
	Leer n;
	
	Definir i, primerNumero, segundoNumero, siguienteNumero Como Entero;
	primerNumero <- 0
	segundoNumero <- 1
	
	Para i <- 1 Hasta n Hacer
		//Escribir al principio incluye el 0 en la sucesi�n.
		//Escribir primerNumero;
		siguienteNumero <- primerNumero + segundoNumero;
		primerNumero <- segundoNumero;
		segundoNumero <- siguienteNumero;
		//Escribir al final del para no incluye el 0 en la sucesi�n.
		Escribir primerNumero;
	FinPara
FinAlgoritmo
