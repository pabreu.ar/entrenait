Algoritmo CalcularSumaPrimerosNúmeros
	//Hacer un algoritmo en Pseint para calcular la suma de los n primeros números.
	
	Definir n Como Entero;
	Escribir "Ingrese la cantidad de números consecutivos que desea sumar: ";
	Leer n;
	
	Definir i, suma Como Entero;
	suma <- 0;
	Para i <- 1 Hasta n Con Paso 1 Hacer
		suma <- suma + i;
	FinPara
	Escribir "La suma de los primeros " n " números es: ", suma;
FinAlgoritmo

//Resolución con While:
//Definir contador, suma Como Entero;
//contador <- 1; suma <- 0;

//Mientras contador <= n Hacer
	//suma <- suma + contador;
	//contador <- contador + 1;
//FinMientras

//Escribir suma;