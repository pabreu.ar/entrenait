Algoritmo SumaDeNumerosParesHasta1000
	//Hacer un algoritmo en Pseint para realizar la suma de todos los n�meros pares hasta el 1000.

	Definir n Como Entero
	n <- 1
	Definir suma Como Entero
	Mientras n <= 1000
		Si n % 2 = 0
			suma = suma + n
		FinSi
		n = n + 1
	FinMientras
	
	Escribir "La suma de todos los n�meros pares hasta el 1000 es: " , suma
FinAlgoritmo
