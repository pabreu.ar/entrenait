Algoritmo DeterminarCiudadConMayorPoblaci�n
	//Se quiere saber cu�l es la ciudad con la poblaci�n de m�s personas, son tres
	//provincias y once ciudades cada una, hacer un algoritmo en Pseint que nos permita saber eso.
	
	Definir provincia, ciudad, ciudadMasPobladaPorProvincia, provinciaConCiudadMasPoblada, ciudadMasPobladaTotal Como Cadena;
	Definir cantHabitantes, mayorCantHabitantesPorProvincia, MayorCantHabitantesTotal Como Entero;
	mayorCantHabitantes <- 0; mayorCantHabitantesPorProvincia <- 0; MayorCantHabitantesTotal <- 0;
	
	Definir i, j , cantMaxProvincia, cantMaxCiudad Como Entero;
	cantMaxProvincia <- 3;
	cantMaxCiudad <- 11;
	
	Para i <- 1 Hasta cantMaxProvincia Con Paso 1 Hacer
		Escribir "Ingrese el nombre de la provincia. ", i " de " cantMaxProvincia " provincias.";
		Leer provincia;
		Para j <- 1 Hasta cantMaxCiudad Con Paso 1 Hacer
			Escribir "Ingrese el nombre de la ciudad. ", j " de " cantMaxCiudad " ciudades."
			Leer ciudad;
			Escribir "Ingrese la cantidad de habitantes de la ciudad: "
			Leer cantHabitantes;
			Si cantHabitantes > mayorCantHabitantesPorProvincia Entonces
				mayorCantHabitantesPorProvincia <- cantHabitantes;
				ciudadMasPobladaPorProvincia <- ciudad;
			FinSi
		FinPara
		Escribir "En la provincia ", provincia, " la ciudad con mayor cantidad de habitantes es: ", ciudadMasPobladaPorProvincia " con " mayorCantHabitantesPorProvincia;
		Si mayorCantHabitantesPorProvincia > MayorCantHabitantesTotal Entonces
			provinciaConCiudadMasPoblada <- provincia;
			ciudadMasPobladaTotal <- ciudadMasPobladaPorProvincia;
			mayorcantHabitantesTotal <- mayorCantHabitantesPorProvincia;
			
		FinSi
	FinPara
	
	Escribir "La ciudad m�s poblada es: ", ciudadMasPobladaTotal " perteneciente a la provincia de: ", provinciaConCiudadMasPoblada " con una cantidad de: " mayorcantHabitantesTotal, " habitantes."
FinAlgoritmo
