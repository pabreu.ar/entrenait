Algoritmo DeterminarMayorValor
	Definir valor1, valor2 Como Real
	Escribir "Ingrese un valor"
	Leer valor1
	Escribir "Ingrese otro valor distinto"
	Leer valor2
	Definir resultado Como Real
	resultado <- valor1
	Si valor1<valor2 Entonces
		resultado <- valor2
	FinSi
	Escribir "El mayor de los dos valores es: " resultado
FinAlgoritmo
