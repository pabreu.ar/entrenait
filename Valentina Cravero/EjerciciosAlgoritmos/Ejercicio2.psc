Algoritmo ImprimirMayorMenosValor
	Definir a,b,c Como Real
	Escribir "Ingrese el primer valor"
	Leer a
	Escribir "Ingrese el segundo valor"
	Leer b
	Escribir "Ingrese el tercer valor"
	Leer c
	Si a=b o a=c o b=c  Entonces
		Escribir "Los valos ingresados son iguales"
	SiNo
		Definir max, min Como Real
		Si a>b Entonces
			max <- a
			min <- b
		SiNo
			max <- b
			min <- a
		FinSi
		Si c>max Entonces
			max <- c
		SiNo
			Si c<min Entonces
				min <- c
			FinSi
		FinSi
		Escribir "El mayor valor es " max
		Escribir "El menor valor es " min
	FinSi
FinAlgoritmo
