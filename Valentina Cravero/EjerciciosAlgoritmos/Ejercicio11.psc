Algoritmo DeterminarAccesoACicloFormativoDeGradoSuperior
	Definir titulo como Caracter
	Repetir
		Escribir "Ingrese S o N para indicar si cuenta con titulo de bachiller"
		Leer titulo
	Hasta Que Mayusculas(titulo) = "S" o Mayusculas(titulo) = "N"
	Si Mayusculas(titulo) = "S" Entonces
		Escribir "Puede acceder al curso"
	SiNo
		Definir prueba Como Caracter
		Repetir
			Escribir "Ingrese S o N para indicar si supero la prueba de acceso"
			Leer prueba
		Hasta Que Mayusculas(prueba) = "S" o Mayusculas(prueba) = "N"
		Si Mayusculas(prueba) = "S" Entonces
			Escribir "Puede acceder al curso"
		SiNo
			Escribir "Lo sentimos, no puede acceder al curso"
		FinSi
	FinSi
FinAlgoritmo
