Algoritmo DeterminarAñosMesesSemanasDíasAPartirDeunNumero
	Definir num Como Entero
	Escribir 'Ingrese un número de días'
	Leer num
	Definir anios,meses,semanas Como Real
	anios <- num/365
	meses <- num/30
	semanas <- num/7
	Escribir 'El número ingresado representa:'
	Escribir Trunc(anios),' años'
	Escribir Trunc(meses),' meses'
	Escribir Trunc(semanas),' semanas'
	Escribir num,' días'
FinAlgoritmo
