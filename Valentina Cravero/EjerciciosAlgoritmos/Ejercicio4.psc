Algoritmo OrdenarNumerosDeMenorAMayor
	Definir num1, num2 Como Real
	Escribir "Ingrese el primer n�mero"
	Leer num1
	Escribir "Ingrese el segundo n�mero"
	Leer num2
	Si num1 = num2 Entonces
		Escribir "Los n�meros ingresados son iguales"
	SiNo
		Definir resultado Como Cadena
		Si num1 > num2 Entonces
			resultado <- Concatenar(ConvertirATexto(num1),Concatenar(",",ConvertirATexto(num2)))
		SiNo
			resultado <- Concatenar(ConvertirATexto(num2),Concatenar(",",ConvertirATexto(num1)))
		FinSi
		Escribir "N�meros ordenados: ", resultado
	FinSi
FinAlgoritmo
