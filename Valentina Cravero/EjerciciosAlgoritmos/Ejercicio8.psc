Algoritmo IntercambiarValoresDeVariables
	Definir A,B Como Real
	Escribir 'Ingrese el valor de A'
	Leer A
	Escribir 'Ingrese el valor de B'
	Leer B
	Definir auxiliarA,auxiliarB Como Real
	auxiliarA <- B
	auxiliarB <- A
	Escribir 'El valor de la variable A es: ',auxiliarA
	Escribir 'El valor de la variable B es: ',auxiliarB
FinAlgoritmo
