Algoritmo CalcularPorcentajeDeNiñosYNiñas
	Definir cantNinos, cantNinas Como Entero
	Escribir "Ingrese la cantidad de niños en el curso"
	Leer cantNinos
	Escribir "Ingrese la cantidad de niñas en el curso"
	Leer cantNinas
	Definir cantTotal Como Entero
	cantTotal <- cantNinos + cantNinas
	Definir porcentajeNinos, porcentajeNinas Como Real
	porcentajeNinos <- cantNinos*100/cantTotal
	porcentajeNinas <- cantNinas*100/cantTotal
	Escribir "La cantidad total de alumnos en el curso es: " cantTotal
	Escribir "El porcentaje de niños es: " porcentajeNinos
	Escribir "El porcentaje de niñas es: " porcentajeNinas
FinAlgoritmo
