Algoritmo CalcularCantidadNumerosEntre50y75MayoresDe80yMenoresDe30
	//Realice un programa que le permita determinar de una lista de n�meros
	//ingresados: �Cu�ntos est�n entre el 50 y 75, ambos inclusive? �Cu�ntos mayores de
	//80?�Cu�ntos menores de 30?
	
	Definir n Como Entero;
	Escribir "Ingrese la cantidad de n�meros a ingresar en la lista: ";
	Leer n
	
	Dimension listaNumeros[n];
	
	Definir num, i Como Entero;
	i <- 1;
	Para i <- 1 Hasta n Con Paso 1 Hacer
		Escribir "Ingrese un n�mero: ";
		Leer listaNumeros[i];
	FinPara
	
	Definir cantidad1, cantidad2, cantidad3 Como Entero;
	//cantidad1 acumula la cantidad de n�meros entre 50 y 75.
	//cantidad2 acumula la cantidad de n�meros mayores a 80.
	//cantidad3 acumula la cantidad de n�meros menores a 30.
	
	Para i <- 1 Hasta n Con Paso 1 Hacer
		Si listaNumeros[i] >= 50 y listaNumeros[i] <= 75 Entonces
			cantidad1 <- cantidad1 + 1;
		FinSi
		Si listaNumeros[i] > 80 Entonces
			cantidad2 <- cantidad2 + 1;
		Fin si
		Si listaNumeros[i] < 30 Entonces
			cantidad3 <- cantidad3 + 1;
		FinSi
	FinPara
	
	Escribir "La cantidad de n�meros de la lista..."
	Escribir "-Entre 50 y 75 es: ", cantidad1;
	Escribir "-Mayores a 80 es: ", cantidad2;
	Escribir "-Menores a 30 es: ", cantidad3;
FinAlgoritmo

