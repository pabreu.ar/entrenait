Algoritmo ConvertirNumeroACorrespondienteRomano
	//Escriba un programa que convierta los siguientes n�meros a su
	//correspondiente n�mero romano: 1, 5, 9, 14, 20, 26, 40, 50, 90, 99, 100, 500 y 1000. Para
	//cualquier otro n�mero se deber� reportar: "No existe traducci�n disponible".
	
	Definir num Como Entero;
	Escribir "Ingrese un n�mero entre los posibles: 1, 5, 9, 14, 20, 26, 40, 50, 90, 99, 100, 500 y 1000.";
	Leer num;

	Segun num Hacer
		1:
			Escribir "I";
		5:
			Escribir "V";
		9:
			Escribir "IX";
		14:
			Escribir "XIV";
		20:
			Escribir "XX";
		26:
			Escribir "XXVI";
		40:
			Escribir "XL";
		50: 
			Escribir "L";
		90: 
			Escribir "XC";
		99:
			Escribir "XCIX";
		100:
			Escribir "C";
		500:
			Escribir "D";
		1000:
			Escribir "M"
		De Otro Modo:
			Escribir "No existe traducci�n disponible";
	Fin Segun
FinAlgoritmo
