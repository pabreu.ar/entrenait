Algoritmo Mostrar15Lineas
	//Programa que muestra 15 l�neas como estas: 1 12 123 1234
	
	Definir n Como Entero;
	n <- 15;
	
	Definir resultado Como Cadena;
	resultado <- "";
	Para i <- 1 Hasta n Hacer
		resultado <- resultado + ConvertirATexto(i);
		Escribir resultado;
	FinPara
FinAlgoritmo 

//Algoritmo sin_titulo
	//Definir i Como Entero;
	//i <- 1;
	//resultado <- "";
	//Repetir
		//resultado <- resultado + ConvertirATexto(i);
		//Escribir resultado;
		//i <- i + 1;
	//Hasta Que i > 15;
//FinAlgoritmo
