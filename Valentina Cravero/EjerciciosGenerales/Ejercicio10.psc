Algoritmo CalcularCantidadCajas
	//Juan Carlos es jefe de bodega en una f�brica de pa�ales desechables y
	//sabe que la producci�n diaria es de 744 pa�ales y que en cada caja donde se empacan
	//para la venta caben 12 pa�ales. �Cu�ntas cajas debe conseguir Juan Carlos para empacar
	//los pa�ales fabricados en una semana (5 d�as)?
	
	//Calculos:
	//1 d�a --> 744
	// 5 d�as --> (744 * 5) = 3720
	//cant cajas --> 3720/12
	
	Definir produccionDiaria, panialesPorCaja, cantDiasSemana Como Entero;
	produccionDiaria <- 744;
	panialesPorCaja <- 12;
	cantDiasSemana <- 5;
	
	Definir produccionSemanal, cantidadCajas Como Entero;
	produccionSemanal <- produccionDiaria * cantDiasSemana;
	cantidadCajas <- produccionSemanal / panialesPorCaja;
	
	Escribir "La cantidad de cajas necesarias para empacar los pa�ales fabricados en una semana es: ", cantidadCajas;
FinAlgoritmo
