Algoritmo ObtenerDiaSiguiente
    //Programa que muestra el d�a que ser� ma�ana.
	
	Definir anio, mes, dia Como Entero;
	Escribir Sin Saltar "Ingrese el a�o: ";
    Leer anio;
    Escribir Sin Saltar "Ingrese el mes: ";
    Leer mes;
    Escribir Sin Saltar "Ingrese el d�a: ";
    Leer dia;
	
	Definir dia_siguiente, mes_siguiente, anio_siguiente Como Entero;
	dia_siguiente = dia;
	//Si se ingresa un d�a fuera de rango
    Si dia<1 O dia>31 O (mes = 2 Y dia > 28 Y anio mod 4 <> 0) O ((mes = 4 O mes = 6 O mes = 9 O mes = 11) Y dia = 31) O (mes = 2 Y dia > 29 Y anio mod 4 = 0) Entonces
        dia_siguiente <- 0;
        Escribir "D�a incorrecto";
    FinSi
	
	//Si se ingresa un mes fuera de rango
    Si mes<1 O mes>12 Entonces
        dia_siguiente <- 0;
        Escribir "Mes incorrecto";
    FinSi
	
	//Si se ingresa un anio alejado de la realidad
    Si anio<1000 O anio>=10000 Entonces
        dia_siguiente <- 0;
        Escribir "A�o incorrecto";
    FinSi
	
	Si dia_siguiente <> 0 Entonces
		
		//Excepciones cuando termina un mes
		Si (dia = 30 Y (mes = 4 O mes = 6 O mes = 9 O mes = 11)) O dia = 31 O (dia = 28 Y mes = 2) O (dia = 29 Y mes = 2 Y anio mod 4 = 0) Entonces
			dia_siguiente <- 1;
			mes_siguiente <- mes + 1;
		SiNo
			dia_siguiente <- dia + 1;
			mes_siguiente <- mes;
		FinSi
		
		//Excepciones cuando empieza nuevo a�o
		Si mes_siguiente = 13 Entonces
			anio_siguiente <- anio + 1;
			mes_siguiente <- 1;
		SiNo
			anio_siguiente <- anio;
		FinSi
	FinSi
	
    Escribir "A�o de ma�ana: ", anio_siguiente;
    Escribir "Mes de ma�ana: ", mes_siguiente;
    Escribir "D�a de ma�ana: ", dia_siguiente;
FinAlgoritmo