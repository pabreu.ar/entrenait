Funcion posicion <- BusquedaSecuencial(arreglo, n, key)
	posicion = -1;
	Definir i Como Entero;
	i <- 1;
	Mientras posicion = -1 y i <= n Hacer
		Si arreglo[i] = key Entonces
			posicion <- i;
		FinSi
		i <- i + 1;
	FinMientras
FinFuncion

Funcion InicializarBaseDeDatosNumerosRomanos(decimales, romanos)
	decimales[1] <- 1;
	romanos[1] <- "I";

	decimales[2] <- 5;
	romanos[2] <- "V";
	
	decimales[3] <- 9;
	romanos[3] <- "IX";
	
	decimales[4] <- 14;
	romanos[4] <- "XIV";
	
	decimales[5] <- 20;
	romanos[5] <- "XX";
	
	decimales[6] <- 26;
	romanos[6] <- "XXVI";
	
	decimales[7] <- 40;
	romanos[7] <- "XL";
	
	decimales[8] <- 50;
	romanos[8] <- "L";
	
	decimales[9] <- 90;
	romanos[9] <- "XC";
	
	decimales[10] <- 99;
	romanos[10] <- "XCIX";
	
	decimales[11] <- 100;
	romanos[11] <- "C";
	
	decimales[12] <- 500;
	romanos[12] <- "D";
	
	decimales[13] <- 1000;
	romanos[13] <- "M";
FinFuncion

Algoritmo ConvertirNumeroACorrespondienteRomano
	//Escriba un programa que convierta los siguientes n�meros a su
	//correspondiente n�mero romano: 1, 5, 9, 14, 20, 26, 40, 50, 90, 99, 100, 500 y 1000. Para
	//cualquier otro n�mero se deber� reportar: "No existe traducci�n disponible".
	
	Definir num Como Entero;
	Escribir "Ingrese un n�mero entre los posibles: 1, 5, 9, 14, 20, 26, 40, 50, 90, 99, 100, 500 y 1000.";
	Leer num;
	
	Definir cantidad Como Entero;
	cantidad <- 13;
	
	Dimension decimales[cantidad]
	Dimension romanos[cantidad]
	
	InicializarBaseDeDatosNumerosRomanos(decimales, romanos)
	
	Definir indice Como Entero;
	indice <- BusquedaSecuencial(decimales, cantidad, num)
	Si indice <> -1 Entonces
		Escribir "Su correspondiente es romanos es: ", romanos[indice];
	SiNo
		Escribir "No existe traducci�n disponible";
	FinSi
FinAlgoritmo
