Algoritmo CalcularNotaFinal
	//Escribir un algoritmo para calcular la nota final de un estudiante, considerando
    //que: por cada respuesta correcta 5 puntos, por una incorrecta -1 y por respuestas en blanco
	//0. Imprime el resultado obtenido por el estudiante.
	
	Definir cantidadRtasCorrectas, cantidadRtasIncorrectas Como Entero;
	
	Escribir "Ingrese la cantidad de respuestas correctas obtenidas: ";
	Leer cantidadRtasCorrectas;
	Escribir "Ingrese la cantidad de respuestas incorrectas: ";
	Leer cantidadRtasIncorrectas;
	
	Definir puntosRtaCorrecta, puntosRtaIncorrecta Como Entero;
	puntosRtaCorrecta <- 5;
	puntosRtaIncorrecta <- -1;
	
	Definir resultado Como Entero;
	resultado <- (cantidadRtasCorrectas * puntosRtaCorrecta) + (cantidadRtasIncorrectas * puntosRtaIncorrecta);
	
	Escribir "El resultado obtenido por el estudiante es: ", resultado;
FinAlgoritmo
