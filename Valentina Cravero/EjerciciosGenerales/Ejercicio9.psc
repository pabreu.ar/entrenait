Algoritmo CantidadBilletesAEntregar
	//Escriba un programa que lea un n�mero que representar� una cantidad de
	//dinero. El n�mero debe estar entre 1 y 4000. Su programa deber� determinar la menor
	//cantidad de billetes de $500, $200, $100, $50 y $20 as� como de monedas de $10, $5 y $1
	//que debe entregar un empleado a una persona que solicita dicha cantidad.
	
	Definir num Como Entero;
	Escribir "Ingrese una cantidad de dinero: ";
	Leer num;
	
	Definir billete500, billete200, billete100, billete50, billete20, moneda10, moneda5, moneda1 Como Entero;
	billete500 <- Trunc(num / 500);
	resto <- num % 500;
	billete200 <- Trunc(resto / 200);
	resto <- resto % 200;
	billete100 <- Trunc(resto / 100);
	resto <- resto % 100;
	billete50 <- Trunc(resto / 50);
	resto <- resto % 50;
	billete20 <- Trunc(resto / 20);
	resto <- resto % 20;
	moneda10 <- Trunc(resto / 10);
	resto <- resto % 10;
	moneda5 <- Trunc(resto / 5);
	resto <- resto % 5;
	moneda1 <- Trunc(resto / 1);
	
	Escribir "Cantidad de billetes de $500: ", billete500;
	Escribir "Cantidad de billetes de $200: ", billete200;
	Escribir "Cantidad de billetes de $100: ", billete100;
	Escribir "Cantidad de billetes de $50: ", billete50;
	Escribir "Cantidad de billetes de $20: ", billete20;
	Escribir "Cantidad de monedas de $10: ", moneda10;
	Escribir "Cantidad de monedas de $5: ", moneda5;
	Escribir "Cantidad de monedas de $1: ", moneda1;
FinAlgoritmo
