Algoritmo CalculoCostoReceta
	// Ejercicio 3: Dise�e un arreglo en el que se ingrese la cantidad de productos y sus respectivos precios, para la preparaci�n de un plato, 
	// tambi�n se debe mostrar al final el costo a gastar
	
	Escribir "Ingresar la cantidad de ingresientes que tiene el plato"
	Leer cant
	Dimension ingredientes[cant];
	Dimension ingredienteCantidad[cant];
	Dimension precioIngredientes[cant];
	Dimension precioTotalIngrediente[cant];
	Definir precioTotalPlato Como Real;
	Definir i Como Entero;
	i = 1;
	Definir ingrediente como Cadena;
	Definir precioUnitario, cantidadNecesaria Como Real;
	Mientras i <= cant Hacer
		Escribir "Ingrese el ingrediente ", i;
		Leer ingrediente;
		ingredientes[i] = ingrediente;
		Escribir "Ingrese la cantidad necesaria de ", ingrediente;
		Leer cantidadNecesaria;
		ingredienteCantidad[i] = cantidadNecesaria;
		Escribir "Ingrese el precio unitario de ", ingrediente;
		Leer precioUnitario
		precioIngredientes[i] = precioUnitario;
		precioTotalIngrediente[i] = precioUnitario * cantidadNecesaria;
		precioTotalPlato = precioTotalPlato +precioTotalIngrediente[i];
		i = i+1;
	Fin Mientras
	
	i=1;
	Mientras i <= cant Hacer
		Escribir "Se necesita ", ingredienteCantidad[i], " de ", ingredientes[i], " y su costo total es $", precioTotalIngrediente[i];
		i = i+1;
	Fin Mientras
	Escribir  "El precio total del plato es $", precioTotalPlato;
	
FinAlgoritmo
