Algoritmo ImporteaPagarPorAlumno
	//En un Instituto una escuela est� organizando un viaje de estudios, y requiere determinar cu�nto 
	//debe cobrar a cada alumno y cu�nto se debe pagar a la agencia de
    //viajes por el servicio. La forma de cobrar es la siguiente: si son 100 alumnos o m�s, el
	//importe por cada alumno es de 65 pesos; de 50 a 99 alumnos, e es de 70 pesos, de 30 a
    //49, de 95 pesos, y si son menos de 30, el importe del alquiler del autob�s es de 4000
	//pesos, sin importar el n�mero de alumnos. Realiza un algoritmo que permita determinar el
	//pago a la agencia de alquiler de autobuses y lo que debe pagar cada alumno por el viaje
	
	//Se debe averiguar cuanto cobrar a cada alumno y cuanto pagarle a la agencia
	//debemos determinar la cantidad de alumnos 
	//se podria cambiar y definir e inicializar las constantes de los precios para que en un futuro si debemos cambiar el precio sea mas facil
	definir cantAlumnos Como Entero;
	Escribir "ingrese la cantidad de alumnos";
	Leer cantAlumnos
	
	Definir pagoAlumno Como Real;
	si cantAlumnos > 100 entonces;
		Escribir "El importe a pagar por cada alumno es:" 65 " pesos"
	FinSi
	si cantAlumnos >= 50 y cantAlumnos < 100 entonces;
		Escribir "El importe a pagar por cada alumno es:" 70 " pesos"
	FinSi
	si cantAlumnos >= 30 y cantAlumnos < 50 entonces;
		Escribir "El importe a pagar por cada alumno es:" 95 " pesos"
	FinSi
	si cantAlumnos < 30 entonces;
		pagoAlumno <- 4000 / cantAlumnos
		Escribir "El importe a pagar por cada alumno es:" pagoAlumno " pesos"
	FinSi
FinAlgoritmo



