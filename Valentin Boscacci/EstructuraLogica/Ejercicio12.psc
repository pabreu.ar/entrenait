Proceso DividirCapitalInvertido
	//Ejercicio 12: Tres personas deciden invertir su dinero para fundar una empresa. Cada una de ellas invierte una cantidad distinta. 
	//Obtener el porcentaje que cada quien invierte	con respecto a la cantidad total invertida.
	
	Definir aportePersona1, aportePersona2, aportePersona3 Como Real;
	Escribir "Ingrese el aporte de capital de la persona 1";
	Leer aportePersona1;
	Escribir "Ingrese el aporte de capital de la persona 2";
	Leer aportePersona2;
	Escribir "Ingrese el aporte de capital de la persona 3";
	Leer aportePersona3;
	
	Definir capitalTotal Como Real;
	capitalTotal = aportePersona1 + aportePersona2 + aportePersona3;
	
	Escribir "Se invirti� un total de $", capitalTotal;
	Escribir "A la persona 1 le corresponde el ", aportePersona1/capitalTotal*100, "% de lo invertido";
	Escribir "A la persona 2 le corresponde el ", aportePersona2/capitalTotal*100, "% de lo invertido";
	Escribir "A la persona 3 le corresponde el ", aportePersona3/capitalTotal*100, "% de lo invertido";
	
FinProceso
