Proceso CalculoTarifaTelefono
	//Ejercicio 6: Calcular el monto a pagar en una cabina de Internet si el costo por hora es de S/.1,5 y por cada 5 horas te dan 
	//una hora de promoci�n gratis, sabiendo que la	permanencia en la cabina fue de 12 horas.
	
	Definir costoHora Como Real;
	costoHora = 1.5;
	Definir horasHabladas, horasSinCosto, horasConCosto Como Entero;
	horasHabladas = 5;
	// Cada 6 horas habladas cobran 5.
	horasSinCosto = TRUNC(horasHabladas/6);
	horasConCosto = horasHabladas - horasSinCosto;
	Definir tarifaTotal Como Real;
	tarifaTotal = horasConCosto * costoHora;
	
	Escribir "Habl� un total de ", horasHabladas, " horas. De las cuales ", horasSinCosto, " fueron sin costo. El total a pagar es $", tarifaTotal;
	
FinProceso
