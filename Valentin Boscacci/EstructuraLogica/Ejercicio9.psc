Proceso DividirTerreno
	//Ejercicio 9: Un empresario decide heredar su terreno a sus 3 hijos, tomando como
	//referencia la cantidad de hijos, al mayor que tiene 6 hijos le otorga el doble del menor, al
	//que tiene 2 hijos le corresponde 125.2 mt2, al menor le otorga el 27% del total del terreno.
	//Calcular el total del terreno y la cantidad que le corresponde a cada hijo.
	
	//Hijo 1 (mayor): tiene 6 hijos, le da el doble que al hijo 3 (menor)
	// Hijo 2: tiene 2 hijos, le da 125.2mt2
	// Hijo 3 (menor): le da el 27% del terreno.
	
	// regla de tres a partir de la sup. del terreno del hijo del medio: Si 125.2 m2 es (porcentajeTerrenoHijoMedio) del 100% del (totalTerreno)
	// entonces totalTerreno = 100% * 125.2 / porcentajeTerrenoHijoMedio Y porcentajeTerrenoHijoMedio = 100% - 27% - 27%*2 = 19%
	// totalTerreno = 658.947m2
	
	Definir porcentajeTerrenoHijoMayor, porcentajeTerrenoHijoMedio, porcentajeTerrenoHijoMenor Como Real;
	porcentajeTerrenoHijoMenor = 0.27;
	porcentajeTerrenoHijoMayor = porcentajeTerrenoHijoMenor * 2;
	porcentajeTerrenoHijoMedio = 1 - porcentajeTerrenoHijoMayor - porcentajeTerrenoHijoMenor;
	Definir terrenoHijoMedio, totalTerreno Como Real;
	terrenoHijoMedio = 125.2;
	totalTerreno = 1 * 125.2 / porcentajeTerrenoHijoMedio;
	Escribir "La superficie total del terreno es ", totalTerreno, "m2";
	
	Definir terrenoHijoMenor, terrenoHijoMayor Como Real;
	terrenoHijoMenor = totalTerreno * porcentajeTerrenoHijoMenor;
	terrenoHijoMayor = totalTerreno * porcentajeTerrenoHijoMayor;
	Escribir "Al hijo menor le corresponden ", terrenoHijoMenor, "m2";
	Escribir "Al hijo del medio le corresponden ", terrenoHijoMedio, "m2";
	Escribir "Al hijo mayor le corresponden ", terrenoHijoMayor, "m2";

FinProceso
