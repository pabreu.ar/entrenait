Proceso CalculoEdad
	//Ejercicio 10: Mostrar la edad de una persona, ingresando el a�o de nacimiento y el a�o	actual.
	Definir anioNacimiento Como Entero;
	Definir anioActual Como Entero;
	Repetir
		Escribir "Ingrese el a�o de nacimiento";
		Leer anioNacimiento;
		Escribir "Ingrese el a�o actual";
		Leer anioActual;
		Si anioActual<anioNacimiento Entonces
			Escribir "El a�o de nacimiento debe ser anterior o igual al a�o actual";
		FinSi
	Hasta Que anioActual>anioNacimiento
		
	Escribir "La edad de la persona es ", anioActual-anioNacimiento;
	
FinProceso
