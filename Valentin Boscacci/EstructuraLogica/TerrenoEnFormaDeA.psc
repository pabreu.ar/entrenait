Algoritmo TerrenoEnFormaDeA
	//Una empresa constructora vende terrenos con la forma A de la figura. Realizar
	//un algoritmo y repres�ntelo mediante un diagrama de flujo y el pseudoc�digo para obtener
	//el �rea respectiva de un terreno de medidas de cualquier valor
	
	Definir ladoA, ladoB, ladoC Como Numerico;
	
	Escribir "Ingrese la medida del lado A de la figura";
	Leer ladoA;
	Escribir "ingrese la medida del lado B de la figura";
	Leer ladoB;
	Escribir "ingrese la medida del lado C de la figura";
	Leer ladoC;
	
	Definir ladoATriangulo Como Numerico;
	ladoATriangulo <- ladoA - ladoC;
	
	Definir areaTriangulo Como Real;
	areaTriangulo <- (ladoB * ladoATriangulo) / 2;
	Escribir "El area del triangulo rectangulo es: ", areaTriangulo;
	
	Definir areaRectangulo Como Real;
	areaRectangulo <- ladoB * ladoC;
	Escribir "El area del rectangulo es: ", areaRectangulo;
	
	Definir areaTerreno Como Real;
	areaTerreno <- areaRectangulo + areaTriangulo;
	Escribir "El area del terreno es de: ",areaTerreno;
FinAlgoritmo
