// Esta funcion carga los valores segun una BD
Funcion  InicializarBaseDeDatos(cedulas Por Referencia, nombres Por Referencia)
	cedulas[1]=1234;
	nombres[1]="Juan";
	cedulas[2]=5678;
	nombres[2]="Anto";
	cedulas[3]=2134;
	nombres[3]="Bosca";
	cedulas[4]=7531;
	nombres[4]="Carlos";
FinFuncion

Funcion position <- busquedaSecuencial(arreglo Por Referencia, llave Por Valor, n Por Valor)
	Definir position Como Entero;
	position = -1;
	Definir i Como Entero;
	i=1;
	Mientras (position = -1 Y i<=n) Hacer
		Si (arreglo[i]=llave) Entonces
			position=i;
		FinSi
		i=i+1;
	FinMientras
FinFuncion

Algoritmo VerificarVotantePorMesa
	//Ejercicio 3: El d�a de elecciones, en cada mesa se cuenta con un listado de las personas que votar�n en dicha mesa. 
	// Los datos est�n guardados en dos vectores, en el primero la c�dula y en el segundo el nombre. 
	//Se requiere un algoritmo que ingrese la c�dula de una persona e informe si puede sufragar en ese puesto de votaci�n.
	
	Definir n Como Entero;
	n = 4;
	Dimension cedulas[n];
	Dimension nombres[n];
	
	// Se inicializan los arreglos
	InicializarBaseDeDatos(cedulas, nombres);
	
	Escribir "Ingrese la cedula a buscar en la lista de la mesa";
	Definir valorBuscado Como Entero;
	Leer valorBuscado;
	
	position = busquedaSecuencial(cedulas, valorBuscado, n);
	
	Si position=-1 Entonces
		Escribir "La persona con cedula ", valorBuscado, " no se encuentra en la lista de votacion de esta mesa";
	SiNo
		Escribir "La persona ", cedulas[position],"-",nombres[position], " se encuentra habilitada para votar en esta mesa.";
	FinSi
	
FinAlgoritmo
