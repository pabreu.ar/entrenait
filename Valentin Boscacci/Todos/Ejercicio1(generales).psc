Algoritmo ConvertirDecimalABinario
	//Ejercicio 1: Ingresar una cantidad en sistema decimal y transformar a binario.
	
	Escribir "Ingrese una cantidad en sistema decimal para transformarlo en binario";
	Definir cantidad, aux, n Como Entero;
	Leer cantidad;
	
	aux = cantidad;
	// Sabemos cuantos ciclos har�, por lo tanto, cuantos digitos tendr� el numero binario.
	Mientras ( aux  >= 1 ) Hacer
		n = n + 1;
		aux = TRUNC(aux / 2);
	FinMientras
	
	aux = cantidad;
	Definir digitoBinario Como Entero;
	Dimension array[n];
	// Recorro el array del final al inicio y voy cargando los digitos binarios
	Para i = n Hasta 1 Con Paso -1 Hacer
		digitoBinario = aux % 2;
		array[i] = digitoBinario;
		aux = TRUNC(aux / 2);
	Fin Para
	
	// Recorro el array que contiene los digitos binarios y los va concatenando
	Definir resultado Como Cadena;
	Para i = 1 Hasta n Hacer
		datoBinario = array[i];
		resultado = CONCATENAR(resultado,CONVERTIRATEXTO(datoBinario) );
	FinPara
	Escribir resultado;
	
FinAlgoritmo
