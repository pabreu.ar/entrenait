Proceso CalculoComisionesYSueldoTotal
	//Ejercicio 1: Un vendedor recibe un sueldo base m�s un 10% extra por comisi�n de sus 
	//ventas. El vendedor desea saber cu�nto dinero obtendr� por concepto de comisiones por
	//las tres ventas que realiza en el mes y el total que recibir� en el mes tomando en cuenta su
	//sueldo base y sus comisiones.
	
	//variables sueldoTotal, sueldoBase, venta1, venta2, venta3, porcentajeComision, comisionTotal;
	// se considera que la comisi�n es el 10% del valor vendido en cada venta
	// porcentajeComision = 10%
	// comisionTotal = (venta1+venta2+venta3)*porcentajeComision
	// sueldoTotal = sueldoBase + comisionTotal
	
	Definir sueldoBase, venta1, venta2, venta3, porcentajeComision Como Real;
	porcentajeComision=10/100;
	Escribir "Ingrese su sueldo base";
	Leer sueldoBase;
	Escribir "Ingrese el monto de venta 1";
	Leer venta1;
	Escribir "Ingrese el monto de venta 2";
	Leer venta2;
	Escribir "Ingrese el monto de venta 3";
	Leer venta3;
			
	Definir sueldoTotal, comisionTotal Como Real;
	comisionTotal = (venta1+venta2+venta3)*porcentajeComision;
	sueldoTotal = sueldoBase + comisionTotal;
	
	Escribir "El monto total de comisiones es ", comisionTotal;
	Escribir "El monto total de sueldo es ", sueldoTotal;
	
FinProceso
