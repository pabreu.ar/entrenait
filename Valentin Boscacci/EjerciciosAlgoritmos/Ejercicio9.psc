Algoritmo CalcularPorcentajeDeNiñosYNiñas
	Definir cantNinios, cantNinias Como Entero
	Escribir "Ingrese la cantidad de niños"
	Leer cantNinios
	Escribir "Ingrese la cantidad de niñas"
	Leer cantNinias
	Definir cantTotal Como Entero
	cantTotal <- cantNinios + cantNinias
	Definir porcentajeNinios, porcentajeNinias Como Real
	porcentajeNinios <- cantNinios*100/cantTotal
	porcentajeNinias <- cantNinias*100/cantTotal
	Escribir "La cantidad total de alumnos en el curso es: " cantTotal
	Escribir "El porcentaje de niños es: " porcentajeNinios
	Escribir "El porcentaje de niñas es: " porcentajeNinias
FinAlgoritmo
