Algoritmo DeterminarAccesoACicloFormativo
	Definir titulo como Caracter
	Repetir
		Escribir "Ingrese Si o No para indicar si cuenta con titulo de bachiller"
		Leer titulo
	Hasta Que Mayusculas(titulo) = "Si" o Mayusculas(titulo) = "No"
	Si Mayusculas(titulo) = "Si" Entonces
		Escribir "Puede acceder al curso"
	SiNo
		Definir prueba Como Caracter
		Repetir
			Escribir "Ingrese Si o No para indicar si supero la prueba de acceso"
			Leer prueba
		Hasta Que Mayusculas(prueba) = "Si" o Mayusculas(prueba) = "No"
		Si Mayusculas(prueba) = "Si" Entonces
			Escribir "Puede acceder al curso"
		SiNo
			Escribir "No puede acceder al curso"
		FinSi
	FinSi
FinAlgoritmo
