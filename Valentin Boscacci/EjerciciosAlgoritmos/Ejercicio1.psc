Algoritmo DeterminarMayorValor
	Definir num1, num2 Como Real
	Escribir "Ingrese un valor"
	Leer num1
	Escribir "Ingrese otro valor distinto"
	Leer num2
	Definir resultado Como Real
	resultado <- num1
	Si num1<num2 Entonces
		resultado <- num2
	FinSi
	Escribir "El mayor de los dos valores es: " resultado
FinAlgoritmo
