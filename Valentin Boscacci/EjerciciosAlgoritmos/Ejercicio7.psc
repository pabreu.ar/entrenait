Algoritmo IngresarNumeroDiasyCalcular
	Definir num Como Entero
	Escribir 'Ingrese un n�mero de d�as'
	Leer num
	Definir anios,meses,semanas Como Real
	anios <- num/365
	meses <- num/30
	semanas <- num/7
	Escribir 'El n�mero ingresado representa:'
	Escribir Trunc(anios),' a�os'
	Escribir Trunc(meses),' meses'
	Escribir Trunc(semanas),' semanas'
	Escribir num,' d�as'
FinAlgoritmo
