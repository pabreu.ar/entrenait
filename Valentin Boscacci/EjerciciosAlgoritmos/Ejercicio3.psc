Algoritmo HipotenusaTriánguloRectángulo
	Definir cateto1, cateto2 Como Real
	Escribir "Ingrese la longitud de un cateto"
	Leer cateto1
	Escribir "Ingrese la longitud del otro cateto"
	Leer cateto2
	Definir hipotenusa Como Real
	hipotenusa <- Raiz(cateto1^2 + cateto2^2)
	Escribir "La hipotenusa es: ", hipotenusa
FinAlgoritmo
