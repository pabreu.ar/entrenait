Algoritmo CalcularCantidadACobrar
	Definir mes Como Cadena
	Escribir "Ingrese el nombre de un mes"
	Leer mes
	Definir importe Como Real
	Escribir "Ingrese el importe de la compra"
	Leer importe
	Definir descuento Como Real
	Si Minusculas(mes) = "octubre" Entonces //definimos minuscular para que no quede ninguna letra en mayus y tengamos problemas
		descuento <- importe - (importe*15/100)
		Escribir "El total con descuento es: " descuento
	SiNo
		Escribir "El total a cobrar sin descuento es: " importe
	FinSi
FinAlgoritmo
