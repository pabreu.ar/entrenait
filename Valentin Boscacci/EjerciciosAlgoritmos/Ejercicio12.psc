Algoritmo ClaveEureka
	Definir clave Como Caracter
	Definir contador Como Entero
	contador <- 1
	Mientras contador<=3 Hacer
		Escribir 'Ingrese una clave'
		Leer clave
		Si Minusculas(clave)='eureka' Entonces
			Escribir 'La clave ingresada es correcta'
			// Asignar a contador un valor mayor a 3 para finalizar el algoritmo
			contador <- 4
		SiNo
			Escribir 'La clave ingresada es incorrecta'
			Si contador=3 Entonces
				Escribir 'Has superado el n�mero de intentos'
			FinSi
			contador <- contador+1
		FinSi
	FinMientras
FinAlgoritmo
