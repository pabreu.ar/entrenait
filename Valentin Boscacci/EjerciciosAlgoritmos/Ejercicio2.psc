Algoritmo ImprimirMayorMenosValor
	Definir primero,segund,tercero Como Real
	Escribir "Ingrese el primer valor"
	Leer primero
	Escribir "Ingrese el segundo valor"
	Leer segund
	Escribir "Ingrese el tercer valor"
	Leer tercero
	Si primero=segund o primero=tercero o segund=tercero Entonces
		Escribir "Los valos ingresados son iguales"
	SiNo
		Definir max, min Como Real
		Si primero>segund Entonces
			max <- primero
			min <- segund
		SiNo
			max <- segund
			min <- primero
		FinSi
		Si tercero>max Entonces
			max <- tercero
		SiNo
			Si tercero<min Entonces
				min <- tercero
			FinSi
		FinSi
		Escribir "El mayor valor es " max
		Escribir "El menor valor es " min
	FinSi
FinAlgoritmo
