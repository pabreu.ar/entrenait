// Esta funcion carga los valores segun una BD
Funcion  InicializarBaseDeDatos(numerosF Por Referencia, nombres Por Referencia, fechas Por Referencia, valores Por Referencia)
	numerosF[1] = 12;
	nombres[1] = "Juan";
	fechas[1] = "12/05/2020";
	valores[1] = 1050;
	
	numerosF[2] = 5;
	nombres[2] = "Antonela";
	fechas[2] = "01/03/2021";
	valores[2] = 12000;
FinFuncion

Funcion posicion <- busquedaSecuencial(arreglo Por Referencia, llave Por Valor, n Por Valor)
	Definir posicion Como Entero;
	posicion = -1;
	Definir i Como Entero;
	i = 1;
	Mientras (posicion = -1 Y i <= n) Hacer
		Si (arreglo[i] = llave) Entonces
			posicion = i;
		FinSi
		i = i + 1;
	FinMientras
FinFuncion

Algoritmo LeerNumeroFactura
	//En el almac�n TodoCaro se cuenta con los datos: n�mero de factura, nombre
	//del cliente, fecha de facturaci�n y valor de la factura, almacenados en vectores. Se desea
	//un algoritmo que lea el n�mero de factura y muestre los dem�s datos
	
	//le asignamos valor a n para que pueda ser modificado en un futuro. Y no haya que cambiar la cantidad de elementos en cada arreglo.
	Definir n Como Entero;
	n = 3;
	Dimension numerosF[n];
	Dimension nombres[n];
	Dimension fechas[n];
	Dimension valores[n];
	// Se inicializan los arreglos
	InicializarBaseDeDatos(numerosF, nombres, fechas, valores);
	
	Escribir "Ingrese el numero de factura que se desea saber";
	Definir num Como Entero;
	Leer num;
	
	Definir posicion Como Entero;
	posicion = busquedaSecuencial(numerosF, num, n);
	
	Definir i Como Entero;
	i = 1;
	Mientras (posicion = -1 Y i <= n) Hacer
		Si (numerosF[i] = num) Entonces
			posicion = i;
		FinSi
		i = i + 1;
	FinMientras
	
	Si posicion = -1 Entonces
		Escribir "No se ha encontrado el numero de factura ";
	SiNo
		Escribir "El numero de la factura es: ", numerosF[i], " la fecha de facturacion es: ", fechas[i], " , lo que debe es", valores[i], " y el nombre del cliente es " , nombres[i];
	FinSi
	
FinAlgoritmo

