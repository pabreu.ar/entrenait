Algoritmo OrdenamientoBinario
	Definir puntero, final, vector Como Entero;
	puntero = 1;
	final <- 5;
	Dimension vector[final];
	vector[1] <- 3;
	vector[2] <- 8;
	vector[3] <- 11;
	vector[4] <- 22;
	vector[5] <- 13;
	Definir encontro Como Logico;
	encontro = Falso;
	
	Definir num,mitad Como Entero;
	Escribir "Ingrese el numero a buscar: ";
	Leer num
	
	Mientras encontro = Falso y puntero <= final Hacer
		mitad <- TRUNC((puntero + final) / 2)
		num = vector(mitad);
		
		Si num = vector(mitad) Entonces
			encontro = Verdadero
		Sino 
			si num < vector(mitad)
				final <- mitad - 1;
			sino 
				puntero <- mitad + 1;
			FinSi
		FinSi
	FinMientras
	
	Si encontro = Verdadero 
		Escribir "El dato se encontro en la posicion: ", mitad;
	Sino 
		Escribir "El dato no se encuentra";
	FinSi
FinAlgoritmo
