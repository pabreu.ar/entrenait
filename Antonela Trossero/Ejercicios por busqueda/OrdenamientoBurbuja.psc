Algoritmo OrdenamientoBurbuja
	Definir arreglo, aux Como Entero;
	Dimension arreglo[5];
	arreglo[1] = 4;
	arreglo[2] = 5;
	arreglo[3] = 2;
	arreglo[4] = 1;
	arreglo[5] = 3;
	
	Definir i,n,j Como Entero;
	n = 5
		Para i <- 1 Hasta n Con Paso 1 Hacer
			Para j <- 1 Hasta n - 1 Hacer
				si arreglo[j] > arreglo[j+1]
					aux <- arreglo[j]
					arreglo[j] <- arreglo[j+1]
					arreglo[j+1] <- aux
				FinSi
				
			FinPara
		FinPara
		
		Escribir "El arreglo ordenado en forma ascendente es: "
		Para i <- 1 Hasta n Hacer
			Escribir  arreglo[i];
		FinPara
	
FinAlgoritmo
