// Esta funcion carga los valores segun una BD
Funcion  InicializarBaseDeDatos(codigos Por Referencia, nombres Por Referencia, notas Por Referencia)
	notas[1]=8;
	codigos[1]=1234;
	nombres[1]="Juan";
	notas[2]=9;
	codigos[2]=5678;
	nombres[2]="Anto";
	notas[3]=7;
	codigos[3]=2134;
	nombres[3]="Bosca";
	notas[4]=6;
	codigos[4]=7531;
	nombres[4]="Carlos";
FinFuncion

Algoritmo DatosDeEstudiantesDeSecundaria
	//Un profesor guarda los datos de sus estudiantes en tres vectores: c�digo,
	//nombre y nota. Se requiere un algoritmo para consultar la nota de un estudiante a partir de su c�digo.
	
	Escribir "Ingrese la cantidad de estudiantes que hay en el curso: "
	Leer n;
	Dimension notas[n];
	Dimension codigos[n];
	Dimension nombres[n];
	
	// Se inicializan los arreglos
	InicializarBaseDeDatos(codigos, nombres, notas);
	
	Escribir "Ingrese el c�digo del alumno cuya nota se quiere saber";
	Definir codigo Como Entero;
	Leer codigo;
	Definir posicion Como Entero;
	posicion = -1;
	Definir i Como Entero;
	i=1;
	Mientras (posicion = -1 Y i <= n) Hacer
		Si (codigos[i] = codigo) Entonces
			posicion = i;
		FinSi
		i = i + 1;
	FinMientras
	
	Si posicion = -1 Entonces
		Escribir "No se ha encontrado el alumno con el c�digo ", codigo;
	SiNo
		Escribir "La nota del alumno ", nombres[position], " es ", notas[position];
	FinSi
	
FinAlgoritmo
