Algoritmo ConvertirDecimalBinario
	Dimension decimales[10]
	i <- 0
	bin <- 0
	
	Definir  res, dec Como Entero
	Escribir "Ingrese un numero decimal";
	Leer dec;
	
	Repetir 
		res <- dec mod 2;
		bin <- bin + 10 ^ i * res
		dec <- trunc(dec/2)
		i = i + 1
	Hasta Que dec = 1
	
	bin <- bin + 10 ^ i
	Escribir "El numero decimal pasado a binario es: ", bin;
FinAlgoritmo