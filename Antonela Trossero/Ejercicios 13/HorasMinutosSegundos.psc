Algoritmo HorasMinutosSegundos
	Definir horas, seg, minutos, tiempo Como Entero;
	Escribir "Ingrese la hora de partida: "
	Leer horas;
	Escribir "Ingrese los minutos de partida: "
	Leer minutos;
	Escribir "Ingrese los segundos de partida: "
	Leer seg;
	Escribir "Ingrese el tiempo que viaja en segundos: "
	Leer tiempo;
	
	Definir totalSeg, segRestantes, nuevoHH, nuevoMM, nuevoSS Como Entero;
	totalSeg <- (horas * 3600 + min * 60 + seg ) + tiempo;
	nuevoHH <- TRUNC(totalSeg / 3600)
	segRestantes <- TRUNC(totalSeg mod 3600)
	nuevoMM <- TRUNC(segRestantes / 60)
	nuevoSS <- TRUNC(segRestantes mod 60)
	
	Escribir "El horario de llegada a destino es ", nuevoHH, " hs ", nuevoMM, " minutos ", nuevoSS, " segundos";
	FinAlgoritmo
