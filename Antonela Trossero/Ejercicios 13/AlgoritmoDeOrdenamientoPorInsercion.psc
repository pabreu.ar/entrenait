Funcion OrdenamientoPorIncersion(arreglo Por Referencia, n Por valor)
	Definir i,j, valorAnterior, valorActual Como Entero;
	Para i <- 2 Hasta n Con Paso 1 Hacer
		valorActual <- arreglo[i];
		j <- i - 1
		valorAnterior = arreglo[j]
		
		Mientras valorActual < valorAnterior y j >= 1 Hacer
			//Aca lo que estamos haciendo es intercambiar 
			arreglo[j] = valorActual
			arreglo[j + 1] = valorAnterior
			//toma el prox valor a comparar
			j = j - 1;
			Si j <> 0 Entonces
				valorAnterior = arreglo[j]
			FinSi
		FinMientras
	FinPara
FinFuncion

Algoritmo AlgoritmoDeOrdenamientoPorInsercion
	Definir n Como Entero;
	n <- 5;
	Dimension arreglo[n];
	arreglo[1] = 4;
	arreglo[2] = 5;
	arreglo[3] = 2;
	arreglo[4] = 1;
	arreglo[5] = 3;
	
	 OrdenamientoPorIncersion(arreglo , n);
	
	Escribir "El arreglo ordenado en forma ascendente es: "
	Para i <- 1 Hasta n Hacer
		Escribir  arreglo[i];
	FinPara
FinAlgoritmo
