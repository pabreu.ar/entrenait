Algoritmo NumeroPositivoONegativo
	// Determinar el algoritmo para saber si un n�mero es positivo o negativo
	
	Definir n Como Entero;
	Leer n;
	Si n > 0 Entonces;
		Escribir "El numero ingresado es positivo";
	SiNo
		Si n = 0 entonces;
			Escribir "El numero ingresado es neutro";
		Sino 
		Escribir "El numero ingresado es negativo";
		FinSi
	FinSi
FinAlgoritmo
