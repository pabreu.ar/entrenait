Algoritmo SumarConUnPara
	
	Definir suma, contador Como Entero;
	contador <- 0;
	suma <- 0;
	
	Para i <- 1 Hasta 100 Hacer
		suma <- contador + suma;
		contador <- contador + 1;
	FinPara
	
	Escribir "La suma es: ", suma;
FinAlgoritmo
