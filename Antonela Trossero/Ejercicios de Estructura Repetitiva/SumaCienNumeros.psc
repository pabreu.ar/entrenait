Algoritmo SumaCienNumeros
	
	Definir suma, contador Como Entero;
	contador <- 0;
	suma <- 0;
	
	Hacer 
		suma <- contador + suma;
		contador <- contador + 1;
	Mientras Que contador < 100
	
	Escribir "La suma es: ", suma;
FinAlgoritmo
