Algoritmo NumerosMultiplicadosDelUnoAlNueve
	//Hacer un algoritmo en Pseint que imprima la tabla de multiplicar de los
	//	n�meros del uno al nueve.
	
	Definir i, j Como Entero;
	Definir numMultiplicado Como Entero;
	Para i <- 1 hasta 9 Hacer
		Escribir "Ingrese la tabla de: ", i;
		Para j <- 1 Hasta 9 Hacer
			numMultiplicado <- i * j
			Escribir "el numero multiplicado da: ", numMultiplicado;
		FinPara
	FinPara
FinAlgoritmo
