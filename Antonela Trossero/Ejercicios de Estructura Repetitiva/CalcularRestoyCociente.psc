
	Algoritmo CalcularRestoyCociente
		// Hacer un algoritmo en Pseint para calcular el resto y cociente por medio de restas sucesivas
		//Ir restando al dividendo el divisor y me quedo con el resto
		// Sumo la cantidad de veces que pude hacer la resta anterior y eso seria el cociente
		
		Definir dividendo, divisor Como Entero;
		Escribir "Ingrese un dividendo y un divisor";
		Leer dividendo, divisor;
		
		Definir cociente, resto Como Entero;
		resto <- dividendo;
		cociente <- 0;
		Mientras resto >= divisor Hacer
			resto <- resto - divisor;
			cociente <- cociente + 1;
		Fin Mientras
		Escribir "El resto es:", resto
		Escribir "El cociente es:", cociente
FinAlgoritmo

