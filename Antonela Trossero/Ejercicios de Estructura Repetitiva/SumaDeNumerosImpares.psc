Algoritmo SumaDeNumerosImpares
	//Hacer un algoritmo en Pseint para calcular la suma de los n�meros impares
	//menores o iguales a n
	//Ingresa un numero
	//Inicializa la variable en cero
	//Usar un mientras  
	//Un numero impar es cuando su resto o su division es distinta de cero
	
	Definir n, suma Como Entero;
	Escribir "Ingrese el valor n";
	Leer n;
	
	Definir i Como Entero;
	i <- 1;
	suma <- 0;
	Mientras i <= n Hacer
		Si i Mod 2 <> 0 Entonces
			suma <- suma + i;
		FinSi
		i <- i + 1;
	FinMientras
	
	Escribir "el valor de la suma es: ",suma;
FinAlgoritmo
