Algoritmo PrecioPorPlato
//Ejercicio 3: Dise�e un arreglo en el que se ingrese la cantidad de productos y sus
	//respectivos precios, para la preparaci�n de un plato, tambi�n se debe mostrar al final el
	//costo a gastar.
	
	Escribir "Ingresar la cantidad de ingredientes que tiene el plato"
	Leer cant
	Dimension ingredientes[cant];
	Dimension precioIngredientes[cant];
	Definir i Como Entero;
	Definir costoFinal Como Real;
	i <- 1
	Definir precioUnitario, cantidadNecesaria Como Real
	Mientras i <= cant Hacer
		Escribir "Ingrese la cantidad necesaria del ingrediente "
		Leer cantidadNecesaria
		Escribir "Ingrese el precio unitario por ingrediente"
		Leer precioUnitario
		precioIngredientes[cant] <- precioUnitario * cantidadNecesaria;
		costoFinal <- precioIngredientes[cant] + costoFinal;
		i <- i + 1;
	Fin Mientras
Escribir "El costo final del plato seria:" ,costoFinal;
FinAlgoritmo
