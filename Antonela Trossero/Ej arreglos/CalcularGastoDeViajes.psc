
Algoritmo CalcularGastoDeViajes
	//Escribir 3 destinos tur�sticos, la distancia desde Sunchales, el costo del litro
	//as� como la distancia . Se debe mostrar el gasto que demanda en viajar a cualquiera de
	//dichos lugares tur�sticos.
	
	//Ingresar destinos
	Definir destino Como Cadena;
	Definir n Como Entero;
	n <- 3;
	Dimension destinos[n];
	
	Para i <- 1 Hasta n Hacer
		Escribir "Ingrese los destinos a recorrer: ";
		Leer destino;
		destinos[n] <- destino;
	FinPara
	
	Definir distancia Como Real;
	Dimension distancias[n];
	
	Para i <- 1 Hasta n Hacer
		Escribir "Ingrese las distancias hacia cada destino: ";
		Leer distancia;
		distancias[n] <- distancia;
	FinPara
	
	Definir costo Como Real;
	Escribir "Ingrese el costo del litro de nafta: ";
	Leer costoLitro;
	Definir litro Como Real;
	Dimension litros[n];
	Dimension gastos[n];
	
	Para i <- 1 Hasta n Hacer
		Escribir "Ingrese los litros cargados en el viaje: ";
		Leer litro;
		litros[n] <- litro;
	FinPara
	Para i <- 1 Hasta n Hacer
		gastos[n] <- costoLitro * litros[n];
	FinPara
	
	Para i <- 1 Hasta n Hacer
		Escribir "El destino recorrido fue: ", destinos[n], "La distancia en km fue de: ", distancias[n], " El gasto de nafta fue de: ", gastos[n];
	FinPara
FinAlgoritmo
